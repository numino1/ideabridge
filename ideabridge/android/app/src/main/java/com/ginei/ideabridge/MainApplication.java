package com.ginei.ideabridge;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.reactnativecommunity.cameraroll.CameraRollPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import com.mg.app.PickerPackage;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
// import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import com.rnfs.RNFSPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.horcrux.svg.SvgPackage;
//import com.imagepicker.ImagePickerPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.smixx.fabric.FabricPackage;
import com.masteratul.exceptionhandler.ReactNativeExceptionHandlerPackage;
// import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import com.terrylinla.rnsketchcanvas.SketchCanvasPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import io.fabric.sdk.android.Fabric;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.crashlytics.android.Crashlytics;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new CameraRollPackage(),
            new DocumentPickerPackage(),
            new PickerPackage(),
            new RNFileViewerPackage(),
            new RNFSPackage(),
            new VectorIconsPackage(),
            new SvgPackage(),
            new RNGestureHandlerPackage(),
            new FabricPackage(),
            new ReactNativeExceptionHandlerPackage(),
            new SketchCanvasPackage(),
            new AsyncStoragePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    
    super.onCreate();
    Fabric.with(this, new Crashlytics());
    SoLoader.init(this, /* native exopackage */ false);
    
  }
}
