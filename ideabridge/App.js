/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import { Alert } from 'react-native';
import {
  Login, Verification, Home, SearchPage, MyIdeas, Notifications, Profile,
  Ideas, Approvals, Comments, Activities, SimilarIdeas, Challenges,
  MyChallenges, ViewPagerAddIdea, ViewPagerAddChallenge, ViewPagerViewIdea, ViewPagerViewChallenge,
  Doodle, Landing, InitiateProject, IdeasForChallenge, ApprovalOptions, EditApprovedChallengeData, IdeaSummary, 
  ChallengeSummary, EvaluateApprovals
} from './app/screens';
import { setJSExceptionHandler, setNativeExceptionHandler } from 'react-native-exception-handler';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/es/integration/react";
import { connect } from "react-redux";
import configureStore from "./app/config/store";

import handleError from './app/config/errorHandler';

const { store, persistor } = configureStore();

const onBeforeLift = store => {
  // Check if any data exist /get data before the gate lifts
};

const RootStack = createStackNavigator({
  Landing: { screen: Landing },
  LoginScreen: { screen: Login },
  Verification: { screen: Verification },
  HomeScreen: { screen: Home },
  SearchScreen: { screen: SearchPage },
  MyIdeas: { screen: MyIdeas },
  Notifications: { screen: Notifications },
  Profile: { screen: Profile },
  Ideas: { screen: Ideas },
  Approvals: { screen: Approvals },
  ApprovalOptions: { screen: ApprovalOptions },
  Comments: { screen: Comments },
  Activities: { screen: Activities },
  SimilarIdeas: { screen: SimilarIdeas },
  Challenges: { screen: Challenges },
  MyChallenges: { screen: MyChallenges },
  ViewPager: { screen: ViewPagerAddIdea },
  ViewPagerAddChallenge: { screen: ViewPagerAddChallenge },
  ViewPagerViewIdea: { screen: ViewPagerViewIdea },
  ViewPagerViewChallenge: { screen: ViewPagerViewChallenge },
  Doodle: { screen: Doodle },
  IdeasForChallenge: { screen: IdeasForChallenge },
  EditApprovedChallengeData: { screen: EditApprovedChallengeData },
  IdeaSummary: { screen: IdeaSummary },
  ChallengeSummary: { screen: ChallengeSummary },
  EvaluateApprovals: { screen: EvaluateApprovals },
  InitiateProject: { screen: InitiateProject, navigationOptions: { header: null, headerMode: 'none' } },
},
  {
    initialRouteName: 'Landing',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#000000',
      headerTitleStyle: {
        fontWeight: 'bold',
      }
    },
  }
)

setJSExceptionHandler((error, isFatal) => {
  console.log("GLOBAL ERROR HANDLER");
  handleError(error, isFatal);
}, false);


setNativeExceptionHandler((errorString) => {
  handleError(errorString, false);
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }

  componentWillMount() {
    this.setState({ isReady: true });
    console.disableYellowBox = true;
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate
          onBeforeLift={onBeforeLift(store)}
          persistor={persistor}
        >
          <RootStack />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;