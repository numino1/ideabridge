import { Alert, Platform } from 'react-native';
var Fabric = require('react-native-fabric');
var { Crashlytics, Answers } = Fabric;

export default handleError = (error, isFatal) => {

  if (isFatal) {

    if (error) {
      console.log(" CRASHLYTICS FATAL ERROR ", error);

      let errorName = (error && error.name) ? error.name : "Unknown Fatal Exception";
      let errorMessage = (error && error.message) ? error.message : "Unknown Fatal Message";

      if (Platform.OS === 'ios') {
        if (Crashlytics)
          Crashlytics.recordError(`FATAL ERROR : ${errorName} ${errorMessage}`);

      } else {
        if (Crashlytics)
          Crashlytics.logException(`FATAL ERROR : ${errorName} ${errorMessage}`);

      }
      Alert.alert(
        'An Unexpected error has occurred.',
        'We have reported this to our team ! Please close the app and start again!',
        [{
          text: 'Close'
        }]
      );
    }

  } else {

    if (error) {
      console.log("API ERROR HANDLER CRASHLYTICS NON FATAL ERROR", error);

      let errorResponse = error.response ? error.response : 'Unknown Error';
      let status = (errorResponse && errorResponse.status) ? errorResponse.status : 503
      let message = (errorResponse && errorResponse.data) ? errorResponse.data : 'Network Error';

      if (Platform.OS === 'ios') {
        if (Crashlytics)
          Crashlytics.recordError(`API ERROR : Status Code: ${status} Message: ${message}`);

      } else {
        if (Crashlytics)
          Crashlytics.logException(`API ERROR : Status Code: ${status} Message: ${message}`);

      }
      Alert.alert(
        'The application encountered an error while processing the request.',
        'Please try again after sometime. Sorry for the inconvenience.',
        [{
          text: 'Close'
        }]
      );
    }
  }
}