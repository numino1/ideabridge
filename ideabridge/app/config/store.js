import { createStore, combineReducers, applyMiddleware } from "redux";
import {
  persistCombineReducers,
  persistStore,
  persistReducer
} from "redux-persist";
import storage from "redux-persist/es/storage";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import ReduxPromise from 'redux-promise';
import dataReducer from "../reducers/dataReducer";
import notification from '../reducers/notification';
import authReducer from "../reducers/authReducer";
import verifyReducer from "../reducers/verifyReducer";
import homeReducer from "../reducers/homeReducer";
import resendReducer from "../reducers/resendReducer"

const config = {
  key: "root",
  storage,
  blacklist: []
};

const config1 = {
  key: "root1",
  storage
};

//const middleWare = [thunk, createLogger()];
const middleWare = [thunk];
// Object of all the reducers for redux-persist
const reducer = {
  dataReducer,
  //loginReducer
};

// We are only persisting the dataReducer and loginReducer
const DataReducer = persistReducer(config, dataReducer);
//const LoginReducer = persistReducer(config1, loginReducer);

// combineReducer applied on persisted(dataReducer) and NavigationReducer
const rootReducer = combineReducers({
  DataReducer,
  authReducer,
  notification,
  verifyReducer,
  resendReducer,
  homeReducer
});

function configureStore() {
  let store = createStore(rootReducer, applyMiddleware(...middleWare, ReduxPromise));
  let persistor = persistStore(store);
  return { persistor, store };
}

export default configureStore;