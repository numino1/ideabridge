import { StyleSheet, Dimensions, DeviceInfo } from 'react-native'

export const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
export const fonts = {
  sm: 13,
  md: 24,
  lg: 30,
  xLg: 40
}

export const fontStyle = {
  dis: 'SF Pro Text',
  sys: 'system font',
  txt: 'SF Pro Text',
  rob: 'roboto'
}

export const marginOffset = {
  top: DeviceInfo.isIPhoneX_deprecated && (dimensions.fullWidth < dimensions.fullHeight) ? 24 : 0
}

export const shadow = {
  shadowColor: '#dedfe0',
  shadowOffset: { width: 0, height: 3 },
  shadowOpacity: 1.0,
  shadowRadius: 5,
  elevation: 5,
}

export const header = {
  fontSize: fonts.xLg,
  color: 'black',
  bottom: 20
}
