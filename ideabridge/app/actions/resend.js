import { RESEND } from '../actions/types';
import axios from 'axios';
import { CLIENT_UAT1_URL } from '../config/configuration';


export function resend(user, callback) {

  const url = CLIENT_UAT1_URL + `/ResendVerificationCodeWithEmailId?emailId=${user.emailId}`;
  const config = {
    headers: {
      'api_key': `LgOiuSu64U`
    }
  };
  const request = axios.get(url, config);
  request.then(() => callback());

  return {
    type: RESEND,
    payload: request
  }
}