import axios from "axios";
import handleError from '../config/errorHandler';
var binaryToBase64 = require('binaryToBase64');
var RNFS = require('react-native-fs');
import FileViewer from 'react-native-file-viewer';
import { Platform } from 'react-native';
import { CLIENT_UAT1_URL } from '../config/configuration';

export const submitIdea = ({ data }) => {
  return (dispatch, getState) => {
    dispatch({ type: "ADD_NEW_IDEA_INFO", payload: data });
  };
};

export const updateSavedAddIdeaFlag = ({ status }) => {
  return (dispatch, getState) => {
    dispatch({ type: "UPDATE_SAVEDADDIDEADATA_FLAG", status });
  };
};

export const updateAPIStatusBody = ({ data }) => {
  return (dispatch, getState) => {
    dispatch({ type: "UPDATE_API_STATUS_BODY", data });
  };
};

export const clearSubCategoryValues = () => {
  return (dispatch, getState) => {
    dispatch({ type: "SUB_CATEGORY_AVAILABLE", data: [] });
  };
};


export const clearTaskIdValues = () => {
  return (dispatch, getState) => {
    dispatch({ type: "SUBMIT_EVALUATE_RESPONSE_INFO", data: {} });
  };
};

export const getEvaluationMatrix = () => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/EvaluationMatrix/GetAllEvaluationMatrix`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
    };
    //if (isConnected) {
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getEvaluationMatrix", response);
        dispatch({ type: "IDEA_EVALUATION_MATRIX", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true, loading: false } })

        );
      })
      .catch((error) => {
        console.log("In getEvaluationMatrix", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const getAllCategoriesAndSubCategories = () => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/SubGroup/GetAllSubGroupDetails`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getAllCategoriesAndSubCategories", response);
        dispatch({ type: "ALL_CATEGORY_SUB_CATEGORY_AVAILABLE", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true, loading: false } })

        );
      })
      .catch((error) => {
        console.log("In getAllCategoriesAndSubCategories", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const getContentDataForEvaluate = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/GetContentDetails?contentID=${data.ContentID}`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      }
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getContentDataForEvaluate", response);
        dispatch({ type: "CONTENT_DATA_FOR_EVALUATE", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true, loading: false } })

        );
      })
      .catch((error) => {
        console.log("IngetContentDataForEvaluate", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};





export const getSubCategoryValues = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Form/GetChildDropdownValues`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        parentSelectedValue: data.selectedValue,
        parentPropertyId: 3,
        childPropertyId: 4
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getSubCategoryValues", response);
        dispatch({ type: "SUB_CATEGORY_AVAILABLE", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true, loading: false } })

        );
      })
      .catch((error) => {
        console.log("In getSubCategoryValues", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const getIdeaInfoById = ({ data, taskID }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    let taskId = taskID ? taskID : "";
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Idea/FindById?contentId=${data}&taskId=${taskId}`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
    };
    //if (isConnected) {
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getIdeaInfoById action");
        //console.log("In getIdeaInfoById action ********",JSON.stringify(response));
        dispatch({ type: "IDEA_INFO_BY_ID", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getIdeaInfoById", error);
        let errorMessage = '';
        if (error.response) {
          if (error.response.status == 401) {
            errorMessage = "Authentication required. Please Login."
          } else if (error.response.status == 403) {
            errorMessage = "Not authorized to access this resource."
          } else {
            handleError(error, false);
          }
        }
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: errorMessage }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const getChallengeInfoById = ({ data, taskID }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Idea/FindChallengeById`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        contentId: data,
        taskId: taskID ? taskID : ""
      },
    };
    //if (isConnected) {

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getChallengeInfoById action");
        dispatch({ type: "CHALLENGE_INFO_BY_ID", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getChallengeInfoById", error);
        let errorMessage = '';
        if (error.response) {
          if (error.response.status == 401) {
            errorMessage = "Authentication required. Please Login."
          } else if (error.response.status == 403) {
            errorMessage = "Not authorized to access this resource."
          } else {
            handleError(error, false);
          }
        }
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: errorMessage }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const removeAttachmentFile = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    // console.log("connection in registerUser", isConnected);
    const url = CLIENT_UAT1_URL + `/Form/RemoveFile?id=${data}`;

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    }
    let body = { id: data };

    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In removeAttachmentFile", response);
        //dispatch({type: "IDEA_INFO_BY_ID", data: response.data});
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In removeAttachmentFile", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const getCreatFormJson = () => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Form/GetSubmitForm?contentTypeId=1&projectTypeId=0&contentId=0&formName=employee_submit_idea_form`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        //console.log("In getCreatFormJson", response);
        dispatch({ type: "CREATE_FORM_DATA_AVAILABLE", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getCreatFormJson", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const getCreatFormJsonForChallenges = () => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Form/GetSubmitForm?contentTypeId=2&projectTypeId=0&contentId=0`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getCreatFormJsonForChallenges");
        dispatch({ type: "CREATE_FORM_DATA_AVAILABLE_FOR_CHALLENGES", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getCreatFormJsonForChallenges", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const downloadAttachments = (id, fileName) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    // console.log("connection in registerUser", isConnected);
    const url = CLIENT_UAT1_URL + `/Form/GetFile?id=${id}`;

    let headers = {
      "api_key": 'LgOiuSu64U',
      "Authorization": token,
    }
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .get(url, { headers: headers, responseType: 'arraybuffer' })
      .then(function (response) {
        console.log("In downloadAttachment", response);
        let filestring = binaryToBase64(response.data);
        let imagePath = `${RNFS.DocumentDirectoryPath}/${fileName}`;

        // write the file
        RNFS.writeFile(imagePath, filestring, 'base64')
          .then((success) => {
            console.log('FILE WRITTEN!');
            FileViewer.open(imagePath, Platform.OS === 'android' ? { showAppsSuggestions: true } : { showAppsSuggestions: false })
              .then(() => {
                // success
                console.log('opened file viewer');
              })
              .catch(error => {
                // error
                console.log('ERROR not opened file viewer', error);
              });
          })
          .catch((err) => {
            console.log("Write file", err.message);
          });

        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: false } })
        );
      })
      .catch((error) => {
        console.log("In downloadAttachment", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};




export const getAllAutoCompleteTags = (text) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    // console.log("connection in registerUser", isConnected);

    const url = CLIENT_UAT1_URL + `/Idea/IdeaTagAutoComplete`;

    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        term: text
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getAllAutoCompleteTags", response);
        dispatch({ type: "AUTO_COMPLETE_TAGS_LIST", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getAllAutoCompleteTags", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const getAllTeamMembers = (text) => {
  return (dispatch, getState) => {

    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    // console.log("connection in registerUser", isConnected);
    const url = CLIENT_UAT1_URL + `/User/PeopleSearch`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        term: text,
        excludeIDs: '0',
        formPropertyId: 24,
        includeSubmittingUser: false
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getAllTeamMembers", response);
        dispatch({ type: "ALL_TEAM_MEMBERS_LIST", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getAllTeamMembers", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const saveIdeaInfo = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    //console.log(JSON.stringify(data));
    const url = CLIENT_UAT1_URL + `/Content/SaveFormPosting`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    }
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });

    //if (isConnected) {
    return axios
      .post(url, data, { headers: headers })
      .then(function (response) {
        console.log("In saveIdeaInfo", response);
        dispatch({ type: "SAVE_IDEA_DATA", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );
      })
      .catch((error) => {
        console.log("In saveIdeaInfo", error);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const submitContentEvaluateData = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    //console.log(JSON.stringify(data));
    let action = '';
    let evalResponse = JSON.stringify(data.evaluationResponse);
    let customHeaders = JSON.stringify([{ "Name": "SimpleEvaluationMatrix", "Value": "" }, { "Name": "btnSubmitData", "Value": "" }, { "Name": "btnAskQuery", "Value": "" }, { "Name": "btnReject", "Value": "" }, { "Name": "btnIReviewer", "Value": "" }, { "Name": "btnEvaluate", "Value": "" }, { "Name": "btnRedirect", "Value": "" }, { "Name": "btnPatent", "Value": "" }, { "Name": "btnRework", "Value": "" }]);
    const url = CLIENT_UAT1_URL + `/Content/SubmitContentData?contentID=${data.contentID}&action=${action}&evaluationResponse=${evalResponse}&taskID=${data.taskID}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
      'Custom': customHeaders
    }

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });

    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In submitContentEvaluateData", response);
        dispatch({ type: "SUBMIT_EVALUATE_RESPONSE_INFO", payload: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );
      })
      .catch((error) => {
        console.log("In submitContentEvaluateData", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const undoSubmitContentOnBack = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    //console.log(JSON.stringify(data));
    let action = 'back';
    let evalResponse = '';
    let customHeaders = '';
    const url = CLIENT_UAT1_URL + `/Content/SubmitContentData?contentID=${data.contentID}&action=${action}&evaluationResponse=${evalResponse}&taskID=${data.taskID}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
      'Custom': customHeaders
    }

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });

    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In undoSubmitContentOnBack", response);
        dispatch({ type: "SUBMIT_EVALUATE_RESPONSE_INFO", payload: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );
      })
      .catch((error) => {
        console.log("In undoSubmitContentOnBack", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};
//
export const evaluateData = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    let evalScores = JSON.stringify(data.scores);
    const url = CLIENT_UAT1_URL + `/Content/EvaluateContent`;

    const evalData = {
      'contentID': data.contentID,
      'totalScore': data.totalScore,
      'scores': evalScores,
      'taskID': data.taskID
    }

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U'
    }

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });

    //if (isConnected) {
    return axios
      .post(url, evalData, { headers: headers })
      .then(function (response) {
        console.log("In evaluateData", response);
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );
      })
      .catch((error) => {
        console.log("In evaluateData", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};



export const initiateProject = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    //console.log(JSON.stringify(data));

    const url = CLIENT_UAT1_URL + `/Project/InitiateProject`;

    let projectData = {
      "ContentTypeId": 1,
      "ParentContentId": data.contentID,
      "ProjectLeadId": data.ProjectLeadId,
      "ProjectName": data.ProjectName,
      "ProjectStartDate": data.ProjectStartDate,
      "Remark": data.Remark
    }

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    }
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });

    //if (isConnected) {
    return axios
      .post(url, projectData, { headers: headers })
      .then(function (response) {
        console.log("In initiateProject", response);
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );
      })
      .catch((error) => {
        console.log("In initiateProject", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const postComment = (text, contentId) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    const url = CLIENT_UAT1_URL + `/Content/PostComment`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        comment: text,
        contentId: contentId,
      },
    };
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, {}, config)
      .then(function (response) {
        dispatch({ type: "POST_COMMENT" });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );

      })
      .catch((error) => {
        console.log("In POST_COMMENT", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "POST_COMMENT",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const getIdeasOnChallenge = (contentId) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Idea/GetIdeasAgainstChallenge`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        contentId: contentId,
      },
    };
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getIdeasOnChallenge success ");
        dispatch({ type: "IDEAS_ON_CHALLENGE", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In getIdeasOnChallenge error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const redirectContent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/ApprovalRedirect`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'remarks': data.remarks,
      'taskID': data.taskId,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In redirectContent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In redirectContent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const publishIdeaContent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/PublishContent`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'remarks': data.remarks,
      'taskID': data.taskId,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In publishIdeaContent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In publishIdeaContent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const publishChallengeContent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/PublishChallenge`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'remarks': data.remarks,
      'taskID': data.taskId,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In publishChallengeContent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In publishChallengeContent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const askQuery = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/AskQuery`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'question': data.remarks,
      'taskID': data.taskId,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In askQuery success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In askQuery error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const respondToQuery = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/AnswerQuery`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentID,
      'Response': data.Response,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In respondToQuery success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In respondToQuery error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const rejectContent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/RejectContent`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'remarks': data.remarks,
      'taskID': data.taskId,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In rejectContent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In rejectContent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const reworkIdeaContent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/SendBackToIdeator`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'remarks': data.remarks,
      'taskID': data.taskId,
      'formFields': ""
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In reworkIdeaContent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In reworkIdeaContent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const reworkChallengeContent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/SendBackToChallenger`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentId,
      'remarks': data.remarks,
      'taskID': data.taskId,
      'formFields': ""
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In reworkChallengeContent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In reworkChallengeContent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const assignReviewer = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    /*let contentID = data.contentID;
    let assignReviewerComments = data.assignReviewerComments;
    let assignReviewerUserIds = data.assignReviewerUserIds;*/
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/AssignReviewer?assignReviewerComments=${data.assignReviewerComments}&assignReviewerUserIds=${data.assignReviewerUserIds}&contentID=${data.contentID}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentID,
      'assignReviewerComments': data.assignReviewerComments,
      'assignReviewerUserIds': data.assignReviewerUserIds,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      //.post(url, dataToPublish, {headers:headers})
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In assignReviewer success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In assignReviewer error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const rejectRedirectRequest = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/RejectRedirectRequest`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentID,
      'remarks': data.remarks,
      'taskID': data.taskID,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In rejectRedirectRequest success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In rejectRedirectRequest error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const changeCategory = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/ChangeCategory?contentID=${data.contentID}&categoryID=${data.categoryID}&subCategoryID=${data.subCategoryID}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In changeCategory success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In changeCategory error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const acceptReviewerInvite = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/AcceptReviewerInvite?contentID=${data.contentID}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In acceptReviewerInvite success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In acceptReviewerInvite error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error, success: false }
          })
        );
       
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const rejectReviewerInvite = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/RejectReviewerInvite?contentID=${data.contentID}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In rejectReviewerInvite success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In rejectReviewerInvite error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const applyForPatent = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/ApplyForPatent`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'ContentId': data.contentID,
      'Remarks': data.remarks,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In applyForPatent success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In applyForPatent error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};



export const removeInvitedReviewer = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/RemoveReviewer`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      "ContentId": data.ContentId,
      "ReviewerId": data.ReviewerId,
      "InviterId": data.InviterId,
      "TaskId": data.TaskId
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In removeInvitedReviewer success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In removeInvitedReviewer error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const remindInvitedReviewer = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/RemindReviewer`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      "ContentId": data.ContentId,
      "ReviewerId": data.ReviewerId,
      "ContentType": data.ContentType,
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In remindInvitedReviewer success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In remindInvitedReviewer error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const submitReview = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/SubmitReview?contentID=${data.contentID}&reviewerComments=${data.reviewerComments}&reviewerStatus=${data.reviewerStatus}&reviewerUserName=${data.reviewerUserName}&scores=${data.scores}`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'contentID': data.contentID,
      'reviewerComments': data.reviewerComments,
      'reviewerStatus': data.reviewerStatus,
      'reviewerUserName': data.reviewerUserName,
      'scores': data.scores
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, {}, { headers: headers })
      .then(function (response) {
        console.log("In submitReview success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In submitReview error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const remindForPresentationSubmission = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/RemindIdeatorForPatentPresentationSubission`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPublish = {
      'ContentId': data.ContentId,
      'Remarks': '',
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPublish, { headers: headers })
      .then(function (response) {
        console.log("In remindForPresentationSubmission success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In remindForPresentationSubmission error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const parkIdea = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    const url = CLIENT_UAT1_URL + `/Content/ParkContent`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    };

    let dataToPark = {
      'ContentID': data.ContentID,
      'Remarks': data.Remarks,
      'ParkEndDate': data.ParkEndDate,
      'TaskID': data.TaskID
    };

    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .post(url, dataToPark, { headers: headers })
      .then(function (response) {
        console.log("In parkIdea success ");
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        console.log("In parkIdea error", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};

export const getRejectionReasons = (contentId) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    // console.log("connection in registerUser", isConnected);
    const url = CLIENT_UAT1_URL + `/Content/GetRejectionReasons`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: token
      },
      params: {
        contentId: contentId,
      },
    };
    // dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    //if (isConnected) {
    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getRejectionReasons", response);
        dispatch({ type: "CONTENT_REJECTION_REASONS", data: response.data });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true, loading: false } })

        );
      })
      .catch((error) => {
        console.log("In getRejectionReasons", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );

      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};


export const updateDate = ({ data }) => {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    // const state = getState(); const isConnected = state.DataReducer.isConnected;
    console.log('data=====>', JSON.stringify(data));
    const url = CLIENT_UAT1_URL + `/Content/UpdateChallengeDuration`;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token,
      'api_key': 'LgOiuSu64U',
    }
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });

    //if (isConnected) {
    return axios
      .post(url, data, { headers: headers })
      .then(function (response) {
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })
        );
      })
      .catch((error) => {
        console.log("Update date", error);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
    //}
    /*else {
          console.log("no internet connection");
          dispatch({ type: 'ADD_TO_ACTION_QUEUE', payload: { url: url, res: data, type: "add" } });
        }*/
  };
};