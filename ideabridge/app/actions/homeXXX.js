import { LOGIN } from '../actions/types';
import axios from 'axios';
import { CLIENT_UAT1_URL } from '../config/configuration';

export function home(user, callback) {

  const url = CLIENT_UAT1_URL + `/User/GetUserContents`;
  const config = {
    headers: {
      'api_key': user.apiKey,
      'Authorization': `Basic dWF0dXNlcjFAZWJ1eWFuZHNhdmUuY28udWs6aWRlYWJyaWRnZUAxMjM=`
    }
  };
  const request = axios.get(url, config).then((data) => {
    callback()
  })

  return {
    type: LOGIN,
    payload: data
  }
}