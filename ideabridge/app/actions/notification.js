import { GET_NOTIFICATION_LIST } from '../actions/types';
import axios from 'axios';
import handleError from '../config/errorHandler';
import { CLIENT_UAT1_URL } from '../config/configuration';

export function getNotificationList() {
    return (dispatch, getState) => {

        const state = getState();
        const token = state.verifyReducer.token;

        const url = CLIENT_UAT1_URL + `/Notification/GetUserNotifications?showMore=false`;
        const config = {
            headers: {
                'api_key': `LgOiuSu64U`,
                'Authorization': token
            }
        };

        dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
        return axios.get(url, config)
            .then(response => {
                dispatch({
                    type: GET_NOTIFICATION_LIST,
                    payload: response
                })
                return Promise.resolve(
                    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: false, success: true } })
                );
            })
            .catch((error) => {

                handleError(error, false);
                return Promise.resolve(
                    dispatch({
                        type: "UPDATE_API_STATUS_BODY",
                        data: { loading: false, error: true, errorMsg: error }
                    })
                );
            })
    }
}