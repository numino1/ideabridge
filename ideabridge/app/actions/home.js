import { HOME_SEARCH, HOME_GET_CONTENT, HOME_GET_IDEAS, HOME_GET_NOTIFICATION_APPROVAL, HOME_GET_CHALLENGES } from '../actions/types';
import axios from 'axios';
import handleError from '../config/errorHandler';
import { CLIENT_UAT1_URL } from '../config/configuration';


export function homeSearch(searchText, callback) {
  return (dispatch, getState) => {

    const state = getState();
    const token = state.verifyReducer.token;
    const url = CLIENT_UAT1_URL + `/User/SearchContentMobile?searchPrefix=${searchText.text}`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      }
    };
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios.get(url, config)
      .then(response => {
        dispatch({
          type: HOME_SEARCH,
          payload: response
        })

        dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: false, success: true } })

      })
      .catch((error) => {
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { loading: false, error: true, errorMsg: error.abc }
          })
        );

      })
  }
}

export function homeGetContent() {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;
    console.log("TOKEN ===================================> ", token);

    const url = CLIENT_UAT1_URL + `/User/GetUserContentsForMobile`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      }
    };
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios.get(url, config)
      .then(function (response) {
        dispatch({ type: "HOME_GET_CONTENT", payload: response });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: false, success: true } })
        );
      })
      .catch((error) => {
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
  }
}

export function getNotificationApproval() {
  return (dispatch, getState) => {

    const state = getState();
    const token = state.verifyReducer.token;
    const url = CLIENT_UAT1_URL + `/Notification/GetNotificationApprovalCount`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      }
    };
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios.get(url, config)
      .then(function (response) {
        dispatch({ type: "HOME_GET_NOTIFICATION_APPROVAL", payload: response });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { success: true } })

        );
      })
      .catch((error) => {
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { error: true, errorMsg: error }
          })
        );
      });
  }
}

export function getHomePageContent() {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;

    const url = CLIENT_UAT1_URL + `/User/GetMobileDashboardData`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      }
    };
    dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: true } });
    return axios.get(url, config)
      .then(function (response) {
        dispatch({ type: "HOME_GET_CONTENT", payload: response });
        return Promise.resolve(
          dispatch({ type: "UPDATE_API_STATUS_BODY", data: { loading: false, success: true } })

        );
      })
      .catch((error) => {
        console.log("In homeGetContent", error);
        handleError(error, false);
        return Promise.resolve(
          dispatch({
            type: "UPDATE_API_STATUS_BODY",
            data: { loading: false, error: true, errorMsg: error }
          })
        );
      });
  }
}

export function getIdeas() {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;

    const url = CLIENT_UAT1_URL + `/Idea/GetFilteredIdeasMobile`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      },
    };

    return axios.get(url, config)
      .then(function (response) {
        dispatch({ type: "HOME_GET_IDEAS", payload: response });

      })
      .catch((error) => {
        console.log("In HomegetIdeas", error);
        handleError(error, false);
      });
  }
}


export function getChallenges() {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;

    const url = CLIENT_UAT1_URL + `/Idea/GetFilteredChallengesMobile`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      }
    };

    return axios.get(url, config)
      .then(function (response) {
        dispatch({ type: "HOME_GET_CHALLENGES", payload: response });

      })
      .catch((error) => {
        console.log("In HomegetChallenges", error);
        handleError(error, false);
      });
  }
}

export function getPendingTasksForLoggedInUser() {
  return (dispatch, getState) => {
    const state = getState();
    const token = state.verifyReducer.token;

    const url = CLIENT_UAT1_URL + `/Content/GetPendingTasks`;

    const config = {
      headers: {
        'api_key': `LgOiuSu64U`,
        'Authorization': token
      }
    };

    return axios.get(url, config)
      .then(function (response) {
        dispatch({ type: "PENDING_TASKS_FOR_USER", payload: response });
      })
      .catch((error) => {
        console.log("In HomegetPendingTasksForUser", error);
        handleError(error, false);
      });
  }
}