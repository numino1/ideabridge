import { VERIFY, AUTH_CODE, LOGOUT, CREATE_AUTH_TOKEN } from '../actions/types';
import axios from 'axios';
import { CLIENT_UAT1_URL } from '../config/configuration';

export function verify(info, callback, errorCallback) {
  const url = CLIENT_UAT1_URL + `/VerifyCodeWithEmail?emailId=${info.emailId}&code=${info.code}`;
  const config = {
    headers: {
      'api_key': info.apiKey
    }
  };
  let userDetails;
  const request = axios.get(url, config)
  request.then(() => callback())
  request.catch((error) => {
    errorCallback(error)
  })
  return {
    type: VERIFY,
    payload: request
  }
}

export function verifiedCode(code) {
  console.log("Code", code);
  return {
    type: AUTH_CODE,
    payload: code
  }
}

export function logout() {
  return {
    type: LOGOUT,
    payload: null
  }
}

export function setAppToken(token) {
  return {
    type: CREATE_AUTH_TOKEN,
    payload: token
  }
}