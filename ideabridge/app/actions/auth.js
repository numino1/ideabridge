import { LOGIN, UPDATE_EMAIL } from '../actions/types';
import axios from 'axios';
import handleError  from '../config/errorHandler';
import { CLIENT_UAT1_URL } from '../config/configuration';


export function login( user, callback, errorCallback ) {

  const url =  CLIENT_UAT1_URL + `/RegistrationWithEmailId?emailId=${user.emailId}`;
  const config = {
    headers: {
      'api_key': user.apiKey
    }
  };
  const request = axios.get(url, config);
  request.then((data) => callback(data)); 
  request.catch(( error ) => {
    errorCallback( error )
    //handleError( error, false )
  }) 

  return {
    type: LOGIN,
    payload: request
  }
}


export function updateLoggedInEmail( email ) {
  console.log("Emailds", email);
  return {
    type: UPDATE_EMAIL,
    payload: email
  }
}