import { LOGIN, UPDATE_EMAIL } from '../actions/types';

const initalState = {
    loginStatus: '',
    emailId: ''
}

export default authReducer = (state = initalState, action) => {

    switch (action.type) {
        case LOGIN:
            return { ...state, loginStatus: action.payload.data }
        case UPDATE_EMAIL:
            return { ...state, emailId: action.payload }
    }
    return state;
}