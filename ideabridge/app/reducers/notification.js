import { GET_NOTIFICATION_LIST } from '../actions/types';


const initialState = {
    notifications: null
}

export default notification = (state = initialState, action) => {
    switch (action.type) {
        case GET_NOTIFICATION_LIST:
            return { ...state, notifications: action.payload.data }
    }
    return state;
}
