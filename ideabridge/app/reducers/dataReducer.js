//import _ from "lodash";
let _ = require('lodash');

const initialState = {
  addIdeaInfo: {},
  savedAddIdeaDataFlag: false,
  subCategoryData: [],
  allCategorySubCategoryData: [],
  tagsList: [],
  teamMembersList: [],
  createFormData: {},
  createFormDataForChallenges: {},
  saveIdeaData: {},
  status: false,
  IdeaInfoById: {},
  ChallengeInfoById: {},
  contentDataForEvaluate: {},
  ideasOnChallenge: [],
  rejectionReasons: [],
  evaluationMatrix: [],
  submittedEvaluateDataResponse: {},
  generalAPIStatusBody: { loading: false, success: false, error: false, errorMsg: "" }
};
const attachmentProperty =
{
  "FormId": 17,
  "Properties": [
    {
      "FormPropertyType": {
        "Id": 10,
        "Type": "FileUpload"
      },
      "AdditionalOptions": {
        "Id": 25,
        "FormFieldId": 25,
        "LinkedHtmlId": null,
        "FormPropertySourceTableId": null,
        "MinimumDate": null,
        "MaximumDate": null,
        "IncludeSubmittingUser": null,
        "Minimum": null,
        "Maximum": null,
        "Placeholder": null,
        "FileExtensions": null,
        "MaximumFileSize": null,
        "MinimumFileSize": null,
        "IsMinimumDateToday": null,
        "IsMaximumDateToday": null,
        "MaximumDaySpan": null,
        "LinkedUserBoxId": null,
        "LinkedMultiselectId": null,
        "LinkedChallengeTypeDropdownId": null,
        "FormPropertyDestinationTableId": null,
        "AllowAttachment": false,
        "TemplateLink": null,
        "TemplateLabel": null,
        "AllowAudio": false,
        "AllowVideo": false
      },
      "FormPropertyListOptions": [],
      "Value": null,
      "MaximumDateString": "",
      "MinimumDateString": "",
      "IsDisabled": false,
      "Attachments": null,
      "Id": 25,
      "FormId": 17,
      "FormFieldName": "idea_attachments",
      "Name": "Attachments",
      "FormPropertyTypeId": 10,
      "HtmlName": "html_name_24",
      "HtmlId": "html_id_24",
      "Label": "Attachments",
      "Required": false,
      "Order": 0,
      "DisableAfterSubmit": false,
      "IsActive": true,
      "CanDelete": true,
      "HelpText": null,
      "RequiredMessage": null,
      "DisableBeforeSubmit": false,
      "Note": null,
      "RequiredAny": false
    }
  ],
  "ContentId": 0,
  "ContentTypeId": 0,
  "ProjectTypeId": null,
  "ContentType": null,
  "DoSubmit": false,
  "ParentContentId": 0,
  "Files": null,
  "SubForms": [],
  "Name": "Attachments",
  "HasVerticalTabs": false,
  "GlobalOptions": null,
  "IsActive": false,
  "UserTypeId": 0,
  "Sequence": 0,
  "FormData": null,
  "Status": null
}

const dataReducer = (state = initialState, action) => {
  switch (action.type) {

    case "ADD_NEW_IDEA_INFO":
      {
        const {
          ideaId,
          pages
        } = action.payload;
        const newPages = state.addIdeaInfo[ideaId] ? [...state.addIdeaInfo[ideaId], ...pages] : pages;
        return {
          ...state,
          addIdeaInfo: { ...state.addIdeaInfo, [ideaId]: newPages }
        }
      }
    case "UPDATE_SAVEDADDIDEADATA_FLAG":
      return Object.assign({}, state, {
        savedAddIdeaDataFlag: action.status.status
      });
    case "IDEA_INFO_BY_ID":
      state = Object.assign({}, state, { IdeaInfoById: action.data });
      return state;
    case "CHALLENGE_INFO_BY_ID":
      state = Object.assign({}, state, { ChallengeInfoById: action.data });
      return state;
    case "IDEAS_ON_CHALLENGE":
      state = Object.assign({}, state, { ideasOnChallenge: action.data });
      return state;
    case "IDEA_EVALUATION_MATRIX":
      state = Object.assign({}, state, { evaluationMatrix: action.data });
      return state;
    case "UPDATE_API_STATUS_BODY":
      {
        return Object.assign({}, state, {
          generalAPIStatusBody: Object.assign({}, state.generalAPIStatusBody, action.data)
        });
      }
    case "SUB_CATEGORY_AVAILABLE": {
      const values = [
        {
          label: "Please select",
          value: "Please select",
          disabled: true
        }
      ];
      action.data.map((item, i) => {
        values.push(
          {
            label: item.Value,
            value: item.Id
            //value: item.Value
          });
      })
      state = Object.assign({}, state, { subCategoryData: values });
      return state;
    }
    case "ALL_CATEGORY_SUB_CATEGORY_AVAILABLE": {
      const values = [];
      action.data.map((item, i) => {
        values.push(
          {
            groupLabel: item.Group.Name,
            groupValue: item.Group.Id,
            subGroupLabel: item.Name,
            subGroupValue: item.Id
          });
      })
      state = Object.assign({}, state, { allCategorySubCategoryData: values });
      return state;

    }
    case "CREATE_FORM_DATA_AVAILABLE":
      let formDataIdea = { ...action.data };
      if (formDataIdea.SubForms.findIndex(item => item.Name === "Attachments") === -1) {
        formDataIdea.SubForms.push(attachmentProperty);
      }
      state = Object.assign({}, state, { createFormData: formDataIdea });
      return state;
    case "CREATE_FORM_DATA_AVAILABLE_FOR_CHALLENGES":
      let formData = { ...action.data };
      if (formData.SubForms.findIndex(item => item.Name === "Attachments") === -1) {
        formData.SubForms.push(attachmentProperty);
      }
      state = Object.assign({}, state, { createFormDataForChallenges: formData });
      return state;
    case "SAVE_IDEA_DATA":
      state = Object.assign({}, state, { saveIdeaData: action.data });
      return state;
    case "AUTO_COMPLETE_TAGS_LIST":
      state = Object.assign({}, state, { tagsList: action.data });
      return state;
    case "ALL_TEAM_MEMBERS_LIST":
      state = Object.assign({}, state, { teamMembersList: action.data });
      return state;
    case "CONTENT_REJECTION_REASONS":
      state = Object.assign({}, state, { rejectionReasons: action.data });
      return state;
    case "SUBMIT_EVALUATE_RESPONSE_INFO":
      state = Object.assign({}, state, { submittedEvaluateDataResponse: action.payload });
      return state;
    case "CONTENT_DATA_FOR_EVALUATE":
      const values = [];
      action.data.Ratings.map((item, i) => {
        values.push(
          {
            criteriaID: item.EvaluationSubCriteriaID,
            ID: item.Id,
          });
      })
      state = Object.assign({}, state, { contentDataForEvaluate: values });
      return state;
    case "CLEAR_STORAGE_OBJECT_FOR_TEST":
      //give whichever store object u want to clear/reset after testing
      return Object.assign({}, state, { addIdeaInfo: {} });
    /*case 'persist/REHYDRATE':
    // By adding a `RESET` action, we can dispatch this to re-initialize our store.
    // You can dispatch this action on logout, or whenever you need to reset.
      return initialState;*/
    case "POST_COMMENT":
      return {
        ...state,
        status: true
      }
    default:
      return state;
  }
};

export default dataReducer;
