import { LOGIN } from '../actions/types';

const initalState = {
    emailId: ''
}

export default authReducer = (state = initalState, action) => {
    switch (action.type) {
        case LOGIN:
            return { emailId: action.payload }
    }
    return state;
}