import { VERIFY, AUTH_CODE, LOGOUT, CREATE_AUTH_TOKEN } from '../actions/types';

const initalState = {
    userDetails: '',
    code: '',
    token: 'Basic dWF0MUBlYnV5YW5kc2F2ZS5jby51azppZGVhYnJpZGdlQDEyMw=='
}

export default verifyReducer = (state = initalState, action) => {
    switch (action.type) {

        case VERIFY:
            return { ...state, userDetails: action.payload.data }
        case AUTH_CODE:
            return { ...state, code: action.payload }
        case LOGOUT:
            return { ...state, code: null }
        case CREATE_AUTH_TOKEN:
            return { ...state, token: action.payload }
        default:
            return state;
    }
}
