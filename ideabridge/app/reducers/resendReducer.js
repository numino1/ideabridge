import { RESEND } from '../actions/types';

const initialState= {
    resendData:''
}

export default authReducer = ( state = initialState, action ) => {

    switch (action.type) {
        case RESEND :
            return { resendData  : action.payload }
    }
    return state;
}