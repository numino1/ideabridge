import { HOME_SEARCH, HOME_GET_CONTENT, HOME_GET_NOTIFICATION_APPROVAL, HOME_GET_IDEAS, HOME_GET_CHALLENGES, PENDING_TASKS_FOR_USER } from '../actions/types';

const initalState = {
    content: {},
    ideas: [],
    challenges: [],
    pendingTasks: [],
    notification: {},
    serachData: {}
}

export default verifyReducer = (state = initalState, action) => {

    switch (action.type) {
        case HOME_SEARCH:
            return { ...state, serachData: action.payload.data }
        case HOME_GET_CONTENT:
            return { ...state, content: action.payload.data };
        case HOME_GET_NOTIFICATION_APPROVAL:
            return { ...state, notification: action.payload.data };
        case HOME_GET_IDEAS:
            return { ...state, ideas: action.payload.data };
        case HOME_GET_CHALLENGES:
            return { ...state, challenges: action.payload.data };
        case PENDING_TASKS_FOR_USER:
            return { ...state, pendingTasks: action.payload.data };
        default:
            return state;
    }
}