import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { systemWeights } from 'react-native-typography';
import CardChallenges from '../components/CardChallenges';
import { getChallengeInfoById, updateAPIStatusBody } from "../actions/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { SearchBar } from 'react-native-elements';
import { fonts, header } from '../css/GlobalCss'
let _ = require('lodash');

class Challenges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      text: '',
    };
  }

  componentDidMount() {
    var data = [];
    this.setState({ data: data });
    const { navigation } = this.props;
    data = navigation.getParam('data', ' ');
    this.setState({ data: data });
  }

  onChangeText = (text) => {
    this.setState({
      text,
    })
  }

  handleKeyDown = () => {

  }

  onClearSearchText = () => {

  }

  renderSpinner() {
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }


  getChallengesByIdAPI(contentId) {

    this.props.getChallengeInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getChallengesByIdAPI props success");
          let challengeData = {
            data: { ...this.props.ChallengeInfoById.ContentData },
            title: this.props.ChallengeInfoById.Title,
            type: this.props.ChallengeInfoById.ChallengeType,
            attachments: this.props.ChallengeInfoById.Attachment,
            canEdit: this.props.ChallengeInfoById.CanEdit,
            canRespond: this.props.ChallengeInfoById.CanRespond
          };
          if (this.props.userDetails.UserID == this.props.ChallengeInfoById.ChallengerDetails.UserID) {
            challengeData.contentId = this.props.ChallengeInfoById.ContentID;
            challengeData.contentStatus = this.props.ChallengeInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewChallenge", challengeData);

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getChallengesByIdAPI props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  listItemClicked(itemData) {
    this.getChallengesByIdAPI(itemData.ContentID);
  }

  render() {
    let { navigation, challenges } = this.props;

    if (challenges.length == 0) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      )
    }
    challenges = _.filter(challenges, (item) => {
      return (item.ContentStatus == "Approved");
    });

    if (this.state.text) {
      challenges = _.filter(challenges, (item) => {
        return (item.Title.toLowerCase().indexOf((this.state.text).toLowerCase()) != -1);
      });
    }


    const data = navigation.getParam('data', ' ');
    const challengeCount = challenges.length;
    return (
      <View style={styles.mainviewStyle} pointerEvents={this.props.loading ? "none" : "auto"}>
        {this.renderSpinner()}
        <ScrollView style={styles.container}>
          <View style={[styles.elementsContainer]}>
            <Text style={[systemWeights.bold, { ...header }]}>Challenges</Text>
            <Text style={styles.count}>( {challengeCount} )</Text>
          </View>

          <View style={[styles.parentSearchBar]}>
            <SearchBar
              lightTheme
              inputContainerStyle={[styles.color]}
              leftIconContainerStyle={[styles.color]}
              rightIconContainerStyle={[styles.color]}
              containerStyle={styles.childSearchBar}
              inputStyle={{ backgroundColor: '#FFFFFF', fontSize: fonts.md, fontWeight: '500', color: 'black', left: -9 }}
              value={this.state.text}
              onChangeText={this.onChangeText}
              placeholder='Search'
              placeholderTextColor={'black'}
              clearIcon={this.state.text !== '' ? { name: 'close', color: 'black', type: 'evilicon', size: 32 } : false}
              searchIcon={{ name: 'search', type: 'evilicons', size: 32, color: 'black' }}
              onSubmitEditing={this.handleKeyDown}
              onClear={this.onClearSearchText}
              showLoadingIcon={this.state.loading} />
          </View>
          {challenges.length === 0 ?
            (<View style={styles.resSearchData}>
              <Text style={styles.resultNotFoundStyle}>Results not found</Text>
            </View>) :
            <View style={styles.padding}>
              <FlatList
                data={challenges}
                renderItem={({ item: rowData }) => {
                  return (
                    <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                      <CardChallenges rowDatab={rowData} />
                    </TouchableOpacity>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  let count = Object.keys(state.DataReducer.addIdeaInfo).length;
  return {
    myIdeaCount: count > 0 ? count : 0,
    userDetails: state.verifyReducer.userDetails,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
    content: (state.homeReducer.content),
    challenges: state.homeReducer.challenges,
    notificationCount: state.homeReducer.notification.NotificationCount,
    approvalCount: state.homeReducer.notification.ApprovalCount,
    searchData: state.homeReducer.serachData,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getChallengeInfoById, updateAPIStatusBody }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Challenges);

const styles = StyleSheet.create({
  mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  resultNotFoundStyle:{
    fontSize: fonts.sm + 5,
  },
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },
  count: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1,
    marginLeft: 2
  },
  activityIndicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5FCFF88"
  },
  padding: { paddingBottom: 90 },
  parentSearchBar: {
    width: '99%',
    flexDirection: 'row',
    marginLeft: 5
  },
  childSearchBar: {
    width: '64%',
    margin: '3%',
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    width: '92%'
  },
  parentIconBar: {
    width: '100%',
    flexDirection: 'row'
  },
  childIconBar: {
    width: '34%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  childViewBadge: {
    width: '34%',
    margin: '3%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  filterIcon: {
    fontSize: fonts.lg + 2,
    color: 'grey',
    left: 20,
    bottom: 8
  },
  color: {
    backgroundColor: '#FFFFFF',
  },
  resSearchData: {
    paddingBottom: Platform.OS === 'android' ? 90 : 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
})