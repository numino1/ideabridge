import React, { Component } from 'react';
import { ScrollView, Platform, StyleSheet, Text, View, FlatList, TextInput, Icon } from 'react-native';
import { systemWeights } from 'react-native-typography';
import CardMyIdea from '../components/CardMyIdea';
import { fonts } from '../css/GlobalCss'

class SimilarIdeas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{ text1: "Text", text2: "Name", text3: "Date" }]
    };
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.elementsContainer}>
          <Text style={[systemWeights.bold, styles.headerText]}>Similar Ideas</Text>
          <Text style={styles.countStyle}> (10)</Text>
        </View>

        <View style={styles.flatListViewStyle}>
          <FlatList
            data={this.state.data}
            renderItem={({ item: rowData }) => {
              return (
                <CardMyIdea rowDatab={rowData} />
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </ScrollView>
    );
  }
}

export default SimilarIdeas;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginTop: -30
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },
  headerText: {
    fontSize: fonts.xLg,
    color: 'black'
  },
  countStyle: {
    fontSize: fonts.sm,
    color: 'black', top: 20
  },
  flatListViewStyle: {
    paddingBottom: 30
  }
})