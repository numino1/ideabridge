import React, { Component } from 'react';

import {
  ScrollView, DeviceInfo, FlatList, StyleSheet, TouchableOpacity, View, Text,
  Dimensions, ActivityIndicator, BackHandler, Platform, RefreshControl, TextInput, Keyboard
} from 'react-native';
import { systemWeights } from 'react-native-typography';
import { Avatar, Card, SearchBar, Icon } from 'react-native-elements';
import { ActionPicker } from 'react-native-action-picker';
import { LinearGradient, Stop, Defs, Text as SVGtext } from 'react-native-svg';
import { BarChart, XAxis } from 'react-native-svg-charts';
import Tags from "react-native-tags";
import IconBadge from 'react-native-icon-badge';
import { getIdeaInfoById, getChallengeInfoById } from "../actions/actions";
import * as scale from 'd3-scale';
import BarChart1 from '../components/BarChart';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import { updateAPIStatusBody, getCreatFormJsonForChallenges, getCreatFormJson } from '../actions/actions';
import Box from '../components/Boxes';
import { homeSearch, getHomePageContent, getIdeas, getChallenges, getPendingTasksForLoggedInUser, getNotificationApproval } from '../actions/home';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { NavigationEvents } from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import OfflineNotice from '../components/OfflineNotice';
import { fonts, dimensions, marginOffset, shadow } from '../css/GlobalCss';

const tags = ["Innovation", "Idea", "Material", "Manufacturing", "Design", "Software", "Innovation", "Idea", "Material", "Manufacturing", "Design", "Software", "Innovation", "Idea", "Material", "Manufacturing", "Design", "Software"];

class Home extends Component {

  static navigationOptions = {
    headerLeft: null,
    header: null,
    gesturesEnabled: false
  };

  constructor(props) {
    super(props);

    this.state = {
      homePageCached: false,
      data: null,
      search: '',
      isModalVisible: false,
      loading: false,
      text: '',
      hideBadge: false,
      myIdeas: 0,
      crossbutton: false,
      myChallenges: 0,
      countSear: 0,
      dataIdea: [],
      dataChallenge: [],
      limitIdeas: 3,
      limitCh: 3,
      flatListKey: false,
      isEmptyText: false,
      count: 20,
      refreshing: false,
      searchBarFocused: false,
      isBlur: false
    };
  }



  _onBlurr = () => {
    BackHandler.removeEventListener('hardwareBackPress',
      this._handleBackButtonClick);
  }

  _onFocus = () => {
    BackHandler.addEventListener('hardwareBackPress',
      this._handleBackButtonClick);
  }

  _handleBackButtonClick = () => {
    BackHandler.exitApp();
    return true;
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.collectFormData();
    this.initiateHomeAction();
  }

  initiateHomeAction() {
    this.props.getHomePageContent();
    this.props.getNotificationApproval();
    this.props.getIdeas();
    this.props.getChallenges();
    this.props.getPendingTasksForLoggedInUser();
  }

  collectFormData() {
    this.props
      .getCreatFormJson()
      .then(() => {
        if (this.props.success) {
          console.log("in getCreatFormJson props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getCreatFormJson props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });

    this.props
      .getCreatFormJsonForChallenges()
      .then(() => {
        if (this.props.success) {
          console.log("in getCreatFormJsonForChallenges props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getCreatFormJsonForChallenges props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  onFocus = () => {
    this.setState({
      searchBarFocused: true
    })

    this.setState({
      isBlur: false
    })
  }

  onChangeText = (text) => {
    this.setState({ text })
  }

  onBlur = () => {
    this.setState({
      searchBarFocused: false
    })

    this.setState({
      isBlur: true
    })

    this.setState({
      text: ''
    })
  }

  onClearSearchText = () => {
    this.setState({
      text: ''
    })
  }

  search = () => {
    this.setState({
      loading: true,
    })
    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 1000);
  }

  onRefresh = () => {
    this.setState({ homePageCached: true });
    this.initiateHomeAction();
  }

  handleOption(id) {
    this.setState({ isModalVisible: !this.state.isModalVisible });
    switch (id) {
      case 1:
        this.props.navigation.navigate('ViewPagerAddChallenge');
        break;
      case 2:
        this.props.navigation.navigate('ViewPager');
        break;
    }
  }

  updateSearch = search => {
    this.setState({ search });
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  // Generate ActionPicker Options
  createOptions = () => {
    return [
      { label: 'Submit a Challenge', action: () => this.handleOption(1) },
      { label: 'Submit an Idea', action: () => this.handleOption(2) }
    ];
  }

  handleKeyDown = () => {
    if (this.state.text) {
      const searchText = {
        'text': this.state.text
      }
      let text = this.state.text;
      this.onClearSearchText();
      this.props.navigation.navigate('SearchScreen', { text: text });
    }
  }

  renderSpinner() {
    if (!this.state.homePageCached && this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  getIdeaByIdAPI(contentId) {
    this.props.getIdeaInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getIdeaByIdAPI props success");
          let ideaData = {
            data: this.props.IdeaInfoById.ContentData,
            costDetailsData: this.props.IdeaInfoById.CostDetails,
            challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
            canRespond: this.props.IdeaInfoById.CanRespond,
            canEdit: this.props.IdeaInfoById.CanEdit,
          };
          if (this.props.userDetails.UserID == this.props.IdeaInfoById.CreatorUserId) {
            ideaData.contentId = this.props.IdeaInfoById.ContentID;
            ideaData.contentStatus = this.props.IdeaInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewIdea", ideaData);
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getIdeaByIdAPI props error",
            this.props.errorMsg
          );
          //alert(this.props.errorMsg);
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  getChallengeByIdAPI(contentId) {

    this.props.getChallengeInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getChallengeByIdAPI props success");
          let challengeData = {
            data: this.props.ChallengeInfoById.ContentData,
            title: this.props.ChallengeInfoById.Title,
            attachments: this.props.ChallengeInfoById.Attachment,
            canRespond: this.props.ChallengeInfoById.CanRespond,
          };
          if (this.props.userDetails.UserID == this.props.ChallengeInfoById.ChallengerDetails.UserID) {
            challengeData.contentId = this.props.ChallengeInfoById.ContentID;
            challengeData.contentStatus = this.props.ChallengeInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewChallenge", challengeData);

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getChallengeByIdAPI props error",
            this.props.errorMsg
          );
          //alert(this.props.errorMsg);
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });

  }

  listItemClicked(itemData) {
    this.getIdeaByIdAPI(itemData.ContentID);
  }

  listItemClickedChallenges(itemData) {
    this.getChallengeByIdAPI(itemData.ContentID);
  }
  render() {
    let notificationCount = 0;
    let approvalCount = 0;
    const { content, userDetails, notification } = this.props;

    if (userDetails.userID) {
      this.userID = userDetails.userID
    }

    if (notification.NotificationCount) {
      notificationCount = notification.NotificationCount;
    }
    if (notification.ApprovalCount) {
      approvalCount = notification.ApprovalCount;
    }
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.homePageCached && this.props.loading}
            onRefresh={this.onRefresh}
          />
        }
        pointerEvents={this.props.loading ? "none" : "auto"}
        style={this.props.loading ? styles.overlay : styles.container} >

        <NavigationEvents
          onWillFocus={this._onFocus}
          onWillBlur={this._onBlurr}
        />
        <OfflineNotice />
        <View pointerEvents={this.props.loading ? "none" : "auto"} style={[{ flex: 1 }, styles.elementsContainer1]}>

          <View style={styles.parentHeader} >
            <Text allowFontScaling style={[systemWeights.bold, styles.header]}>IdeaBridge</Text>
          </View>

          <View style={styles.profile} >
            <Avatar rounded
              source={require('../../images/user1.png')}
              onPress={() => this.props.navigation.navigate('Profile')} />
          </View>
        </View>

        <View style={styles.parentSearch} pointerEvents={this.props.loading ? "none" : "auto"}>
          <View style={[styles.childSearch, { width: this.state.searchBarFocused ? '98%' : '58%' }]}>
            <Ionicons name={"ios-search"} style={{ fontSize: fonts.lg - 1, color: 'black' }} />
            <TextInput placeholder="Search" style={{ fontSize: fonts.md - 3, marginLeft: '3%', flex: 1 }}
              placeholderTextColor='black'
              onFocus={this.onFocus}
              onChangeText={this.onChangeText}
              value={this.state.text}
              caretHidden={this.state.isBlur ? true : false}
              onSubmitEditing={this.handleKeyDown}
              onBlur={this.onBlur}
            />
            {this.state.searchBarFocused ? <MaterialIcons name='clear' style={{ fontSize: fonts.lg, color: 'black' }} onPress={() => { this.onBlur(); }} /> : null}
          </View>
          {!this.state.hideBadge && this.state.text == '' &&
            <View style={[styles.childViewBadge]} pointerEvents={this.props.loading ? "none" : "auto"} >
              <View style={[styles.parentIconBar]}>
                <View style={[styles.childIconBar]}>
                  <IconBadge
                    MainElement={
                      <View style={{
                        backgroundColor: '#EEEEEE',
                        justifyContent: 'center',
                        width: 30,
                        height: 25,
                        margin: 3,

                      }}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Notifications') }}>
                          <Icon
                            name='bells'
                            type='antdesign'
                            iconStyle={{ height: 45, width: 35, fontSize: fonts.md + 3 }}

                          >
                          </Icon>
                        </TouchableOpacity>
                      </View>
                    }
                    BadgeElement={notificationCount == 0 ? null :
                      <Text style={{ color: '#FFFFFF', fontWeight: Platform.OS === 'ios' ? '400' : '200' }}>{notificationCount}</Text>
                    }

                    IconBadgeStyle={
                      {
                        flex:1,
                        flexDirection:'row',
                        flexWrap:'wrap',
                        maxWidth: 40,//25,
                        minWidth: 25,
                        padding:1,
                        height: notificationCount == 0 ? 0 : 25,//30,
                        top: -17,
                        right: -10,
                        borderRadius: 20,
                        backgroundColor: 'red'
                      }
                    }

                    Hidden={this.state.badgeCount == 0}
                  />
                </View>

                {userDetails != '' && userDetails.UserRoleList.length > 1 && <View style={[styles.childIconBar]}>
                  <IconBadge
                    MainElement={
                      <View style={{
                        backgroundColor: '#EEEEEE',
                        width: 30,
                        height: 40,
                        margin: 3
                      }}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Approvals') }}>
                          <Icon
                            name='check-circle'
                            type='feather'
                            iconStyle={{ height: 30, width: 35, fontSize: fonts.md + 3, backgroundColor: '#EEEEEE' }}
                            disabled

                          >
                          </Icon>
                        </TouchableOpacity>
                      </View>
                    }
                    BadgeElement={approvalCount == 0 ? null :
                      <Text style={{ color: '#FFFFFF', fontWeight: Platform.OS === 'ios' ? '400' : '200' }}>{approvalCount}</Text>
                    }
                    IconBadgeStyle={
                      {
                        flex:1,
                        flexDirection:'row',
                        flexWrap:'wrap',
                        maxWidth: 40,//25,
                        minWidth: 25,
                        padding:1,
                        height: approvalCount == 0 ? 0 : 25,
                        top: -10,
                        right: -16,
                        borderRadius: 20,
                        backgroundColor: 'red'
                      }
                    }
                    Hidden={this.state.badgeCount == 0}
                  />
                </View>}

              </View>
            </View>}
          {/* </View> */}
        </View>


        {content.UserContentsCount ? (
          <View style={[styles.parent]} pointerEvents={this.props.loading ? "none" : "auto"} >
            <Box color='#5AC8FB' listData={[]} count={content.UserContentsCount.Challenges} route='Challenges' title={'Challenges'} navigation={this.props.navigation} />
            <Box color='#8483E1' listData={[]} count={content.UserContentsCount.Ideas} route='Ideas' title={'Ideas'} navigation={this.props.navigation} />
            <Box color='#007AFF' listData={[]} count={content.UserContentsCount.MyChallenges} route='MyChallenges' title={'My Challenges'} navigation={this.props.navigation} />
            <Box color='#5855D6' listData={[]} count={content.UserContentsCount.MyIdeas} route='MyIdeas' title={'My Ideas'} navigation={this.props.navigation} />
          </View>) : (
            <View style={[styles.parent]} pointerEvents={this.props.loading ? "none" : "auto"}>
              <Box pointerEvents={this.props.loading ? "none" : "auto"} color='#8483E1' listData={[]} count={0} route='Challenges' title={'Challenges'} navigation={this.props.navigation} />
              <Box pointerEvents={this.props.loading ? "none" : "auto"} color='#8483E1' listData={[]} count={0} route='Ideas' title={'Ideas'} navigation={this.props.navigation} />
              <Box pointerEvents={this.props.loading ? "none" : "auto"} color='#8483E1' listData={[]} count={0} route='MyChallenges' title={'My Challenges'} navigation={this.props.navigation} />
              <Box pointerEvents={this.props.loading ? "none" : "auto"} color='#5855D6' listData={[]} count={0} route='MyIdeas' title={'My Ideas'} navigation={this.props.navigation} />
            </View>
          )}

        {this.renderSpinner()}

        <TouchableOpacity style={[styles.addButton]} onPress={() => { this.toggleModal() }} disabled={this.props.loading ? true : false}  >
          <AntDesign name='plus' style={{ fontSize: fonts.lg + 5, color: '#ffffff' }} />
          {/* </View> */}
        </TouchableOpacity>
        <View style={[styles.elementsContainer, { marginTop: 30 }]}>
        </View>

        {content.RecentChallenges ? (
          <View style={[styles.elementsContainer, { marginTop: 10 }]}>
            <Text style={[systemWeights.bold, { fontSize: fonts.sm + 3, color: 'black' }]}>Recent Challenges</Text>
          </View>
        ) : (<View></View>)}

        {content.RecentChallenges ? (
          <View style={{ height: 180 }}>
            <FlatList
              horizontal
              style={{ padding: 10 }}
              key={content.RecentChallenges.length}
              data={content.RecentChallenges}
              renderItem={({ item: rowData }) => {
                return (
                  <TouchableOpacity onPress={() => { this.listItemClickedChallenges(rowData) }}>
                    <Card title={null}
                      containerStyle={[styles.card]}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={[systemWeights.semibold, styles.bar]} />
                        <Text style={{ marginBottom: 10, color: 'darkgrey', fontSize: fonts.sm + 1 }}>
                          {rowData.ContentStatus}
                        </Text>
                      </View>

                      <View >
                        <Text style={[systemWeights.semibold, styles.categoryName]} numberOfLines={2}>
                          {rowData.CategoryName}
                        </Text>
                      </View >

                      <View style={styles.title}>
                        <Text style={[systemWeights.semibold, styles.t]}>
                          {rowData.Title}
                        </Text>
                      </View >

                      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        {rowData.DaysLeftForParticipation == 0 ?
                          (<Text style={{ color: 'darkgrey' }}>
                            Ended
                        </Text>) :
                          (<Text style={{ color: 'darkgrey' }}>
                            Ending In <Text style={{ color: 'darkgrey', fontWeight: '800' }}>{rowData.DaysLeftForParticipation} days </Text>
                          </Text>)}
                      </View>
                    </Card>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>) : (<View></View>)
        }

        {content.RecentIdeas ? (
          <View style={[styles.elementsContainer, { marginTop: 10 }]}>
            <Text style={[systemWeights.bold, { fontSize: fonts.sm + 3, color: 'black' }]}>Recent Ideas</Text>
          </View>
        ) : (<View></View>)}

        {content.RecentIdeas ? (
          <View style={{ height: 180 }}>
            <FlatList
              horizontal
              style={{ padding: 10 }}
              key={content.RecentIdeas.length}
              data={content.RecentIdeas}
              renderItem={({ item: rowData }) => {
                return (
                  <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                    <Card title={null}
                      containerStyle={[styles.card]}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={[systemWeights.semibold, styles.bar]} />
                        <Text style={{ marginBottom: 10, color: 'darkgrey', fontSize: fonts.sm + 1 }}>
                          {rowData.ContentStatus}
                        </Text>
                      </View>

                      <View >
                        <Text style={[systemWeights.semibold, styles.categoryName]} numberOfLines={2}>
                          {rowData.CategoryName}
                        </Text>
                      </View >

                      <View style={styles.title}>
                        <Text style={[systemWeights.semibold, styles.t]}>
                          {rowData.Title}
                        </Text>
                      </View >

                      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ color: 'darkgrey' }}>
                          {rowData.IdeatorName}
                        </Text>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={{ color: 'darkgrey' }}>
                            {rowData.LikeCount}
                          </Text>
                          <EvilIcons name='like' style={{ fontSize: fonts.md + 1 }} />
                        </View>
                      </View>
                    </Card>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>) : (<View></View>)
        }

        <View style={[styles.elementsContainer, { marginTop: 20, marginLeft: -20, backgroundColor: '#D3D3D3' }]}>
        </View>
        <View style={styles.ideaContainer}>
        </View>

        <View style={[styles.elementsContainer, { marginTop: 20, marginLeft: -20, }]}>
        </View>
        <View style={styles.challengeContainer}>
        </View>

        <ActionPicker
          options={this.createOptions()}
          isVisible={this.state.isModalVisible}
          onCancelRequest={this.toggleModal} />

      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.verifyReducer.userDetails,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
    content: state.homeReducer.content,
    notification: state.homeReducer.notification,
    searchData: state.homeReducer.serachData,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getCreatFormJsonForChallenges, getCreatFormJson, getIdeaInfoById, getChallengeInfoById, getHomePageContent, updateAPIStatusBody, homeSearch, getIdeas, getChallenges, getPendingTasksForLoggedInUser, getNotificationApproval }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0.5,
    backgroundColor: '#F0F8FF',
    width: dimensions.fullWidth
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row'
  },
  elementsContainer1: {
    marginTop: marginOffset.top,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  parentHeader: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    fontSize: 0.055 * dimensions.fullHeight,
    color: '#000000',
    right: 20,
    left: 0
  },
  profile: {
    flex: 2,
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
    left: 15
  },
  parent: {
    top: 10,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },

  parentIconBar: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  childIconBar: {
    width: '44%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  childViewBadge: {
    width: '28%',
    marginLeft: '69%',
    margin: '3%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
    height: 60
  },
  addButton: {
    backgroundColor: '#F51173',
    top: 15,
    height: 0.1 * dimensions.fullHeight,
    width: '94%',
    marginLeft: 10,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ideaContainer: {
    backgroundColor: '#D3D3D3'
  },
  challengeContainer: {
  },
  activityIndicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5FCFF88"
  },
  bar: {
    width: 4,
    height: 20,
    backgroundColor: '#FFFF00',
    left: -10
  },
  parentSearch: {
    height: 80,
    backgroundColor:
      '#ffffff',
    paddingHorizontal: 5,
    justifyContent: 'center'
  },
  childSearch: {
    height: 60,
    backgroundColor: '#EEEEEE',
    borderRadius: 5,
    flexDirection: 'row',
    padding: 5,
    alignItems: 'center',
    zIndex: 1,
    top: 10,
    left: 10,
    position: 'absolute',
  },

  card: {
    ...shadow,
    padding: 10,
    width: 250,
    borderRadius: 4,
    height: 130
  },
  categoryName: {
    fontSize: fonts.sm,
    color: 'darkgrey',
    marginLeft: 6,
    marginTop: -8
  },
  title: {
    height: 0.05 * dimensions.fullHeight,
    marginTop: 15
  },
  titleText: {
    fontSize: fonts.sm + 3,
    color: 'black',
    marginLeft: 6
  }
});