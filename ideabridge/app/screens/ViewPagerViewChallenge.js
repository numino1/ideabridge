import React, { Component } from 'react';
import { View, Button, Dimensions, Platform, StyleSheet, TextInput, PermissionsAndroid, TouchableOpacity, Keyboard, ActivityIndicator, Alert, ScrollView, FlatList, ProgressBarAndroid, ProgressViewIOS } from 'react-native';
import { Header, Icon, Text, Card } from 'react-native-elements';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import { bindActionCreators } from "redux";
import Toast, { DURATION } from 'react-native-easy-toast';
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import data from './apiResponse';
import DynamicForm, { buildTheme } from 'react-native-dynamic-form';
import OfflineNotice from '../components/OfflineNotice';
import IconBadge from 'react-native-icon-badge';
import { homeSearch, getHomePageContent, getIdeas, getChallenges } from '../actions/home';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Doodle from './Doodle';
import DocumentPicker from 'react-native-document-picker';
let _ = require('lodash');
var RNFS = require('react-native-fs');
import axios from "axios";
import ImagePicker from 'react-native-customized-image-picker';
import { CLIENT_UAT1_URL } from '../config/configuration';
import { DOC_TYPES } from '../config/attachmentDocTypes';
import Moment from 'moment';
import { fonts, fontStyle, dimensions } from '../css/GlobalCss'
import handleError from '../config/errorHandler';

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height
const ELEMENT = {
  TextBox: "text",
  Number: "number",
  TextArea: "textarea",
  SourceDropdown: "select",
  Dropdown: "select",
  ChallengeTypeDropdown: 'select',
  SourceBoundMultiselectDropdown: 'select',
  CheckBox: "checkbox-group",
  RadioButton: 'radio-group',
  Switch: 'checkbox-group',
  FileUpload: 'text',
  UserField: 'tags',
  TagsField: 'tags',
  DateRange: 'text',
  Caption: 'paragraph',
};

class ViewPagerViewChallenge extends Component {
  formArray = [];
  requiredFieldsArray = [];
  propertiesArray = [];
  fieldsWithValueArray = [];
  tagsList = [];
  challengeLocation = {};
  challengeCategory = {};
  challengeSubCategory = {};
  challengeType = '';
  saveIdeaDetails = {
    FormId: 59,
    Properties: [],
    ContentId: 0,
    ContentTypeId: 2,
    ProjectTypeId: 0,
    ContentType: null,
    DoSubmit: true,
    ParentContentId: 0,
    Files: [],
    SubForms: null,
    Name: null,
    HasVerticalTabs: false,
    GlobalOptions: null,
    IsActive: false,
    UserTypeId: 0,
    Sequence: 0,
  };
  attachmentFileList = [];
  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      vpPagePosition: 0,
      fieldValues: [],
      elementDisableForCostDetails: true,
      elementDisableForImplement: true,
      elementDisableForText: true,
      disableSaveButton: true,
      disableSubmitButton: true,
      hideBadge: true,
      badgeCount: 0,
      editButtonClicked: false,
      allTagsList: [],
      peopleTagsList: [],
      tagsSuggestions: [],
      perPageRequiredStatus: { Overview: true, Details: true },
      contentId: 0,
      contentStatus: '',
      selectedCategory: 0,
      formDataModified: false,
      footerVisible: false,
      fileattached: false,
      fileName: '',
      refresh: false,
      attachmentLoadComplete: true,
      attachmentStatus: true,
      progressIncrement: 0,
      title: '',
      type: '',
      contentIDForIdeasOnChallenge: 0,
      attachDetails: {},
      showReviewerInvite: false,
      inviteCounter: 0,
    };
  }

  onAttachment = data => {
    this.setState(data);
    let responseObj = data.attachDetails;
    let fileSize = data.attachDetails.size;
    this.readFileAndAttach(responseObj, fileSize);
  };

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardWillShow",
      this._keyboardDidShow.bind(this)
    );
    const { navigation } = this.props;
    const data = navigation.getParam('data', 'NO-data');
    const attachments = navigation.getParam('attachments', 'NO-data');
    if (attachments.length > 0) {
      this.updateAttachmentList(attachments);
    }
    this.setState({ fieldValues: data, refresh: true });

    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    this.setState({ contentId: contentId });

    const contentIDForIdeasOnChallenge = data.ContentID.Value;
    this.setState({ contentIDForIdeasOnChallenge: contentIDForIdeasOnChallenge });
    this.props.getIdeasOnChallenge(contentIDForIdeasOnChallenge);


    const contentStatus = this.props.navigation.getParam('contentStatus', 'NO-data');
    this.setState({ contentStatus: contentStatus });
    const title = this.props.navigation.getParam('title', 'NO-data');
    this.setState({ title: title });
    const type = this.props.navigation.getParam('type', 'NO-data');
    this.setState({ type: type });
    const showReviewerInvite = this.props.navigation.getParam('showReviewerInvite', 'NO-data');
    this.setState({ showReviewerInvite: showReviewerInvite });
  }

  componentWillUnmount() {
    this.props
      .clearSubCategoryValues();
  }

  _keyboardDidShow() {
    if (!this.state.editButtonClicked) {
      Keyboard.dismiss();
    }
  }

  initiateHomeAction() {
    this.props.getHomePageContent()
      .then(() => {
        if (this.props.success) {
          console.log("in getHomePageContent props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }

        if (this.props.error) {
          console.log(
            "in getHomePageContent props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });

    this.props.getIdeas()
      .then(() => {
        if (this.props.success) {
          console.log("in getIdeas props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }

        if (this.props.error) {
          console.log(
            "in getIdeas props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
    this.props.getChallenges()
      .then(() => {
        if (this.props.success) {
          console.log("in getChallenges props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }

        if (this.props.error) {
          console.log(
            "in getChallenges props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  getAllIdeaTags(text) {
    let self = this;
    let tagList = [];
    const url = CLIENT_UAT1_URL + `/Idea/IdeaTagAutoComplete`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: this.props.token
      },
      params: {
        term: text
      },
    };
    return axios
      .get(url, config)
      .then(function (response) {
        if (response.data.length > 0) {
          response.data.map((item, i) => {
            tagList.push(
              {
                name: item.TagName,
                // value: item.TagId
              });
          });
        }
        self.setState({ tagsSuggestions: tagList });
      })
      .catch(function (error) {
        console.log("In getAllAutoCompleteTags", error);
        self.setState({ tagsSuggestions: [] });
        handleError(error, false);
      });
  }

  getAllPeopleEmail(text) {
    let self = this;
    let tagList = [];
    const url = CLIENT_UAT1_URL + `/User/PeopleSearch`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: this.props.token
      },
      params: {
        term: text,
        excludeIDs: '0',
        formPropertyId: 24,
        includeSubmittingUser: false
      },
    };

    return axios
      .get(url, config)
      .then(function (response) {
        if (response.data.length > 0) {
          response.data.map((item, i) => {
            tagList.push(
              {
                name: item.info,
                value: item.id,
              });
          });
        }
        self.setState({ peopleTagsList: tagList });
      })
      .catch(function (error) {
        console.log("In getAllTeamMembers", error);
        self.setState({ peopleTagsList: [] });
        handleError(error, false);
      });
  }

  customFilterDataTags = text => {
    this.getAllIdeaTags(text);
  }

  customFilterDataTeamMembers = (text) => {
    this.getAllPeopleEmail(text);
  }

  showIconBadge() {
    if (!this.state.hideBadge) {
      const taskId = this.props.navigation.getParam('taskId', 'NO-data');
      return (
        <View style={[styles.childViewBadge, { backgroundColor: '#EEEEEE', height: 45 }]}>
          <View style={[styles.parentIconBar,]}>
            <View style={[styles.childIconBar,]}>
              <IconBadge
                MainElement={
                  <View style={{
                    backgroundColor: '#EEEEEE',
                    width: 30,
                    height: 25,
                    margin: 6
                  }} >
                    <Icon

                      name='lightbulb-on-outline'
                      type='material-community'
                      iconStyle={{ height: 40, width: 30, fontSize: 28, color: 'black' }}
                      onPress={() => { this.keyboardDidShowListener.remove(); this.props.navigation.navigate('IdeasForChallenge', { challengeTitle: this.state.title }); }}

                    >
                    </Icon>
                  </View>
                }
                // BadgeElement={
                //   <Text style={{ color: '#FFFFFF', fontWeight: '900' }}>{this.state.badgeCount}</Text>
                // }
                IconBadgeStyle={
                  {
                    width: 32,
                    height: 0,
                    right: -20,
                    borderRadius: 12,
                    backgroundColor: 'red',
                    borderColor: '#EEEEEE',
                    borderWidth: 0.8
                  }
                }
                Hidden={this.state.badgeCount == 0}
              />
            </View>
            <View style={[styles.childIconBar]}>
              <IconBadge
                MainElement={
                  <View style={{
                    backgroundColor: '#EEEEEE',
                    width: 30,
                    height: 25,
                    margin: 6
                  }}>
                    <Icon
                      name='message-text-outline'
                      type='material-community'
                      iconStyle={{ fontSize: 30 }}
                      onPress={() => { this.keyboardDidShowListener.remove(); this.props.navigation.navigate('Comments', { type: 'Challenge' }); }}
                    >
                    </Icon>
                  </View>
                }
                BadgeElement={
                  <Text style={{ color: '#FFFFFF', fontWeight: '900' }}>{this.state.badgeCount}</Text>
                }
                IconBadgeStyle={
                  {
                    width: 32,
                    height: 25,
                    top: -1,
                    right: -25,
                    borderRadius: 12,
                    backgroundColor: 'red',
                    borderColor: '#EEEEEE',
                    borderWidth: 0.8
                  }
                }
                Hidden={this.state.badgeCount == 0}
              />
            </View>
            <View style={[styles.childIconBar]}>
              <IconBadge
                MainElement={
                  <View style={{
                    backgroundColor: '#EEEEEE',
                    width: 30,
                    height: 25,
                    margin: 6
                  }} >
                    <Icon
                      name='calendar'
                      type='antdesign'
                      iconStyle={{ height: 40, width: 30, fontSize: 25, color: 'black' }}
                      onPress={() => { this.props.navigation.navigate('Activities', { type: 'Challenge' }) }}
                    >
                    </Icon>
                  </View>
                }
                // BadgeElement={
                //   <Text style={{ color: '#FFFFFF', fontWeight: '900' }}>{this.state.badgeCount}</Text>
                // }
                IconBadgeStyle={
                  {
                    width: 32,
                    height: 0,
                    right: -20,
                    borderRadius: 12,
                    backgroundColor: 'red',
                    borderColor: '#EEEEEE',
                    borderWidth: 0.8
                  }
                }
                Hidden={this.state.badgeCount == 0}
              />
            </View>
            <View style={[styles.childIconBar]}>
              <IconBadge
                MainElement={
                  <View style={{
                    backgroundColor: '#EEEEEE',
                    width: 30,
                    height: 25,
                    margin: 6
                  }} >
                    <Icon
                      name='infocirlceo'
                      type='antdesign'
                      iconStyle={{ height: 40, width: 30, fontSize: 25, color: 'black' }}
                      onPress={() => { this.props.navigation.navigate('ChallengeSummary', { taskId: taskId }) }}
                    >
                    </Icon>
                  </View>
                }
                // BadgeElement={
                //   <Text style={{ color: '#FFFFFF', fontWeight: '900' }}>{this.state.badgeCount}</Text>
                // }
                IconBadgeStyle={
                  {
                    width: 32,
                    height: 0,
                    right: -20,
                    borderRadius: 12,
                    backgroundColor: 'red',
                    borderColor: '#EEEEEE',
                    borderWidth: 0.8
                  }
                }
                Hidden={this.state.badgeCount == 0}
              />
            </View>
            <View style={[styles.childIconBar]}>
              <Icon
                name='cross'
                type='entypo'
                iconStyle={{ marginRight: 25, fontSize: 33 }}
                onPress={() => { console.log("in cross button"); this.setState({ hideBadge: true }) }}
              >
              </Icon>
            </View>
          </View>
        </View>
      )
    }
  }
  iconPressFunc() {
    console.log("in icon press func");
    this.setState({ hideBadge: false })
  }

  file() {
    console.log("in file");

    DocumentPicker.pick({
      type: DOC_TYPES,
    }).then((res, error) => {

      if (!error) {
        if (res.size < 5000000) {
          this.setState({ formDataModified: true });
          console.log("file type", res);
          if (this.attachmentFileList.findIndex(item => item.fileName === res.name) === -1) {
            this.setState({ fileattached: true });
            var path = res.uri;
            RNFS.readFile(path.replace(/%20/g, '\ '), 'base64')
              .then(responseString => {
                let fileObject = {
                  fileName: res.name,
                  fileByteString: responseString,
                  fileType: res.type,
                  fileId: 0,
                  key: this.attachmentFileList.length,
                  progressBarShow: false,
                  progressIncrement: 0,
                  success: true,
                  uploaded: false,
                };
                this.attachmentFileList.push(fileObject);
                this.setState({ refresh: true });
              })
              .catch((err) => {
                console.log("RNFS ReadFile Error", err);
                this.setState({ fileattached: false });
              });
          }
        } else {
          Alert.alert("Please upload a file of less than 5 MB");
        }
      } else {
        console.log("Document Picker Error", error);
      }
    });
  }

  imageNew() {
    ImagePicker.openPicker({
      isSelectBoth: true,
    }).then(imageInfo => {
      let image = '';
      if (Platform.OS === 'android') {
        //array of objects
        image = imageInfo[0];
      } else {
        //object
        image = imageInfo;
      }

      let fileSize = 0;
      let responseObj = image;
      if (image.size) {
        fileSize = image.size;
        this.readFileAndAttach(responseObj, fileSize);
      }
      else {
        RNFS.stat(image.path)
          .then((fileInfo) => {
            fileSize = fileInfo.size;
            this.readFileAndAttach(responseObj, fileSize);
          })
          .catch((error) => {
            console.log("RNFS File stat error in Image");
          })
      }

    }).catch(error => {
      console.log("ImagePicker.openPicker", error);
    });
  }

  readFileAndAttach(response, fileSize) {
    if (fileSize < 5000000) { //implement function and call in if else above as async call gets execited later
      this.setState({ formDataModified: true });

      let fileName = response.path.split('/').pop();

      if (this.attachmentFileList.findIndex(item => item.fileName === fileName) === -1) {
        this.setState({ fileattached: true });
        var path = response.path;
        RNFS.readFile(path.replace(/%20/g, '\ '), 'base64')
          .then(responseString => {
            let fileObject = {
              fileName: fileName,
              fileByteString: responseString,
              fileType: response.mime,
              fileId: 0,
              key: this.attachmentFileList.length,
              progressBarShow: false,
              progressIncrement: 0,
              success: true,
              uploaded: false,
            };
            this.attachmentFileList.push(fileObject);
            this.setState({ refresh: true });
          })
          .catch((err) => {
            console.log("RNFS ReadFile Error for Image", err);
            this.setState({ fileattached: false });
          });
      }
    } else {
      Alert.alert("Please upload a file of less than 5 MB");
    }
  }

  opendoodle() {
    this.props.navigation.navigate('Doodle', { position: this.props.position, onAttachment: this.onAttachment });
  }

  camera() {
    ImagePicker.launchCamera(options1, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
        const source = { uri: response.uri };
        this.setState({ avatarSourceCamera: source });
      }
    });
  }

  async openPhoneCamera() {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
        'title': 'External Storage Access',
        'message': 'App needs to access your external storage'
      })
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          compressQuality: 50,
          minCompressSize: 2048
        }).then(imageInfo => {
          let image = '';
          if (Platform.OS === 'android') {
            //array of objects
            image = imageInfo[0];
          } else {
            //object
            image = imageInfo;
          }

          let fileSize = 0;
          let responseObj = image;
          if (image.size) {
            fileSize = image.size;
            this.readFileAndAttach(responseObj, fileSize);
          }
          else {
            RNFS.stat(image.path)
              .then((fileInfo) => {
                fileSize = fileInfo.size;
                this.readFileAndAttach(responseObj, fileSize);
              })
              .catch((error) => {
                console.log("openPhoneCamera RNFS File stat error in Image");
              })
          }

        }).catch(error => {
          console.log("openPhoneCamera ImagePicker.openPicker===============>", error);
        });
      } else {
        Alert.alert("Permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }

  openPhoneCameraIOS() {

    ImagePicker.openCamera({
      compressQuality: 50,
      minCompressSize: 2048,
      openCameraOnStart: true,
    }).then(imageInfo => {
      let image = '';
      if (Platform.OS === 'android') {
        //array of objects
        image = imageInfo[0];
      } else {
        //object
        image = imageInfo;
      }

      let fileSize = 0;
      let responseObj = image;
      if (image.size) {
        fileSize = image.size;
        this.readFileAndAttach(responseObj, fileSize);
      }
      else {
        RNFS.stat(image.path)
          .then((fileInfo) => {
            fileSize = fileInfo.size;
            this.readFileAndAttach(responseObj, fileSize);
          })
          .catch((error) => {
            console.log("openPhoneCamera RNFS File stat error in Image");
          })
      }

    }).catch(error => {
      console.log("openPhoneCamera ImagePicker.openPicker===============>", error);
    });
  }


  toggle() {
    this.setState({ footerVisible: !this.state.footerVisible })
  }

  removeattachment(rowData) {
    //remove from list 
    this.attachmentFileList = this.attachmentFileList.filter((item) => { return item.fileName != rowData.fileName });
    if (this.attachmentFileList.length == 0) {
      this.setState({ fileattached: false });
    }
    this.setState({ refresh: true });
    if (rowData.fileId == 0) {
      this.refs.toast.show('The attachment has been removed successfully.', 500, () => {
      });
    }
    //make remove api call only if rowData.fileId exists
    if (rowData.fileId != 0) {
      this.props.removeAttachmentFile({
        data: rowData.fileId
      })
        .then(() => {
          if (this.props.success) {
            console.log("in removeAttachmentFile props success", this.props.contentId);
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('The attachment has been removed successfully.', 500, () => {
            });
          }
          if (this.props.error) {
            console.log(
              "in removeAttachmentFile props error",
              this.props.errorMsg
            );
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }
  }

  refreshAttachment(rowData) {
    let fileObject = {
      'contentFileName': rowData.fileName,
      'contentFileBytes': rowData.fileByteString,
      'fileType': rowData.fileType,
      'key': rowData.key,
      'contentId': this.state.contentId
    }
    let status = this.fileAttachAPI(fileObject);
  }

  downloadAttachment(fileID, filename) {
    this.props.downloadAttachments(fileID, filename)
      .then(() => {
        if (this.props.error) {
          console.log(
            "in downloadAttachment props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }


  showIconByFileType(filetype, fileName) {
    switch (filetype) {
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
      case "org.openxmlformats.wordprocessingml.document":
      case "com.microsoft.word.doc":
      case "application/msword":
        {
          return (
            <View >
              <AntDesign style={styles.fileIconStyle} name='wordfile1' />
            </View>
          );
        }
      case "org.openxmlformats.spreadsheetml.sheet":
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
      case "com.microsoft.excel.xls":
      case "application/vnd.ms-excel":
        {
          return (
            <View >
              <FontAwesome style={styles.fileIconStyle} name='file-excel-o' />
            </View>
          );

        }
      case "org.openxmlformats.presentationml.presentation":
      case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
      case "com.microsoft.powerpoint.​ppt":
      case "application/vnd.ms-powerpoint":
        {
          return (
            <View >
              <AntDesign style={styles.fileIconStyle} name='pptfile1' />
            </View>
          );
        }
      case "public.plain-text":
      case "text/plain":
      case "public.text":
      case "text/rtf":
        {
          return (
            <View >
              <Entypo style={styles.fileIconStyle} name='text-document' />
            </View>);
        }
      case "com.adobe.pdf":
      case "application/pdf":
        {
          return (
            <View >
              <AntDesign style={styles.fileIconStyle} name='pdffile1' />
            </View>);
        }
      case "public.jpeg":
      case "public.png":
      case "com.microsoft.bmp":
      case "com.compuserve.gif":
      case "image/jpeg":
      case "image/gif":
      case "image/png":
      case "image/bmp":
        {
          return (
            <View >
              <Entypo style={styles.fileIconStyle} name='image' />
            </View>);
        }
      case "video/mp4":
      case "video/mpeg":
      case "public.mpeg-4":
        {
          return (
            <View >
              <Entypo style={styles.fileIconStyle} name='video' />
            </View>);
        }
      default:
        {
          return (
            <View >
              <Entypo style={styles.fileIconStyle} name='attachment' />
            </View>
          );
        }
    }
  }

  renderAttachmentButton() {
    if (!this.state.footerVisible) {
      return (
        <TouchableOpacity style={[styles.floatingButton, { bottom: Platform.OS === 'ios' ? 170 : 194, }]} onPress={() => this.toggle()} >
          <AntDesign name='plus' style={{ fontSize: 25, color: '#0086b3' }} />
        </TouchableOpacity>);
    }
    else {
      return (
        <View>
          <View style={[styles.attachmentRibbon, !this.state.fileattached ? { bottom: 90 } : { bottom: 90 * 2 }]} >
            <Text style={{ color: 'white', fontSize: 24, marginTop: 5, marginLeft: 25 }}>Attachments</Text>
          </View>
          {this.state.fileattached &&
            <View style={[styles.footer, { bottom: 90, height: 90 }]}>

              <FlatList
                contentContainerStyle={{ paddingBottom: 20 }}
                style={{ padding: 10 }}
                key={this.attachmentFileList.length}
                data={this.attachmentFileList}
                extraData={this.state.refresh}
                renderItem={({ item: rowData }) => {
                  return (

                    <View pointerEvents={this.props.loading ? "auto" : "auto"} style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', height: 30, borderRadius: 20, borderColor: 'black', borderWidth: 0.3, marginTop: 5 }}>
                      {this.showIconByFileType(rowData.fileType, rowData.fileName)}
                      <Text numberOfLines={1} ellipsizeMode='head' style={{ flexGrow: 1, width: 0, flexDirection: "column", justifyContent: "center", fontWeight: 'bold', alignItems: 'center', fontSize: 16 }}>{rowData.fileName} </Text>
                      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: 30 }}>
                        {(Platform.OS === 'android')
                          ?
                          (rowData.progressBarShow && <ProgressBarAndroid progressTintColor="green" progress={rowData.progressIncrement} styleAttr="Horizontal" indeterminate={false} />)
                          :
                          (rowData.progressBarShow && <ProgressViewIOS style={{ transform: [{ scaleX: 1.0 }, { scaleY: 4 }], height: 12, width: 100 }} progressTintColor="green" progress={rowData.progressIncrement} />)
                        }
                        {rowData.success == false && rowData.progressBarShow == false && <Text style={{ alignItems: 'center', fontSize: 14, color: 'red' }}>Failed </Text>}
                        {rowData.success == false && rowData.progressBarShow == false && <FontAwesome style={{ color: '#0086b3', fontWeight: 'bold', alignItems: 'center', fontSize: 18, marginRight: 10, marginLeft: 10 }} name='refresh' onPress={() => this.refreshAttachment(rowData)} />}
                        {rowData.uploaded == true && <Fontisto style={{ color: '#0086b3', fontWeight: 'bold', alignItems: 'center', fontSize: 22 }} name='preview' onPress={() => { this.downloadAttachment(rowData.fileId, rowData.fileName) }} />}
                        <Entypo style={{ color: '#0086b3', fontWeight: 'bold', alignItems: 'center', fontSize: 22, marginRight: 10, marginLeft: 5 }} name='cross' onPress={() => { if (rowData.progressBarShow == false) { this.removeattachment(rowData) } }} />
                      </View>
                    </View>

                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          }

          <TouchableOpacity style={[styles.floatingButton, !this.state.fileattached ? { bottom: 100 } : { bottom: 100 * 2 }]} onPress={() => this.toggle()} >
            <AntDesign name='close' style={{ fontSize: 25, color: '#0086b3' }} />
          </TouchableOpacity>


          <View style={styles.footer}>
            <TouchableOpacity style={styles.bottomButtons} onPress={() => this.file()}>
              <View style={styles.buttonContainer}>
                <Entypo style={styles.footerText} name='attachment' />
                <Text style={styles.footerText2}>Attachment</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomButtons} onPress={() => this.imageNew()} >
              <View style={styles.buttonContainer}>
                <Entypo style={styles.footerText} name='image' />
                <Text style={styles.footerText2}>Image</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomButtons} onPress={() => this.opendoodle()}>
              <View style={styles.buttonContainer}>
                <Entypo style={styles.footerText} name='pencil' />
                <Text style={styles.footerText2}>Doodle</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomButtons} onPress={() => {
              if (Platform.OS === 'android') {
                this.openPhoneCamera();
              } else {
                this.openPhoneCameraIOS();
              }
            }}>
              <View style={styles.buttonContainer}>
                <Entypo style={styles.footerText} name='camera' />
                <Text style={styles.footerText2}>Camera</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }




  validateFormData(clicked) {
    if (this.state.formDataModified || !this.state.formDataModified && clicked == 'submit') {
      let responses = this.getAllResponses();
      //default values
      const fieldIndex = this.propertiesArray.findIndex(item => item.FormFieldName === 'applicable_for_patent_application');
      if (fieldIndex != -1 && this.propertiesArray[fieldIndex].Value == null) {
        this.propertiesArray[fieldIndex].Value = 0;
      }
      //get form responses

      for (var key in responses) {
        if (responses.hasOwnProperty(key)) {
          const foundIndex = this.propertiesArray.findIndex(item => item.FormFieldName === responses[key].actualName)
          if (this.propertiesArray[foundIndex].FormPropertyTypeId == 5 || this.propertiesArray[foundIndex].FormPropertyTypeId == 6 || this.propertiesArray[foundIndex].FormPropertyTypeId == 15) {
            //dropdown, sourcedropdown
            var answer = responses[key].userAnswer[0];
            this.propertiesArray[foundIndex].Value = answer;
          } else if (this.propertiesArray[foundIndex].FormPropertyTypeId == 18 || this.propertiesArray[foundIndex].FormPropertyTypeId == 16) {
            //switch , checkbox
            this.propertiesArray[foundIndex].Value = responses[key].userAnswer.regular.length == 0 ? 0 : 1; // 1 - on, 0 - off , 
          }
          else if (this.propertiesArray[foundIndex].FormPropertyTypeId == 12 || this.propertiesArray[foundIndex].FormPropertyTypeId == 11) {
            //tagsfield , userfield
            let propertyID = this.propertiesArray[foundIndex].FormPropertyTypeId;
            if (responses[key].userAnswer.length > 0) {
              let tagValues = responses[key].userAnswer.map((item) => {
                if (propertyID == 12) {
                  return item.name;
                } else if (propertyID == 11) {
                  return item.value;
                }
              }).join(",");
              console.log("FINAL Tag Values", tagValues);
              this.propertiesArray[foundIndex].Value = tagValues; // 1 - on, 0 - off , 
            }
          }
          else {
            this.propertiesArray[foundIndex].Value = responses[key].userAnswer;
          }
        }
      }
      this.saveIdeaDetails.Properties = this.propertiesArray;
      if (clicked == 'save') {
        this.saveIdeaDetails.DoSubmit = false; //save as draft
      } else {
        this.saveIdeaDetails.DoSubmit = true; //save
      }
      this.saveIdeaDetails.ContentId = this.state.contentId;
      if (this.attachmentFileList.length > 0) {
        this.toggle();
      }
      this.props.saveIdeaInfo({
        data: this.saveIdeaDetails
      })
        .then(() => {
          if (this.props.success) {
            console.log("in saveIdeaInfo props success", this.props.contentId);
            this.setState({ contentId: this.props.contentId });
            this.addAllAttachments(clicked);
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (clicked == 'save') {
              this.refs.toast.show('The Challenge has been saved successfully.', 500, () => {
                this.initiateHomeAction();
                this.setState({
                  editButtonClicked: false,
                  formDataModified: false,
                });
                if (this.state.attachmentStatus == true) {
                  this.setState({ footerVisible: false });
                }
              });
            }
            else {
              this.refs.toast.show('The Challenge has been submitted successfully.', 500, () => {
                this.initiateHomeAction();
                if (this.state.attachmentStatus == true) {
                  this.props.navigation.navigate("HomeScreen");
                }
              });
            }
          }
          if (this.props.error) {
            console.log(
              "in saveIdeaInfo props error",
              this.props.errorMsg
            );
            //alert(this.props.errorMsg);
            handleError(error, false);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }
  }

  addAllAttachments(clicked) {
    //attachments
    if (this.attachmentFileList.length > 0) {
      let fileObject = [];
      let fileObjectPromise = [];
      this.attachmentFileList.map((item, idx) => {
        if (item.uploaded == false) {
          let index = fileObject.push(
            {
              contentFileName: item.fileName,
              contentFileBytes: item.fileByteString,
              fileType: item.fileType,
              key: item.key,
              contentId: this.state.contentId
            }) - 1;
          fileObjectPromise.push(this.fileAttachAPI(fileObject[index]));
        }
      });

      if (fileObjectPromise.length > 0) {
        Promise.all(fileObjectPromise).then((responsesArray) => {
          console.log("Promise returned values", responsesArray);

          const checkResult = responsesArray.includes(false);
          if (checkResult == true) {
            this.setState({ attachmentStatus: false });
          }
          else {
            this.setState({ attachmentStatus: true });
          }
        });
      }
    }
  }

  fileAttachAPI(fileObject) {

    this.attachmentFileList[fileObject.key].progressBarShow = true;
    this.setState({ refresh: true });

    let self = this;
    let key = fileObject.key;

    const url = CLIENT_UAT1_URL + `/Challenge/SaveAttachment`;
    let data = {
      ContentFileName: fileObject.contentFileName,
      ContentId: fileObject.contentId,
      ContentFileId: 0,
      ContentFileBytes: fileObject.contentFileBytes,
      FileType: fileObject.fileType,
      FormFieldId: 25
    }
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': this.props.token,
      'api_key': 'LgOiuSu64U',
    }

    return axios
      .post(url, data, {
        headers: headers, onUploadProgress(progressEvent) {
          var percentCompleted = (Math.round((progressEvent.loaded * 100) / progressEvent.total)) / 100;
          self.setState({ progressIncrement: percentCompleted });
          self.attachmentFileList[key].progressIncrement = percentCompleted;
        }
      })
      .then(function (response) {
        console.log("In fileAttachAPI", response, key);
        self.attachmentFileList[key].fileId = response.data.ContentFileId;
        self.attachmentFileList[key].progressBarShow = false;
        self.attachmentFileList[key].success = true;
        self.attachmentFileList[key].uploaded = true;
        self.setState({ refresh: true });
        return Promise.resolve(true);
      })
      .catch(function (error) {
        console.log("In fileAttachAPI", error, key);
        self.attachmentFileList[key].progressBarShow = false;
        self.attachmentFileList[key].success = false;
        self.attachmentFileList[key].uploaded = false;
        self.setState({ refresh: true });
        return Promise.resolve(false);
      });
  }


  checkOwnKeys(keys, object) {
    let perPageRequiredStatus = {};
    for (var key in keys) {
      perPageRequiredStatus[key] = keys[key].every((key) => {
        if (object.hasOwnProperty(key)) {
          if (object[key].userAnswer == "") {
            return false;
          }
          else {
            return true;
          }
        }
      });
    }
    return perPageRequiredStatus;
  }

  checkSaveEnabeKeys(key, object) {
    if (object.hasOwnProperty(key)) {
      if (object[key].userAnswer == "") {
        return false;
      }
      else {
        return true;
      }
    }
  }


  getAllResponses() {
    let responseArray = [];
    let responseArray1 = []

    this.formArray.map((form, i) => {
      const response = form._getFormResponses();
      responseArray.push(response);
    });
    responseArray1 = responseArray.reduce(function (result, currentObject) {
      for (var key in currentObject) {
        if (currentObject.hasOwnProperty(key)) {
          result[key] = currentObject[key];
        }
      }
      return result;
    }, {});
    return responseArray1;
  }

  toggleSwitch(data, idx) {
    if (data.applicable_for_patent_application0.userAnswer.regular.length == 0) {
      this.formArray[idx].props.form.map((element, index) => {
        this.setState({ elementDisableForText: true });
      });
    }
    else if (data.applicable_for_patent_application0.userAnswer.regular.length > 0) {
      this.formArray[idx].props.form.map((element, index) => {
        this.setState({ elementDisableForText: false });
      })
    }
  }

  getSubCatergoryAPI(data, idx) {
    let info = {
      selectedValue: data.category2.userAnswer[0],
    }
    this.props
      .getSubCategoryValues({
        data: info,
      })
      .then(() => {
        if (this.props.success) {
          console.log("in getSubCategory props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getSubCategory props error",
            this.props.errorMsg
          );
          //alert(this.props.errorMsg);
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  toggleCheckBox(data, idx) {
    if (data.CostSavingsApplicable0) {
      if (data.CostSavingsApplicable0.userAnswer.regular.length == 0) {
        this.setState({ elementDisableForCostDetails: true }); //true
      } else if (data.CostSavingsApplicable0.userAnswer.regular.length > 0) {
        this.setState({ elementDisableForCostDetails: false }); //false
      }
    }
    if (data.ImplementationCostApplicable0) {
      if (data.ImplementationCostApplicable0.userAnswer.regular.length == 0) {
        this.setState({ elementDisableForImplement: true });

      } else if (data.ImplementationCostApplicable0.userAnswer.regular.length > 0) {
        this.setState({ elementDisableForImplement: false });
      }
    }
  }

  renderSpinner() {
    //check state value of loading variable

    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  validateForSubmitIdea() {
    this.keyboardDidShowListener.remove();
    this.props.navigation.navigate('ViewPager', {
      context: 'challenge',
      challengeTitle: this.state.title,
      challengeContentId: this.state.contentIDForIdeasOnChallenge,
      challengeCategory: { ...this.challengeCategory },
      challengeSubCategory: { ...this.challengeSubCategory },
      challengeLocation: { ...this.challengeLocation },
    });
  }

  validateForEditData() {

    const contentStatus = this.props.navigation.getParam('contentStatus', 'NO-data');
    const canEdit = this.props.navigation.getParam('canEdit', 'NO-data');
    if (contentStatus == 'Approved' && canEdit == false) {
      //show modal or navigate to a screen to show start date and end date and submit using API POST /Content/UpdateChallengeDuration
      this.props.navigation.navigate("EditApprovedChallengeData");
    } else {
      this.setState({ editButtonClicked: true });
      if (this.fieldsWithValueArray.indexOf('challenge_title0') != -1) {
        this.setState({ disableSaveButton: false })
      }
      let perPageRequiredStatus = {};
      for (var key in this.requiredFieldsArray) {
        perPageRequiredStatus[key] = this.requiredFieldsArray[key].every((key) => {
          if (this.fieldsWithValueArray.indexOf(key) == -1) {
            return false;
          }
          else {
            return true
          }
        });
      }
      this.setState({ perPageRequiredStatus: perPageRequiredStatus });
      const checkResult = Object.values(perPageRequiredStatus).includes(false);

      if (checkResult == false) {
        this.setState({ disableSubmitButton: false });
      }
    }
  }

  renderEditButton() {
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    const contentStatus = this.props.navigation.getParam('contentStatus', 'NO-data');
    return (
      <View style={[styles.getStartedButtonForEdit, { bottom: contentStatus == "Approved" ? 80 : 0 }]}>
        {!this.state.editButtonClicked && contentId != 'NO-data' && (contentStatus == 'Approved' || contentStatus == 'Awaiting Publish' || contentStatus == 'Draft')
          && <TouchableOpacity
            style={[styles.bottomButton, { backgroundColor: '#4e12bc', }]}
            onPress={() => { console.log("in rendereditbutton"); this.validateForEditData() }}
            disabled={false}
          >
            <Text
              style={{ fontSize: fonts.md, fontWeight: "600", color: "white", fontFamily: Platform.OS === 'android' ? 'SF Pro Display' : 'system font' }}
            >
              Edit
          </Text>
          </TouchableOpacity>}
        {this.state.editButtonClicked && contentStatus === 'Draft' &&
          <TouchableOpacity
            style={[styles.bottomButton, (this.state.disableSaveButton) ? { backgroundColor: '#EEEEEE' } : { backgroundColor: '#4e12bc' }]}
            onPress={() => { this.validateFormData('save') }}
            disabled={this.state.disableSaveButton}
          >
            <Text
              style={{ fontSize: fonts.md, fontWeight: "600", color: "white", fontFamily: Platform.OS === 'android' ? 'SF Pro Display' : 'system font' }}
            >
              Save
          </Text>
          </TouchableOpacity>}

        {this.state.editButtonClicked && <TouchableOpacity
          style={[styles.bottomButton, (this.state.disableSubmitButton) ? { backgroundColor: '#EEEEEE' } : { backgroundColor: '#F51173' }]}
          onPress={() => { this.validateFormData('submit') }}
          disabled={this.state.disableSubmitButton}
        >
          <Text
            style={{ fontSize: fonts.md, fontWeight: "600", color: "white", fontFamily: Platform.OS === 'android' ? 'SF Pro Display' : 'system font' }}
          >
            Submit
          </Text>
        </TouchableOpacity>}
      </View>
    );
  }

  renderSubmitAnIdeaButton() {
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    const contentStatus = this.props.navigation.getParam('contentStatus', 'NO-data');
    return (
      <View style={styles.getStartedButton}>
        {contentStatus == 'Approved' && <TouchableOpacity
          style={[styles.bottomButton, { backgroundColor: '#F51173' }]}
          onPress={() => { this.validateForSubmitIdea() }}
          disabled={false}
        >
          <Text
            style={{ fontSize: fonts.md, fontWeight: "600", color: "white", fontFamily: Platform.OS === 'android' ? 'SF Pro Display' : 'system font' }}
          >
            Submit Idea
          </Text>
        </TouchableOpacity>}
      </View>
    );
  }

  formDataChanges(data, idx) {
    const { dispatch } = this.props;
    this.setState({ formDataModified: true });
    if (data.applicable_for_patent_application0) {
      this.toggleSwitch(data, idx);
    }
    if (data.category2) { //&& !data.idea_subgroup3) {
      //make API call to get sub-category
      if (data.category2.userAnswer[0] != this.state.selectedCategory) {
        this.setState({ selectedCategory: data.category2.userAnswer[0] });
        this.getSubCatergoryAPI(data, idx);
      }
    }
    if (data.CostSavingsApplicable0 || data.ImplementationCostApplicable0) {
      this.toggleCheckBox(data, idx);
    }

    let responses = this.getAllResponses();

    //check if Save can be enabled
    let checkSaveResult = this.checkSaveEnabeKeys('title0', responses);
    if (checkSaveResult == true) {
      //update state to enable the save button 
      this.setState({ disableSaveButton: false });
    }
    else {
      //update state to enable the save button 
      this.setState({ disableSaveButton: true });
    }

    //check if submit can be enabled
    let checkResultMap = this.checkOwnKeys(this.requiredFieldsArray, responses);
    this.setState({ perPageRequiredStatus: checkResultMap });
    const checkResult = Object.values(checkResultMap).includes(false);

    if (checkResult == true) {
      //update state to enable the save button 
      this.setState({ disableSubmitButton: true });
    }
    else {
      //update state to enable the save button 
      this.setState({ disableSubmitButton: false });
    }
  }

  updateAttachmentList(attachmentList) {
    attachmentList.map((data, index) => {
      if (this.attachmentFileList.findIndex(item => item.fileName === data.ContentFileName) === -1) {
        this.setState({ fileattached: true });
        let fileObject = {
          fileName: data.ContentFileName,
          fileByteString: data.ContentFile,
          fileType: data.FileType,
          fileId: data.ContentFileId,
          key: this.attachmentFileList.length,
          progressBarShow: false,
          success: true,
          uploaded: true,
        };
        this.attachmentFileList.push(fileObject);
      }
    })
  }

  render() {
    const fieldValuesArray = this.props.navigation.getParam('data', 'NO-data');
    this.challengeType = this.props.navigation.getParam('type', 'NO-data');
    const contentStatus = this.props.navigation.getParam('contentStatus', 'NO-data');
    const formControls = this.props.navigation.getParam('formControls', 'NO-data');
    const canRespond = this.props.navigation.getParam('canRespond', 'NO-data');
    const canEdit = this.props.navigation.getParam('canEdit', 'NO-data');

    if (this.props.createFormJson) {
      const addideaComponent = this.props.createFormJson.SubForms.map((form1, idx) => {
        const fieldsArray = [];
        let dropDownList = [];
        let switchOptions = [
          {
            label: '',
            value: '',
            selected: false
          }
        ];
        let switchOptionsForSubForm = [
          [{
            label: '',
            value: '',
            selected: false
          }],
          [{
            label: '',
            value: '',
            selected: false
          }]
        ];
        const formTheme = {
          label: {
            fontWeight: '900'
          },
        }
        const mytheme = buildTheme({}, {}, formTheme);
        //create the fields array
        if (form1.Properties != null) {
          let tempStoreProperties = [...form1.Properties];
          //filter out 
          if (form1.Name == 'Challenge Type' && this.challengeType != 'Department Challenge' || form1.Name == 'Challenge Type' && this.challengeType != 'Unit Level') {

            let fiteredProperties = tempStoreProperties.filter((item) => {
              if (item.Name == 'Invited Members' || item.Name == 'Department Multiselect') {
                if (this.challengeType == 'Unit Level' && item.Name == 'Department Multiselect') {
                  return item;
                } else {
                  return;
                }
              } else {
                return item;
              }
            })
            tempStoreProperties = fiteredProperties;
          }

          tempStoreProperties.map((element, index) => {
            if (element.FormPropertyType.Type != 'FileUpload') { //!FileUpload
              dropDownList = [];
              if (ELEMENT[element.FormPropertyType.Type] == 'select') {
                dropDownList = [{
                  label: "Please select",
                  value: "Please select",
                  disabled: true
                }];
                element.FormPropertyListOptions.map((item, i) => {
                  dropDownList.push(
                    {
                      label: item.Value,
                      value: item.Id,
                      selected: false
                    });
                });
              }
              let subCategoryValue = [...this.props.subCategory];
              let obj = {
                key: element.FormFieldName + index,
                actualName: element.FormFieldName,
                type: ELEMENT[element.FormPropertyType.Type],
                required: element.Required,
                label: element.Label,
                values: element.FormPropertyType.Type == 'Switch' ? switchOptions : dropDownList.length == 1 && ELEMENT[element.FormPropertyType.Type] != 'tags' ? subCategoryValue : dropDownList,//element.FormPropertyListOptions,
                searchInputPlaceholder: 'Select' + ' ' + element.Label,
                toggle: element.FormPropertyType.Type == 'Switch' ? true : false, // renders a toggle instead of checkbox
                style: { fontWeight: 'bold' },
                directTextEdit: !this.state.editButtonClicked ? false : true,
                data: element.FormFieldName == 'challenge_tags' ? this.state.tagsSuggestions : this.state.peopleTagsList,
                filterForTags: element.FormFieldName == 'challenge_tags' ? this.customFilterDataTags : this.customFilterDataTeamMembers,
                disabled: !this.state.editButtonClicked ? true : element.DisableAfterSubmit,
                tagDisable: !this.state.editButtonClicked ? 'none' : 'auto',
                textInputProps: { editable: !this.state.editButtonClicked ? false : element.DisableAfterSubmit ? false : true },
                selectDropdownDisable: !this.state.editButtonClicked ? 'none' : element.DisableAfterSubmit ? 'none' : 'auto',
                multiple: element.FormPropertyType.Type == 'SourceBoundMultiselectDropdown' ? true : false,
              }

              if (ELEMENT[element.FormPropertyType.Type] == 'select') {
                if (element.FormFieldName == 'challenge_subcategory' && fieldValuesArray[element.FormFieldName].Value != null) {
                  let valueObj = {
                    label: fieldValuesArray[element.FormFieldName].Value.Value,
                    value: fieldValuesArray[element.FormFieldName].Value.Id,
                    selected: true
                  };
                  obj.values.push(valueObj);
                  this.challengeSubCategory = { ...valueObj };
                }
                else {
                  if (fieldValuesArray[element.FormFieldName].Value != null) {
                    if (element.FormFieldName == 'challenge_category' || element.FormFieldName == 'challenge_location') {
                      let valueObj = {
                        label: fieldValuesArray[element.FormFieldName].Value.Value,
                        value: fieldValuesArray[element.FormFieldName].Value.Id,
                        selected: true
                      };
                      if (element.FormFieldName == 'challenge_category') {
                        this.challengeCategory = { ...valueObj };
                      }
                      else {
                        this.challengeLocation = { ...valueObj };
                      }
                    }
                    if (element.FormPropertyType.Type == 'SourceBoundMultiselectDropdown') {
                      let toBeselectedObj = {};
                      var johns = _.map(fieldValuesArray[element.FormFieldName].Value, function (o) {
                        toBeselectedObj = obj.values.find(item => item.value === o.Id);

                        return toBeselectedObj.selected = true;

                      });

                    } else {
                      var johns = _.map(obj.values, function (o) {
                        if (o.value == fieldValuesArray[element.FormFieldName].Value.Id)
                          return o.selected = true;
                      });
                    }
                  }
                }
              } else if (ELEMENT[element.FormPropertyType.Type] == 'checkbox-group' && fieldValuesArray[element.FormFieldName].Value != null) {
                if (element.FormFieldName == 'applicable_for_patent_application') {
                  obj.values[0]['selected'] = fieldValuesArray[element.FormFieldName].Value;
                }
              } else if (ELEMENT[element.FormPropertyType.Type] == 'tags' && fieldValuesArray[element.FormFieldName].Value != null) {
                let tagsList = [];
                if (element.FormFieldName == 'tags' || 'challenge_tags') {
                  if (fieldValuesArray[element.FormFieldName].Value.length > 0) {
                    fieldValuesArray[element.FormFieldName].Value.map((item) => {
                      tagsList.push(
                        {
                          name: item.TagName
                        }
                      );
                    });
                  }
                  obj.values = tagsList;
                } else if (element.FormFieldName == 'team_members' || 'challenge_team') {
                  if (fieldValuesArray[element.FormFieldName].Value.length > 0) {
                    fieldValuesArray[element.FormFieldName].Value.map((item) => {
                      tagsList.push(
                        {
                          name: item.UserName
                        }
                      );
                    });
                  }
                  obj.values = tagsList;
                }
              }
              else if (element.FormPropertyType.Type == 'DateRange') {
                let dateString = fieldValuesArray[element.FormFieldName].Value;
                let parts = dateString.split("*");
                let startDate = Moment(parts[0].replace(/\s/g, '')).format('DD/MM/YYYY');
                let endDate = Moment(parts[1].replace(/\s/g, '')).format('DD/MM/YYYY');
                obj.value = startDate + ' - ' + endDate;
              }
              else {
                if (fieldValuesArray[element.FormFieldName].Value != null) {
                  obj.value = fieldValuesArray[element.FormFieldName].Value;
                }
              }
              fieldsArray.push(obj);
              if (element.Required == true) {
                this.requiredFieldsArray[form1.Name] = this.requiredFieldsArray[form1.Name] ? this.requiredFieldsArray[form1.Name] : [];
                if (this.requiredFieldsArray[form1.Name].indexOf(element.FormFieldName + index) === -1) {
                  this.requiredFieldsArray[form1.Name].push(element.FormFieldName + index);
                }
              }
              if (this.propertiesArray.findIndex(item => item.FormFieldName === element.FormFieldName) === -1) {
                let propertyValues = { ...element };
                propertyValues.Value = null;
                this.propertiesArray.push(propertyValues);
              }
              if (fieldValuesArray[element.FormFieldName].Value != null) {
                if (this.fieldsWithValueArray.indexOf(element.FormFieldName + index) === -1) {
                  this.fieldsWithValueArray.push(element.FormFieldName + index);
                }
              }
            }//!FileUpload
          });
        }
        else {
          //subforms
          form1.SubForms.map((subform, i) => {
            fieldsArray.push({
              key: i.toString(),
              type: 'paragraph',
              subtype: 'h1',
              label: subform.Name,
              style: { fontSize: 20, fontWeight: '800' }
            });
            subform.Properties.map((element, index) => {
              // if element is a dropdown 
              if (element.FormPropertyType.Type == 'SourceDropdown' || element.FormPropertyType.Type == 'Dropdown' || element.FormPropertyType.Type == 'RadioButton') {
                dropDownList = [];
                element.FormPropertyListOptions.map((item, i) => {
                  dropDownList.push(
                    {
                      label: item.Value,
                      value: item.Value
                    });
                });
              }
              let obj = {
                key: element.FormFieldName + index,
                actualName: element.FormFieldName,
                type: ELEMENT[element.FormPropertyType.Type],
                required: element.Required,
                label: element.Label,
                values: element.FormPropertyType.Type == 'Switch' || element.FormPropertyType.Type == 'CheckBox' ? switchOptionsForSubForm[i] : dropDownList,//element.FormPropertyListOptions,
                searchInputPlaceholder: 'Select' + ' ' + element.Label,
                toggle: element.FormPropertyType.Type == 'Switch' ? true : false, // renders a toggle instead of checkbox
                style: { fontWeight: 'bold' },
                directTextEdit: !this.state.editButtonClicked ? false : element.FormPropertyType.Type == 'Number' && element.DisableBeforeSubmit && i == 0 ? !this.state.elementDisableForCostDetails : !this.state.elementDisableForImplement,
                disabled: !this.state.editButtonClicked ? true : element.DisableBeforeSubmit && i == 0 ? this.state.elementDisableForCostDetails : element.FormFieldName == 'CostSavingsApplicable' ? false : element.FormFieldName == 'ImplementationCostApplicable' ? false : this.state.elementDisableForImplement
              }
              if (costFieldValuesArray.length > 0) {
                costFieldValuesArray.map((item, itemIndex) => {
                  if (ELEMENT[element.FormPropertyType.Type] == 'checkbox-group') {
                    if (element.FormFieldName == 'CostSavingsApplicable' && item.CostType == 1) {
                      obj.values[0]['selected'] = true;
                    } else if (element.FormFieldName == 'ImplementationCostApplicable' && item.CostType == 2) {
                      obj.values[0]['selected'] = true;
                    }
                  }
                  else if (ELEMENT[element.FormPropertyType.Type] == 'number') {
                    if (element.FormFieldName == 'EstimatedSavings' && item.CostType == 1) {
                      obj.value = item.EstimatedSavings;
                    } else if (element.FormFieldName == 'EstimatedCost' && item.CostType == 2) {
                      obj.value = item.EstimatedCost;
                    }
                  }
                  else if (ELEMENT[element.FormPropertyType.Type] == 'radio-group') {
                    if (element.FormFieldName == 'SavingsType' && item.CostType == 1) {

                      _.map(obj.values, function (o) {
                        if (o.value == 'One time' && item.OccurrenceTypeID == 2) { //one time
                          return o.selected = true;
                        }
                        else if (o.value == 'Recurring' && item.OccurrenceTypeID == 1) { //recurring
                          return o.selected = true;
                        }
                      });

                    } else if (element.FormFieldName == 'CostType' && item.CostType == 2) {
                      _.map(obj.values, function (o) {
                        if (o.value == 'One time' && item.OccurrenceTypeID == 2) { //one time
                          return o.selected = true;
                        }
                        else if (o.value == 'Recurring' && item.OccurrenceTypeID == 1) { // recurring
                          return o.selected = true;
                        }
                      });
                    }
                  }
                  else if (ELEMENT[element.FormPropertyType.Type] == 'textarea') {
                    if (element.FormFieldName == 'CostSavingsRemark' && item.CostType == 1) {
                      obj.value = item.Remark;
                    }
                    else if (element.FormFieldName == 'ImplementationCostRemark' && item.CostType == 2) {
                      obj.value = item.Remark;
                    }
                  }
                });
              }

              fieldsArray.push(obj);
              if (element.Required == true) {
                this.requiredFieldsArray[form1.Name] = this.requiredFieldsArray[form1.Name] ? this.requiredFieldsArray[form1.Name] : [];
                if (this.requiredFieldsArray[form1.Name].indexOf(element.FormFieldName + index) === -1) {
                  this.requiredFieldsArray[form1.Name].push(element.FormFieldName + index);
                }
              }
              if (this.propertiesArray.findIndex(item => item.FormFieldName === element.FormFieldName) === -1) {
                let propertyValues = { ...element };
                propertyValues.Value = null;
                this.propertiesArray.push(propertyValues);
              }
            });
          });
        }
        //create the form
        if (fieldsArray.length > 0) {
          return (
            <View
              key={idx}
              style={{
                padding: 10
              }}
            >

              <Header
                containerStyle={styles.headerstyle}
                leftComponent={<Icon name='left' type='antdesign' color='blue' onPress={() => {
                  if (this.state.vpPagePosition == 0) {
                    if (this.state.formDataModified) {
                      let buttonsArray = [
                        {
                          text: 'OK',
                          onPress: () => this.props.navigation.navigate('HomeScreen'),
                          style: 'cancel',
                        },
                      ];
                      if (this.state.contentStatus === 'Draft') {
                        buttonsArray.push({ text: 'Cancel', onPress: () => this.validateFormData('save') });
                      } else {
                        buttonsArray.push({ text: 'Cancel', onPress: () => this.validateFormData('submit') });
                      }
                      Alert.alert(
                        '',
                        'Do you really want to close the window?. You will be losing filled data.',
                        buttonsArray,
                        { cancelable: false },
                      );
                    } else {
                      this.props.navigation.goBack();
                    }
                  } else {
                    this.viewPager.setPage(this.state.vpPagePosition - 1);
                  }
                }} />}
                centerComponent={{ text: form1.Name, style: { color: 'blue', fontSize: 22 } }}
                rightComponent={this.state.vpPagePosition != (this.state.editButtonClicked ? (this.props.createFormJson.SubForms.length - 2) : (this.props.createFormJson.SubForms.length - 1)) && <TouchableOpacity onPress={() => {
                  this.viewPager.setPage(this.state.vpPagePosition + 1)
                }} ><Text style={{ fontSize: 20, color: 'blue', marginRight: Platform.OS === 'android' ? 5 : 5 }}>Next</Text></TouchableOpacity>}
              />
              <OfflineNotice />
              {this.state.hideBadge &&
                <View style={styles.childButton}>
                  <Icon iconStyle={styles.infoIcon} type='antdesign' name='infocirlceo' onPress={() =>
                    this.iconPressFunc()
                  } />
                </View>
              }
              {this.showIconBadge()}
              <DynamicForm
                ref={(ref) => {
                  if (this.formArray.indexOf(ref) === -1 && ref != null) {
                    this.formArray.push(ref);
                  }
                }}
                form={fieldsArray}
                theme={mytheme}
                style={styles.backgroundStyle}
                onFormDataChange={(data) => this.formDataChanges(data, idx)}
              />
            </View>
          );
        }
        else {
          if (this.state.editButtonClicked == true) {
            return (null);
          } else {


            return (
              <View
                key={idx}
                style={{
                  padding: 10
                }}
              >

                <Header
                  containerStyle={styles.headerstyle}
                  leftComponent={<Icon name='left' type='antdesign' color='blue' onPress={() => {
                    if (this.state.vpPagePosition == 0) {
                      if (this.state.formDataModified) {
                        let buttonsArray = [
                          {
                            text: 'OK',
                            onPress: () => this.props.navigation.navigate('HomeScreen'),
                            style: 'cancel',
                          },
                        ];
                        if (this.state.contentStatus === 'Draft') {
                          buttonsArray.push({ text: 'Cancel', onPress: () => this.validateFormData('save') });
                        } else {
                          buttonsArray.push({ text: 'Cancel', onPress: () => this.validateFormData('submit') });
                        }
                        Alert.alert(
                          '',
                          'Do you really want to close the window?. You will be losing filled data.',
                          buttonsArray,
                          { cancelable: false },
                        );
                      } else {
                        this.props.navigation.goBack();
                      }
                    } else {
                      this.viewPager.setPage(this.state.vpPagePosition - 1);
                    }
                  }} />}
                  centerComponent={{ text: form1.Name, style: { color: 'blue', fontSize: 22 } }}
                  rightComponent={this.state.vpPagePosition != this.props.createFormJson.SubForms.length - 1 && <TouchableOpacity onPress={() => {
                    this.viewPager.setPage(this.state.vpPagePosition + 1)
                  }} ><Text style={{ fontSize: 20, color: 'blue', marginRight: Platform.OS === 'android' ? 5 : 5 }}>Next</Text></TouchableOpacity>}
                />
                <OfflineNotice />
                {this.state.hideBadge &&
                  <View style={styles.childButton}>
                    <Icon iconStyle={styles.infoIcon} type='antdesign' name='infocirlceo' onPress={() =>
                      this.iconPressFunc()
                    } />
                  </View>
                }
                {this.showIconBadge()}
                <View style={{ height: 500 }}>

                  <FlatList
                    contentContainerStyle={{ paddingBottom: 5, paddingLeft: 5, marginTop: 30 }}
                    style={{ padding: 20 }}
                    key={this.attachmentFileList.length}
                    data={this.attachmentFileList}
                    extraData={this.state.refresh}
                    renderItem={({ item: rowData }) => {
                      return (

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', height: 30, borderRadius: 20, borderColor: 'black', borderWidth: 0.3, marginTop: 5 }}>
                          {this.showIconByFileType(rowData.fileType, rowData.fileName)}
                          <Text numberOfLines={1} ellipsizeMode='head' style={{ flexGrow: 1, width: 0, flexDirection: "column", justifyContent: "center", fontWeight: 'bold', alignItems: 'center', fontSize: 16 }}>{rowData.fileName} </Text>
                          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: 30 }}>
                            <Fontisto style={{ color: '#0086b3', fontWeight: 'bold', alignItems: 'center', fontSize: 22, marginRight: 10, marginLeft: 10 }} name='preview' onPress={() => { this.downloadAttachment(rowData.fileId, rowData.fileName) }} />
                          </View>
                        </View>

                      );
                    }}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>

              </View>
            );
          }
        }
      });


      return (

        <View pointerEvents={this.props.loading ? "none" : "auto"} style={styles.mainViewStyle}>
          <IndicatorViewPager
            style={{ height: this.state.footerVisible ? deviceHeight : deviceHeight - 80, backgroundColor: 'white' }}
            indicator={this._renderDotIndicator()}
            onPageSelected={(e) => {
              this.setState({ vpPagePosition: e.position });
            }}
            ref={viewPager => { this.viewPager = viewPager }}
          >

            {addideaComponent}

          </IndicatorViewPager>
          {this.renderSpinner()}
          <Toast ref="toast" position='top' />
          {this.state.editButtonClicked && (formControls == 'NO-data' || formControls.length == 0) && this.renderAttachmentButton()}
          {!this.state.footerVisible && (formControls == 'NO-data' || formControls.length == 0) && this.renderEditButton()}
          {!this.state.footerVisible && (formControls == 'NO-data' || formControls.length == 0) && contentStatus == 'Approved' && this.renderSubmitAnIdeaButton()}
          {!this.state.editButtonClicked && !this.state.footerVisible && canRespond == true && this.showRespondToQueryButton()}
          {this.state.showReviewerInvite == false && formControls != 'NO-data' ? this.showApprovalsPanel(formControls) : this.analyseReviewerInvite()}

        </View>
      );
    } else {
      return (
        <View>Internal Error(GetSubmitForm API)</View>
      );
    }
  }

  analyseReviewerInvite() {
    const formControls = this.props.navigation.getParam('formControls', 'NO-data');
    if (this.state.inviteCounter == 0 && formControls != 'NO-data') {
      console.log("inviteCounter", this.state.inviteCounter);
      this.setState({ inviteCounter: 1 });
      Alert.alert(
        '',
        'You are invited to review this Idea. Would you like to accept the invite?',
        [
          {
            text: 'Yes',
            onPress: () => { this.reviewInviteAccepted() },
          },
          { text: 'No', onPress: () => { this.reviewInviteRejected() } },
          { text: 'Cancel', onPress: () => { this.props.navigation.navigate("Approvals") } },
        ],
      );
    }
  }

  reviewInviteAccepted() {
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    let data = {
      contentID: contentId,
    }
    this.props.acceptReviewerInvite({ data: data })
      .then(() => {
        if (this.props.success) {
          console.log("in acceptReviewerInvite props success");
          this.setState({ showReviewerInvite: false });
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }

        if (this.props.error) {
          console.log(
            "in acceptReviewerInvite props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });

  }

  reviewInviteRejected() {
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    let data = {
      contentID: contentId,
    }
    this.props.rejectReviewerInvite({ data: data })
      .then(() => {
        if (this.props.success) {
          console.log("in rejectReviewerInvite props success");
          this.props.navigation.navigate("Approvals");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }

        if (this.props.error) {
          console.log(
            "in rejectReviewerInvite props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }


  showRespondToQueryButton() {
    const canRespond = this.props.navigation.getParam('canRespond', 'NO-data');
    const contentStatus = this.props.navigation.getParam('contentStatus', 'NO-data');
    return (
      <View style={[styles.getStartedButton, { bottom: contentStatus == 'Approved' ? 160 : 80 }]}>
        <TouchableOpacity
          style={[styles.bottomButton, { backgroundColor: '#F51173' }]}
          onPress={() => { this.validateRespondToQuery() }}
          disabled={false}
        >
          <Text
            style={[styles.navText, { fontSize: fonts.md, fontWeight: "600", color: "white", fontFamily: Platform.OS === 'android' ? 'SF Pro Display' : 'system font' }]}
          >
            Respond To Query
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  validateRespondToQuery() {
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    const taskId = this.props.navigation.getParam('taskId', 'NO-data');
    this.keyboardDidShowListener.remove();
    this.props.navigation.navigate("ApprovalOptions", { type: 1, title: "Respond To Query", label: "Comment", index: 12, btnLabel: "Submit", contentId: contentId, taskId: taskId });

  }

  approvalsAPI(index) {
    console.log("Approval Index", index);
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    const taskId = this.props.navigation.getParam('taskId', 'NO-data');
    const reviewersList = this.props.navigation.getParam('reviewersList', 'NO-data');
    let reviewerCount = 0;
    if (reviewersList != 'NO-data') {
      reviewerCount = reviewersList.length;
    }
    this.keyboardDidShowListener.remove();
    if (index == 0) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Publish Challenge", label: "Comment", index: 0, btnLabel: "Submit", contentId: contentId, taskId: taskId });


    } else if (index == 1) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Redirect Challenge", label: "Comment", index: 1, btnLabel: "Redirect", contentId: contentId, taskId: taskId });

    } else if (index == 2) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Ask Query", label: "Query", index: 2, btnLabel: "Submit", contentId: contentId, taskId: taskId });

    } else if (index == 3) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Reject Challenge", label: "Comment", index: 3, btnLabel: "Submit", showDropdown: true, contentId: contentId, taskId: taskId });

    } else if (index == 4) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Send Back to Challenger", label: "Comment", index: 4, btnLabel: "Submit", contentId: contentId, taskId: taskId });
    }
    else if (index == 5) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Invite Reviewer", label: "Comment", index: 5, btnLabel: "Invite Reviewer", reviewersList: reviewersList, reviewerCount: reviewerCount, showTags: true, contentId: contentId, taskId: taskId });
    }
    else if (index == 6) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Reject Redirect Request", label: "Comment", index: 6, btnLabel: "OK", contentId: contentId, taskId: taskId });
    }
    else if (index == 7) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Change Category", label: "Comment", index: 7, btnLabel: "OK", showDropdownTwice: true, contentId: contentId, taskId: taskId });
    }
    else if (index == 8) {
      //this.props.navigation.navigate("ApprovalOptions", {type:2,title:"Evaluate", label: "Comment", index: 8, btnLabel:"Evaluate", contentId: contentId, taskId: taskId});
    }
    else if (index == 9) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Apply For Patent", label: "Comment", index: 9, btnLabel: "Submit", contentId: contentId, taskId: taskId });
    }
    else if (index == 10) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Approve Challenge", label: "Comment", index: 10, btnLabel: "Approve", contentId: contentId, taskId: taskId });
    }
    else if (index == 11) {
      this.props.navigation.navigate("ApprovalOptions", { type: 2, title: "Reject Challenge", label: "Comment", index: 11, btnLabel: "Reject", showDropdown: true, contentId: contentId, taskId: taskId });
    }
  }

  showApprovalsPanel(formControls) {
    console.log(formControls);
    const views = [];
    for (var i = 0; i < formControls.length; i++) {
      if (formControls[i].DisplayName == "Publish") {
        if (views.indexOf('Publish') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(0)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='check-circle' />
                <Text style={styles.footerText2}>Publish</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Redirect") {
        if (views.indexOf('Redirect') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(1)}>
              <View style={styles.buttonContainer}>
                <Fontisto style={styles.footerText} name='arrow-return-right' />
                <Text style={styles.footerText2}>Redirect</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Ask Query" || formControls[i].DisplayName == "Ask query") {
        if (views.indexOf('Query') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(2)}>
              <View style={styles.buttonContainer}>
                <MaterialCommunityIcons style={styles.footerText} name='comment-question-outline' />
                <Text style={styles.footerText2}>Query</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Reject") {
        if (views.indexOf('Reject') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(3)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='x-circle' />
                <Text style={styles.footerText2}>Reject</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Invite Reviewer") {
        if (views.indexOf('Invite Reviewer') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(5)}>
              <View style={styles.buttonContainer}>
                <AntDesign style={styles.footerText} name='adduser' />
                <Text style={styles.footerText2}>Invite Reviewer</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "btnRework" || formControls[i].DisplayName == "Rework") {
        if (views.indexOf('Rework') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(4)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='refresh-cw' />
                <Text style={styles.footerText2}>Rework</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Reject Redirect Request") {
        if (views.indexOf('Reject Redirect Request') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(6)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='x-circle' />
                <Text style={styles.footerText2}>Reject Redirect Request</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Change Category") {
        if (views.indexOf('Change Category') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(7)}>
              <View style={styles.buttonContainer}>
                <AntDesign style={styles.footerText} name='edit' />
                <Text style={styles.footerText2}>Change Category</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Evaluate") {
        if (views.indexOf('Evaluate') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(8)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='check-circle' />
                <Text style={styles.footerText2}>Evaluate</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Patent") {
        if (views.indexOf('Patent') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(9)}>
              <View style={styles.buttonContainer}>
                <MaterialCommunityIcons style={styles.footerText} name='car-brake-parking' />
                <Text style={styles.footerText2}>Apply For Patent</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Approve") {
        if (views.indexOf('Approve') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(10)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='check-circle' />
                <Text style={styles.footerText2}>Approve</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }
      else if (formControls[i].DisplayName == "Review Reject") {
        if (views.indexOf('Review Reject') === -1) {
          views.push(
            <TouchableOpacity style={styles.bottomButtonsApproval} onPress={() => this.approvalsAPI(11)}>
              <View style={styles.buttonContainer}>
                <Feather style={styles.footerText} name='x-circle' />
                <Text style={styles.footerText2}>Reject</Text>
              </View>
            </TouchableOpacity>
          );
        }
      }

    }//for

    return (

      <View style={[styles.footerApproval]}>
        {views}
      </View>

    );
  }


  _renderDotIndicator() {

    let dotstyleListObjects = [
      { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.Overview ? 'red' : 'lightblue', bottom: -5 },
      { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.Details ? 'red' : 'lightblue', bottom: -5 },
      { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 },
      { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 },
      { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 },
      { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 },
      { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 }
    ];
    return (
      <PagerDotIndicator
        style={{
          top: '-60%',
          width: 30,
          transform: [{ rotate: '90deg' }]
        }}

        pageCount={this.state.editButtonClicked ? this.props.createFormJson.SubForms.length - 1 : this.props.createFormJson.SubForms.length} //6
        dotStyle={dotstyleListObjects}
        selectedDotStyle={{ backgroundColor: 'darkblue', borderRadius: 1, height: 45, }} />

    );
  }
}

function mapStateToProps(state) {
  return {
    savedAddIdeaData: state.DataReducer.savedAddIdeaDataFlag,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
    IdeaInfoById: state.DataReducer.IdeaInfoById.Comment,
    subCategory: state.DataReducer.subCategoryData,
    createFormJson: state.DataReducer.createFormDataForChallenges,
    token: state.verifyReducer.token,
    contentId: state.DataReducer.saveIdeaData.contentID,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ReduxActions, getHomePageContent, homeSearch, getIdeas, getChallenges }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewPagerViewChallenge);

var styles = StyleSheet.create({
  headerstyle: {
    backgroundColor: 'white',
    marginTop: Platform.OS === 'ios' ? 0 : - 30
  },
  mainViewStyle:{
    flex: 1,
    backgroundColor:'white'
  },
  fileIconStyle: {
    color: '#0086b3',
    fontWeight: 'bold',
    alignItems: 'center',
    fontSize: 18,
    margin: 5
  },
  containerforform: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  formContainer: {
    marginTop: 100,
  },
  backgroundStyle: {
    backgroundColor: 'white',
    marginLeft: 15,
  },
  footer: {
    position: 'absolute',
    paddingTop: 15,
    flex: 0.1,
    left: 0,
    right: 0,
    bottom: deviceHeight / 1000,
    flexDirection: 'row',
    height: 90,
    backgroundColor: 'white',
    borderWidth: 0.2,
    flexWrap: 'wrap'
  },
  footerApproval: {
    position: 'absolute',
    paddingTop: 15,
    left: 0,
    right: 0,
    bottom: deviceHeight / 1000,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderWidth: 0.2,
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  getStartedButton: {
    position: "absolute",
    bottom: 0,//15,
    left: 0,
    borderRadius: 3,
    height: 80,//55,
    width: deviceWidth,
    justifyContent: "center",
    flex: 1,
    flexDirection: 'row',
    zIndex: 100,
  },
  getStartedButtonForEdit: {
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: 3,
    height: 80,//55,
    width: deviceWidth,
    justifyContent: "center",
    flex: 1,
    flexDirection: 'row',
    zIndex: 100,
  },
  bottomButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'green'
  },
  bottomButtonsApproval: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
    marginRight: 15,
    height: 80
  },
  activityindicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0.3 * deviceHeight,
    bottom: 0,
    alignItems: "center",
    justifyContent: 'center',
  },
  floatingButton: {
    position: 'absolute',
    flex: 1,
    left: '86%',
    right: 10,
    bottom: 170 + 24,
    flexDirection: 'row',
    borderWidth: 3,
    borderColor: '#0086b3',
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    height: 40,
    backgroundColor: 'white',
    borderRadius: 100,
    elevation: 4,
    shadowOffset: { width: 5, height: 5 },
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowRadius: 10,
  },
  attachmentRibbon: {
    position: 'absolute',
    flex: 0.1,
    left: 0,
    right: 0,
    bottom: 90,
    backgroundColor: 'black',
    flexDirection: 'row',
    height: 40,
  },
  bottomButtons: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  footerText: {
    color: '#0086b3',
    fontWeight: 'bold',
    alignItems: 'center',
    fontSize: 24,
  },
  footerText2: {
    color: '#0086b3',
    alignItems: 'center',
    fontSize: 13,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 10
  },
  card: {
    padding: 10,
    width: 40,
    borderRadius: 4,
  },
  parentIconBar: {
    width: '110%',
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  childIconBar: {
    width: '15%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  childViewBadge: {
    width: '91%',
    borderRadius: 5,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#EEEEEE',
  },
  infoIcon: {
    color: 'black',
    fontWeight: 'bold',
    alignItems: 'center',
    fontSize: 27,
    backgroundColor: 'white'
  },
  childButton: {
    width: '12%',
    borderRadius: 5,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    bottom: 0,
    top: 0,
  },
})
