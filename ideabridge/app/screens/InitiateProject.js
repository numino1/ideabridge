import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, Alert, FlatList, TouchableHighlight, TouchableOpacity, Dimensions, ActivityIndicator, TextInput, KeyboardAvoidingView } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import CardApprovals from '../components/CardApprovals';
let _ = require('lodash');
import { fonts, dimensions, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast';
import { Dropdown } from 'react-native-material-dropdown';
import { CLIENT_UAT1_URL } from '../config/configuration';
import axios from "axios";
import DynamicForm, { buildTheme } from 'react-native-dynamic-form';
import { getPendingTasksForLoggedInUser } from '../actions/home';
import DatePicker from 'react-native-datepicker';
import handleError from '../config/errorHandler';
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height

class InitiateProject extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            header: null,
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            remarks: '',
            implementationDate: '',
            rejectReason: '',
            suggestions: [],
            reviewerIds: '',
            category: '',
            subCategory: '',
            subCategoryList: [],
            reviewersCounter: 1, //default is the ideator name
            implementationName: 'JDI - ' + this.props.navigation.getParam('ideaName', 0),
            tagsLabel: '',
            userList: []
        };
    }

    componentDidMount() {
        const { navigation } = this.props;
        const contentId = this.props.navigation.getParam('contentId', 'NO-data');
        this.setState({ reviewerIds: this.props.ideaData.IdeatorDetails.UserID });
        let ideaProjectType = this.props.navigation.getParam('projectType');
        let ideaName = this.props.navigation.getParam('ideaName', 0);
        let implName = ideaProjectType + ' - ' + ideaName;
        this.setState({ implementationName: implName });
        this.setState({ tagsLabel: 'Select ' + ideaProjectType + ' Leader' });
        if (this.state.userList.findIndex(data => data.name === this.props.ideaData.IdeatorName) === -1) {
            this.state.userList.push(
                {
                    name: this.props.ideaData.IdeatorName,
                }
            )
        }
    }

    initiateHomeAction() {
        this.props.getPendingTasksForLoggedInUser();
    }


    customFilterData = query => {
        //override suggestion filter, we can search by specific attributes
        if (this.state.reviewersCounter > 0) {
            return;
        }
        query = query.toLowerCase();
        this.getAllPeopleEmail(query);

    };

    executeAPI() {
        const contentId = this.props.ideaData.ContentID;
        let data = {
            contentID: contentId,
            ProjectLeadId: this.state.reviewerIds,
            ProjectName: this.state.implementationName,
            ProjectStartDate: this.state.implementationDate,
            Remark: this.state.remarks
        }
        this.props.initiateProject({ data: data })
            .then(() => {
                if (this.props.success) {
                    console.log("in initiateProject props success");
                    this.props.updateAPIStatusBody({
                        data: { loading: false, success: false }
                    });
                    this.refs.toast.show('Implementation intiated successfully.', 500, () => {
                        this.initiateHomeAction();
                        this.props.navigation.navigate('Approvals');
                    });
                }

                if (this.props.error) {
                    console.log(
                        "in initiateProject props error",
                        this.props.errorMsg
                    );
                    //alert("Some error has occured. Please try again");
                }
            });
    }

    validateForInputs(index) {
        if (this.state.implementationName == '') {
            Alert.alert("Please Enter Implementation Name");
        } else if (this.state.implementationDate == '') {
            Alert.alert("Please Enter Implementation Date");
        } else if (this.state.reviewerIds == '') {
            Alert.alert("Please select a leader");
        }
        else {
            this.executeAPI();
        }
    }

    renderSpinner() {
        if (this.props.loading) {
            return (
                <View pointerEvents="none" style={[styles.activityindicator]}>
                    <ActivityIndicator size="large" color='red' />
                </View>
            );
        } else {
            return null;
        }
    }


    getAllPeopleEmail(text) {
        console.log("in API getAllPeopleEmail");
        let self = this;
        let tagList = [];
        const url = CLIENT_UAT1_URL + `/User/UserSearch`;
        let config = {
            headers: {
                api_key: 'LgOiuSu64U',
                Authorization: this.props.token
            },
            params: {
                prefix: text,
                excludeIDs: '0',
                includeCurrentUser: true,
                onlyIdeaEntryOperators: false
            },
        };

        return axios
            .get(url, config)
            .then(function (response) {
                console.log("In getAllTeamMembers approvals", response);
                if (response.data !== null && response.data.length > 0) {
                    response.data.map((item, i) => {
                        if (self.state.userList.findIndex(data => data.name === item.name) === -1) {
                            tagList.push(
                                {
                                    name: item.name,//item.info,
                                    value: item.id
                                });
                        }
                    });
                }
                console.log("Teammembers list initiate project", tagList);
                self.setState({ suggestions: tagList });
            })
            .catch(function (error) {
                console.log("In getAllTeamMembers initiate project", error);
                self.setState({ suggestions: [] });
                handleError(error, false);
            });
    }

    formDataChanges(data) {
        let newDataString = "";
        if (data.tags_leader.userAnswer.length == 0) {
            this.state.userList = [];
        }
        if (data.tags_leader.userAnswer.length > 0) {
            this.setState({ suggestions: [] });
        }
        if (data.tags_leader.userAnswer.length > 0) {
            newDataString = Object.keys(data.tags_leader.userAnswer).map(function (k) {
                return data.tags_leader.userAnswer[k].value
            }).join(",");
        }
        this.setState({ reviewersCounter: data.tags_leader.userAnswer.length })
        this.setState({ reviewerIds: newDataString });
    }

    getSubCategory(category) {
        this.setState({ category });
        let subCategoryList = [];
        this.props.allCategorySubCategoryData.map((item) => {
            if (item.groupValue == category) {
                subCategoryList.push(
                    {
                        label: item.subGroupLabel,
                        value: item.subGroupValue,
                    });
            }
        })
        this.setState({ subCategoryList });
    }

    render() {
        const title = this.props.navigation.getParam('title', 'NO-data');
        const label = this.props.navigation.getParam('label', 'NO-data');
        const btnLabel = this.props.navigation.getParam('btnLabel', 'NO-data');
        const index = this.props.navigation.getParam('index', 'NO-data');
        const ideaName = this.props.navigation.getParam('ideaName', 'NO-data');
        const minDate = new Date();
        const formTheme = {
            label: {
                fontWeight: 'bold',
                fontSize: 24,
                bottom: 10,
            },
        }
        const mytheme = buildTheme({}, {}, formTheme);
        const fieldsArray = [
            {
                key: 'tags_leader',
                type: 'tags',
                label: this.state.tagsLabel,
                data: this.state.suggestions,
                filterForTags: this.customFilterData,
                values: this.state.userList,
                searchInputPlaceholder: 'Type name or email id'
            }
        ];
        return (
            <View style={styles.container}>
                <View style={styles.ideaNameView}>
                    <Text style={styles.ideaNameText} >{title}: <Text style={styles.ideaNameTextName}>{ideaName}</Text></Text>
                </View>
                {this.renderSpinner()}
                <View style={styles.viewBelowHeader} >
                    <ScrollView style={{ flex: 1 }}>
                        <Text style={styles.implementationText}>Name of the Implementation</Text>
                        <TextInput
                            style={styles.implementationTextInputStyle}
                            onChangeText={(text) => this.setState({ implementationName: text })}
                            value={this.state.implementationName}
                            multiline={true}
                            placeholder=''
                        />

                        <Text style={styles.estImplText}>Estimated Implementation Completion</Text>
                        <DatePicker
                            style={styles.datePickerStyle}
                            date={this.state.implementationDate}
                            mode="date"
                            minDate={minDate}
                            placeholder="Select Date"
                            format="DD/MM/YYYY"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={(startDate) => { this.setState({ implementationDate: startDate }) }}
                            customStyles={{
                                dateText: {
                                    alignSelf: 'flex-start',
                                    fontSize: 16,
                                },
                                placeholderText: {
                                    alignSelf: 'flex-start',
                                    fontSize: 16,
                                    color: '#A9A9A9'
                                }
                            }}
                        />

                        <Text style={styles.labelTextStyle}>{label}</Text>

                        <TextInput
                            style={styles.remarkTextInput}
                            onChangeText={(text) => this.setState({ remarks: text })}
                            value={this.state.remarks}
                            multiline={true}
                            placeholder='Enter Remarks'
                            maxLength={250}
                        />

                        <View style={[styles.elementsContainer]}>
                            <DynamicForm
                                form={fieldsArray}
                                theme={mytheme}
                                style={styles.backgroundStyle}
                                onFormDataChange={(data) => this.formDataChanges(data)}
                            />
                        </View>
                        <View style={styles.xtraViewStyle} />
                    </ScrollView>
                    <View style={styles.outerContainer}>
                        <View style={styles.buttonViewStyle}>
                            <TouchableOpacity onPress={() => { this.validateForInputs(index) }}>
                                <Text style={styles.buttonTextStyle}>{btnLabel}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonViewStyle}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Approvals') }}>
                                <Text style={styles.buttonTextStyle}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>

                <Toast ref="toast" position='top' />
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        approvals: state.homeReducer.pendingTasks,
        loading: state.DataReducer.generalAPIStatusBody.loading,
        success: state.DataReducer.generalAPIStatusBody.success,
        error: state.DataReducer.generalAPIStatusBody.error,
        errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
        rejectionReasons: state.DataReducer.rejectionReasons,
        allCategorySubCategoryData: state.DataReducer.allCategorySubCategoryData,
        token: state.verifyReducer.token,
        ideaData: state.DataReducer.IdeaInfoById,

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ReduxActions, getPendingTasksForLoggedInUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(InitiateProject);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
        maxWidth: deviceWidth
    },
    buttonTextStyle: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    buttonViewStyle: {
        borderRadius: 3,
        height: 70,
        alignItems: 'center',
        margin: 5,
        padding: 5,
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#F51173'
    },
    xtraViewStyle: {
        height: 100
    },
    remarkTextInput: {
        fontSize: 18,
        margin: 5,
        top: 0,
        flexDirection: 'row',
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    labelTextStyle: {
        left: 5,
        flexDirection: 'row',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25
    },
    datePickerStyle: {
        width: 200,
        marginBottom: 30,
        margin: 5,
        alignItems: 'flex-start',
    },
    estImplText: {
        left: 5,
        flexDirection: 'row',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25
    },
    viewBelowHeader: {
        marginLeft: 5,
        margin: 10,
        flex: 1,
        marginTop: 0
    },
    implementationText: {
        margin: 5,
        flexDirection: 'row',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25,
        marginBottom: 0
    },
    implementationTextInputStyle: {
        fontSize: 18,
        margin: 5,
        marginBottom: 30,
        top: 0,
        flexDirection: 'row',
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    ideaNameView: {
        flexDirection: 'row',
        color: 'black',
        flexWrap: 'wrap',
        margin: 5,
        marginTop: 30,
        marginBottom: 10
    },
    ideaNameTextName: {
        fontSize: 18
    },
    ideaNameText: {
        left: 5,
        flex: 1,
        fontWeight: 'bold',
        fontSize: 25,
        marginBottom: 0
    },
    elementsContainer: {
        height: 500,
        marginLeft: -10,
        paddingTop: 10
    },
    customTagsContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "flex-start",
        backgroundColor: "#efeaea",
        width: '100%'
    },
    customTag: {
        backgroundColor: "#9d30a5",
        justifyContent: "center",
        alignItems: "center",
        height: 30,
        marginTop: 5,
        borderRadius: 30,
        padding: 8
    },
    autocompleteContainer: {
        flex: 1,
        position: "absolute",
        right: 10,
        top: 40,
        backgroundColor: '#FFFFFF',
        overflow: 'visible',
        height: 10,
        zIndex: 1,
    },
    label: {
        color: "#614b63",
        fontWeight: "bold",
        marginBottom: 10
    },
    header: {
        flex: 1,
        backgroundColor: "#9d30a5",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        paddingTop: 15,
        marginBottom: 10,
    },
    outerContainer: {
        flex: 1,
        flexDirection: 'row',
        position: 'absolute',
        bottom: deviceHeight / 1000,
        left: 0, right: 0
    },
})