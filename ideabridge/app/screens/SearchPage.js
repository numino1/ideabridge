import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { systemWeights } from 'react-native-typography';
import { Card } from 'react-native-elements';
import { homeSearch } from '../actions/home';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getIdeaInfoById, getChallengeInfoById, } from "../actions/actions";
import { updateAPIStatusBody } from '../actions/actions';
import { fonts, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';

class SearchPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      text: '',
      searchDataCard: [],

    };
  }

  searchData = (text) => {
    const searchText = {
      'text': text,

    }

    this.props.homeSearch(searchText).then(() => {
      this.setState({
        searchDataCard: this.props.searchData
      })
    });
  }

  componentDidMount() {
    const { navigation } = this.props;
    const text = navigation.getParam('text', ' ');
    if (text) {
      this.searchData(text);
    }
  }

  getIdeaByIdAPI(contentId) {

    this.props.getIdeaInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getIdeaByIdAPI props success");
          let ideaData = {
            data: this.props.IdeaInfoById.ContentData,
            costDetailsData: this.props.IdeaInfoById.CostDetails,
            challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
            canRespond: this.props.IdeaInfoById.CanRespond,
            canEdit: this.props.IdeaInfoById.CanEdit,
          };
          if (this.props.userDetails.UserID == this.props.IdeaInfoById.CreatorUserId) {
            ideaData.contentId = this.props.IdeaInfoById.ContentID;
            ideaData.contentStatus = this.props.IdeaInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewIdea", ideaData);

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getIdeaByIdAPI props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  getChallengesByIdAPI(contentId) {

    this.props.getChallengeInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getChallengesByIdAPI props success");
          let challengeData = {
            data: { ...this.props.ChallengeInfoById.ContentData },
            title: this.props.ChallengeInfoById.Title,
            type: this.props.ChallengeInfoById.ChallengeType,
            attachments: this.props.ChallengeInfoById.Attachment,
            canEdit: this.props.ChallengeInfoById.CanEdit,
            canRespond: this.props.ChallengeInfoById.CanRespond
          };
          if (this.props.userDetails.UserID == this.props.ChallengeInfoById.ChallengerDetails.UserID) {
            challengeData.contentId = this.props.ChallengeInfoById.ContentID;
            challengeData.contentStatus = this.props.ChallengeInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewChallenge", challengeData);

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getChallengesByIdAPI props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  listItemClicked(itemData) {
    this.getIdeaByIdAPI(itemData.ContentId);

  }

  listItemClickedChallenges(itemData) {
    this.getChallengesByIdAPI(itemData.ContentId);
  }

  renderSpinner() {

    if (this.props.loading) {
      return (
        <ActivityIndicator size="large" color='red' />
      );
    } else {

      return null;
    }
  }

  render() {
    const { navigation, searchData } = this.props;
    const text = navigation.getParam('text', ' ');

    return (
      <View style={styles.mainviewStyle}>
        <OfflineNotice />
        <ScrollView style={styles.container}>
          <View style={[styles.elementsContainer]}>
            <Text style={[systemWeights.bold, { ...header }]}>Search</Text>
          </View>
          {this.renderSpinner()}
          {this.props.searchData == '' ?
            <View style={styles.resSearchData}>
              {this.props.loading ? null : <Text style={styles.resultsNotFoundStyle}>Results not found</Text>}
            </View> :
            <View style={styles.flatListStyle}>

              <FlatList
                data={this.state.searchDataCard}
                renderItem={({ item: rowData }) => {
                  return (
                    (rowData.ContentTypeId == '1') ?
                      <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                        <Card title={null}
                          containerStyle={styles.main}>
                          <View style={{ flexDirection: 'row' }}>
                            <View style={styles.bar} />
                            <View style={{ flexDirection: 'column' }}>

                              {(rowData.ContentTypeId == '1') ? <Text>Idea</Text> : null}
                              <Text style={[systemWeights.semibold, styles.text2]}>
                                {rowData.Title}
                              </Text>
                            </View>
                          </View>
                        </Card>
                      </TouchableOpacity> : (rowData.ContentTypeId == '2') ?
                        <TouchableOpacity onPress={() => { this.listItemClickedChallenges(rowData) }}>
                          <Card title={null}
                            containerStyle={styles.main}>
                            <View style={{ flexDirection: 'row' }}>
                              <View style={styles.bar} />
                              <View style={{ flexDirection: 'column' }}>

                                {(rowData.ContentTypeId == '2') ? <Text>Challenge</Text> : null}
                                <Text style={[systemWeights.semibold, styles.text2]}>
                                  {rowData.Title}
                                </Text>
                              </View>
                            </View>
                          </Card>
                        </TouchableOpacity> : null
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  let count = Object.keys(state.DataReducer.addIdeaInfo).length;
  return {
    myIdeaCount: count > 0 ? count : 0,
    userDetails: state.verifyReducer.userDetails,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
    content: (state.homeReducer.content),
    notificationCount: state.homeReducer.notification.NotificationCount,
    approvalCount: state.homeReducer.notification.ApprovalCount,
    searchData: state.homeReducer.serachData,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ homeSearch, getIdeaInfoById, getChallengeInfoById, updateAPIStatusBody }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);

const styles = StyleSheet.create({
  mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  flatListStyle: {
    paddingBottom: 90
  },
  resultsNotFoundStyle: {
    fontSize: fonts.sm + 5,
  },
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },
  resSearchData: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
  bar: {
    width: 4,
    height: 15,
    backgroundColor: '#EE2C38',
    left: -15
  },
  text1: {
    color: '#6495ED',
    fontSize: fonts.sm + 5
  },
  text2: {
    color: '#6495ED',
    fontSize: fonts.sm + 3,
    marginTop: 16
  },
  main: {
    ...shadow,
    flex: 1,
    borderRadius: 5,
  },
})