import React, { Component } from 'react';
import { View, Button, Dimensions, Platform, StyleSheet, TextInput, PermissionsAndroid, TouchableOpacity, Alert, ActivityIndicator, ScrollView, FlatList, ProgressBarAndroid, ProgressViewIOS } from 'react-native';
import { Header, Icon, Text, Card } from 'react-native-elements';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import { bindActionCreators } from "redux";
import Toast, { DURATION } from 'react-native-easy-toast';
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import data from './apiResponse';
import DynamicForm, { buildTheme } from 'react-native-dynamic-form';
import OfflineNotice from '../components/OfflineNotice';
import { homeSearch, getHomePageContent, getIdeas, getChallenges } from '../actions/home';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Doodle from './Doodle';
import DocumentPicker from 'react-native-document-picker';
let _ = require('lodash');
var RNFS = require('react-native-fs');
import axios from "axios";
import ImagePicker from 'react-native-customized-image-picker';
import { CLIENT_UAT1_URL } from '../config/configuration';
import { DOC_TYPES } from '../config/attachmentDocTypes';
import { fonts, fontStyle, dimensions } from '../css/GlobalCss';
import handleError from '../config/errorHandler';


let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height
const ELEMENT = {
    TextBox: "text",
    Number: "number",
    TextArea: "textarea",
    SourceDropdown: "select",
    SourceBoundMultiselectDropdown: 'select',
    ChallengeTypeDropdown: 'select',
    Dropdown: "select",
    CheckBox: "checkbox-group",
    RadioButton: 'radio-group',
    Switch: 'checkbox-group',
    FileUpload: 'text',
    UserField: 'tags',
    TagsField: 'tags',
    Caption: 'paragraph',
};

const options = {
    title: 'my app',
    chooseFromLibraryButtonTitle: 'Choose from Gallery'
}
const options1 = {
    title: 'my app',
    takePhotoButtonTitle: 'Take photo with your camera',

}
class ViewPagerAddChallenge extends Component {
    formArray = [];
    requiredFieldsArray = [];
    propertiesArray = [];
    subFormsPropertiesArray = [];
    tagsList = [];
    pageCountAdjust = false;
    saveIdeaDetails = {
        FormId: 59,
        Properties: [],
        ContentId: 0,
        ContentTypeId: 2,
        ProjectTypeId: 0,
        ContentType: null,
        DoSubmit: true,
        ParentContentId: 0,
        Files: [],
        SubForms: null,
        Name: null,
        HasVerticalTabs: false,
        GlobalOptions: null,
        IsActive: false,
        UserTypeId: 0,
        Sequence: 0,
    };

    attachmentFileList = [];

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            vpPagePosition: 0,
            ideaId: '',
            createFormData: this.props.createFormJson,//data,
            elementDisableForCostDetails: true,
            elementDisableForImplement: true,
            elementDisableForText: true,
            disableSaveButton: true,
            disableSubmitButton: true,
            allTagsList: [],
            peopleTagsList: [],
            tagsSuggestions: [],
            perPageRequiredStatus: { Overview: false, Details: false, Duration: false, ChallengeType: false, SeekingGain: false },
            selectedCategory: 0,
            formDataModified: false,
            contentId: 0,
            footerVisible: false,
            fileattached: false,
            fileName: '',
            refresh: false,
            attachmentLoadComplete: true,
            attachmentStatus: true,
            progressIncrement: 0,
            context: 'NO-CONTEXT',
            challengeId: 0,
            challengeType: 0,
            attachDetails: {}
        };
    }

    onAttachment = data => {
        this.setState(data);
        let responseObj = data.attachDetails;
        let fileSize = data.attachDetails.size;
        this.readFileAndAttach(responseObj, fileSize);
    };

    componentDidMount() {
        const { navigation } = this.props;
        const context = navigation.getParam('context', 'NO-CONTEXT');
        this.setState({ context: context });
        const challengeId = navigation.getParam('challengeContentId', 0);
        this.setState({ challengeId: challengeId });
    }
    componentWillUnmount() {
        this.props
            .clearSubCategoryValues();
    }

    initiateHomeAction() {
        this.props.getHomePageContent()
            .then(() => {
                if (this.props.success) {
                    console.log("in getHomePageContent props success");
                    this.props.updateAPIStatusBody({
                        data: { loading: false, success: false }
                    });
                }

                if (this.props.error) {
                    console.log(
                        "in getHomePageContent props error",
                        this.props.errorMsg
                    );
                    this.props.updateAPIStatusBody({
                        data: { loading: false, error: false, errorMsg: "" }
                    });
                }
            });

        this.props.getIdeas()
            .then(() => {
                if (this.props.success) {
                    console.log("in getIdeas props success");
                    this.props.updateAPIStatusBody({
                        data: { loading: false, success: false }
                    });
                }

                if (this.props.error) {
                    console.log(
                        "in getIdeas props error",
                        this.props.errorMsg
                    );
                    this.props.updateAPIStatusBody({
                        data: { loading: false, error: false, errorMsg: "" }
                    });
                }
            });

        this.props.getChallenges();
    }

    mycustomFilterData = query => {
        console.log("MY QUERY", query);
        query = query.toUpperCase();
        let searchResults = this.state.suggestions2.filter(s => {
            return (
                s.value.toUpperCase().includes(query) ||
                s.name.toUpperCase().includes(query)
            );
        });
        console.log("My RESULTS", searchResults);
        return searchResults;
    };

    getAllIdeaTags(text) {
        let _this = this;
        let tagList = [];
        const url = CLIENT_UAT1_URL + `/Idea/IdeaTagAutoComplete`;
        let config = {
            headers: {
                api_key: 'LgOiuSu64U',
                Authorization: this.props.token
            },
            params: {
                term: 'abc'
            },
        };
        return axios
            .get(url, config)
            .then(function (response) {
                console.log("In getAllAutoCompleteTags", response);
                if (response.data.length > 0) {
                    response.data.map((item, i) => {
                        tagList.push(
                            {
                                name: item.TagName,
                                // value: item.TagId
                            });
                    });
                }
                console.log("Tags list", tagList);
                _this.setState({ tagsSuggestions: tagList });
            })
            .catch(function (error) {
                console.log("In getAllAutoCompleteTags", error);
                _this.setState({ tagsSuggestions: [] });
                handleError(error, false);
            });
    }

    getAllPeopleEmail(text) {
        let _this = this;
        let tagList = [];
        const url = CLIENT_UAT1_URL + `/User/PeopleSearch`;
        let config = {
            headers: {
                api_key: 'LgOiuSu64U',
                Authorization: this.props.token
            },
            params: {
                term: text,
                excludeIDs: '0',
                formPropertyId: 24,
                includeSubmittingUser: false
            },
        };

        return axios
            .get(url, config)
            .then(function (response) {
                console.log("In getAllTeamMembers", response);
                if (response.data.length > 0) {
                    response.data.map((item, i) => {
                        tagList.push(
                            {
                                name: item.info,
                                value: item.id,
                            });
                    });
                }
                console.log("Teammembers list", tagList);
                _this.setState({ peopleTagsList: tagList });
            })
            .catch(function (error) {
                console.log("In getAllTeamMembers", error);
                _this.setState({ peopleTagsList: [] });
                handleError(error, false);
            });
    }

    customFilterDataTags = text => {
        this.getAllIdeaTags(text);
    }

    customFilterDataTeamMembers = (text) => {
        this.getAllPeopleEmail(text);
    }

    renderSpinner() {
        //check state value of loading variable
        if (this.props.loading) {
            return (
                <View pointerEvents="none" style={[styles.activityindicator]}>
                    <ActivityIndicator size="large" color='red' />
                </View>
            );
        } else {
            return null;
        }
    }

    file() {
        DocumentPicker.pick({
            type: DOC_TYPES,
        }).then((res, error) => {

            if (!error) {
                if (res.size < 5000000) {
                    this.setState({ formDataModified: true });
                    console.log("file type", res);
                    if (this.attachmentFileList.findIndex(item => item.fileName === res.name) === -1) {
                        this.setState({ fileattached: true });
                        var path = res.uri;
                        RNFS.readFile(path.replace(/%20/g, '\ '), 'base64')
                            .then(responseString => {
                                let fileObject = {
                                    fileName: res.name,
                                    fileByteString: responseString,
                                    fileType: res.type,
                                    fileId: 0,
                                    key: this.attachmentFileList.length,
                                    progressBarShow: false,
                                    progressIncrement: 0,
                                    success: true,
                                    uploaded: false,
                                };

                                this.attachmentFileList.push(fileObject);
                                console.log("attachment list", this.attachmentFileList);
                                this.setState({ refresh: true });
                            })
                            .catch((err) => {
                                console.log("RNFS ReadFile Error", err);
                                this.setState({ fileattached: false });
                            });
                    }
                } else {
                    Alert.alert("Please upload a file of less than 5 MB");
                }
            } else {
                console.log("Document Picker Error", error);
            }
        });
    }
    imageNew() {
        ImagePicker.openPicker({
            isSelectBoth: true,
        }).then(imageInfo => {
            let image = '';
            if (Platform.OS === 'android') {
                image = imageInfo[0];
            } else {
                image = imageInfo;
            }

            console.log("IMAGE Info", image);
            let fileSize = 0;
            let responseObj = image;
            if (image.size) {
                fileSize = image.size;
                this.readFileAndAttach(responseObj, fileSize);
            }
            else {
                RNFS.stat(image.path)
                    .then((fileInfo) => {
                        fileSize = fileInfo.size;
                        this.readFileAndAttach(responseObj, fileSize);
                    })
                    .catch((error) => {
                        console.log("RNFS File stat error in Image");
                    })
            }

        }).catch(error => {
            console.log("ImagePicker.openPicker", error);
        });
    }

    readFileAndAttach(response, fileSize) {
        console.log('response object===>', response)
        if (fileSize < 5000000) { //implement function and call in if else above as async call gets execited later
            this.setState({ formDataModified: true });

            let fileName = response.path.split('/').pop();

            if (this.attachmentFileList.findIndex(item => item.fileName === fileName) === -1) {
                this.setState({ fileattached: true });
                var path = response.path;
                RNFS.readFile(path.replace(/%20/g, '\ '), 'base64')
                    .then(responseString => {
                        let fileObject = {
                            fileName: fileName,
                            fileByteString: responseString,
                            fileType: response.mime,
                            fileId: 0,
                            key: this.attachmentFileList.length,
                            progressBarShow: false,
                            progressIncrement: 0,
                            success: true,
                            uploaded: false,
                        };
                        this.attachmentFileList.push(fileObject);
                        this.setState({ refresh: true });
                    })
                    .catch((err) => {
                        console.log("RNFS ReadFile Error for Image", err);
                        this.setState({ fileattached: false });
                    });
            }
        } else {
            Alert.alert("Please upload a file of less than 5 MB");
        }
    }

    opendoodle() {

        this.props.navigation.navigate('Doodle', { position: this.props.position, onAttachment: this.onAttachment });
    }

    camera() {
        ImagePicker.launchCamera(options1, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else {
                const source = { uri: response.uri };
                this.setState({ avatarSourceCamera: source });
            }
        });
    }

    async openPhoneCamera() {
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
                'title': 'External Storage Access',
                'message': 'App needs to access your external storage'
            })
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                ImagePicker.openCamera({
                    compressQuality: 50,
                    minCompressSize: 2048
                }).then(imageInfo => {
                    let image = '';
                    if (Platform.OS === 'android') {
                        //array of objects
                        image = imageInfo[0];
                    } else {
                        //object
                        image = imageInfo;
                    }

                    let fileSize = 0;
                    let responseObj = image;
                    if (image.size) {
                        fileSize = image.size;
                        this.readFileAndAttach(responseObj, fileSize);
                    }
                    else {
                        RNFS.stat(image.path)
                            .then((fileInfo) => {
                                fileSize = fileInfo.size;
                                this.readFileAndAttach(responseObj, fileSize);
                            })
                            .catch((error) => {
                                console.log("openPhoneCamera RNFS File stat error in Image");
                            })
                    }

                }).catch(error => {
                    console.log("openPhoneCamera ImagePicker.openPicker===============>", error);
                });
            } else {
                Alert.alert("Permission denied")
            }
        } catch (err) {
            console.warn(err)
        }
    }

    openPhoneCameraIOS() {
        ImagePicker.openCamera({
            compressQuality: 50,
            minCompressSize: 2048,
            openCameraOnStart: true,
        }).then(imageInfo => {
            let image = '';
            if (Platform.OS === 'android') {
                //array of objects
                image = imageInfo[0];
            } else {
                //object
                image = imageInfo;
            }

            let fileSize = 0;
            let responseObj = image;
            if (image.size) {
                fileSize = image.size;
                this.readFileAndAttach(responseObj, fileSize);
            }
            else {
                RNFS.stat(image.path)
                    .then((fileInfo) => {
                        fileSize = fileInfo.size;
                        this.readFileAndAttach(responseObj, fileSize);
                    })
                    .catch((error) => {
                        console.log("openPhoneCamera RNFS File stat error in Image");
                    })
            }

        }).catch(error => {
            console.log("openPhoneCamera ImagePicker.openPicker===============>", error);
        });
    }

    toggle() {
        this.setState({ footerVisible: !this.state.footerVisible })
    }

    removeattachment(rowData) {
        //remove from list 
        this.attachmentFileList = this.attachmentFileList.filter((item) => { return item.fileName != rowData.fileName });
        console.log("REMOVE 2", this.attachmentFileList);
        if (this.attachmentFileList.length == 0) {
            this.setState({ fileattached: false });
        }
        this.setState({ refresh: true });
        if (rowData.fileId == 0) {
            this.refs.toast.show('The attachment has been removed successfully.', 500, () => {
            });
        }
        //make remove api call only if rowData.fileId exists
        if (rowData.fileId != 0) {
            this.props.removeAttachmentFile({
                data: rowData.fileId
            })
                .then(() => {
                    if (this.props.success) {
                        console.log("in removeAttachmentFile props success", this.props.contentId);
                        this.props.updateAPIStatusBody({
                            data: { loading: false, success: false }
                        });
                        this.refs.toast.show('The attachment has been removed successfully.', 500, () => {
                        });
                    }
                    if (this.props.error) {
                        console.log(
                            "in removeAttachmentFile props error",
                            this.props.errorMsg
                        );
                        this.props.updateAPIStatusBody({
                            data: { loading: false, error: false, errorMsg: "" }
                        });
                    }
                });
        }
    }

    refreshAttachment(rowData) {
        let fileObject = {
            'contentFileName': rowData.fileName,
            'contentFileBytes': rowData.fileByteString,
            'fileType': rowData.fileType,
            'key': rowData.key,
            'contentId': this.state.contentId
        }
        let status = this.fileAttachAPI(fileObject);
    }


    showIconByFileType(filetype, fileName) {
        switch (filetype) {
            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            case "org.openxmlformats.wordprocessingml.document":
            case "com.microsoft.word.doc":
            case "application/msword":
                {
                    return (
                        <View >
                            <AntDesign style={styles.fileIconStyle} name='wordfile1' />
                        </View>
                    );
                }
            case "org.openxmlformats.spreadsheetml.sheet":
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
            case "com.microsoft.excel.xls":
            case "application/vnd.ms-excel":
                {
                    return (
                        <View >
                            <FontAwesome style={styles.fileIconStyle} name='file-excel-o' />
                        </View>
                    );

                }
            case "org.openxmlformats.presentationml.presentation":
            case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
            case "com.microsoft.powerpoint.​ppt":
            case "application/vnd.ms-powerpoint":
                {
                    return (
                        <View >
                            <AntDesign style={styles.fileIconStyle} name='pptfile1' />
                        </View>
                    );
                }
            case "public.plain-text":
            case "text/plain":
            case "public.text":
            case "text/rtf":
                {
                    return (
                        <View >
                            <Entypo style={styles.fileIconStyle} name='text-document' />
                        </View>);
                }
            case "com.adobe.pdf":
            case "application/pdf":
                {
                    return (
                        <View >
                            <AntDesign style={styles.fileIconStyle} name='pdffile1' />
                        </View>);
                }
            case "public.jpeg":
            case "public.png":
            case "com.microsoft.bmp":
            case "com.compuserve.gif":
            case "image/jpeg":
            case "image/gif":
            case "image/png":
            case "image/bmp":
                {
                    return (
                        <View >
                            <Entypo style={styles.fileIconStyle} name='image' />
                        </View>);
                }
            case "video/mp4":
            case "video/mpeg":
            case "public.mpeg-4":
                {
                    return (
                        <View >
                            <Entypo style={styles.fileIconStyle} name='video' />
                        </View>);
                }
            default:
                {
                    return (
                        <View >
                            <Entypo style={styles.fileIconStyle} name='attachment' />
                        </View>
                    );
                }
        }
    }

    renderAttachmentButton() {
        if (!this.state.footerVisible) {
            return (
                <TouchableOpacity style={styles.floatingButton} onPress={() => this.toggle()} >
                    <AntDesign name='plus' style={styles.plusIconStyle} />
                </TouchableOpacity>);
        } else {
            return (
                <View>
                    <View style={[styles.attachmentRibbon, !this.state.fileattached ? { bottom: 90 } : { bottom: 90 * 2 }]} >
                        <Text style={styles.attachmentHeaderStyle}>Attachments</Text>
                    </View>
                    {
                        this.state.fileattached &&
                        <View style={[styles.footer, styles.flatListViewStyle]}>

                            <FlatList
                                contentContainerStyle={{ paddingBottom: 20 }}
                                style={{ padding: 10 }}
                                key={this.attachmentFileList.length}
                                data={this.attachmentFileList}
                                extraData={this.state.refresh}
                                renderItem={({ item: rowData }) => {
                                    return (

                                        <View pointerEvents={this.props.loading ? "auto" : "auto"} style={styles.fLContainerView}>
                                            {this.showIconByFileType(rowData.fileType, rowData.fileName)}
                                            <Text numberOfLines={1} ellipsizeMode='head' style={styles.fLFileName}>{rowData.fileName} </Text>
                                            <View style={styles.progressBarView}>
                                                {(Platform.OS === 'android')
                                                    ?
                                                    (rowData.progressBarShow && <ProgressBarAndroid progressTintColor="green" progress={rowData.progressIncrement} styleAttr="Horizontal" indeterminate={false} />)
                                                    :
                                                    (rowData.progressBarShow && <ProgressViewIOS style={{ transform: [{ scaleX: 1.0 }, { scaleY: 4 }], height: 12, width: 100 }} progressTintColor="green" progress={rowData.progressIncrement} />)
                                                }
                                                {rowData.success == false && rowData.progressBarShow == false && <Text style={styles.fileUploadFailedTextStyle}>Failed </Text>}
                                                {rowData.success == false && rowData.progressBarShow == false && <FontAwesome style={styles.refreshAttachmentStyle} name='refresh' onPress={() => this.refreshAttachment(rowData)} />}
                                                <Entypo style={styles.removeAttachmentStyle} name='cross' onPress={() => { if (rowData.progressBarShow == false) { this.removeattachment(rowData) } }} />
                                            </View>
                                        </View>

                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    }
                    <TouchableOpacity style={[styles.floatingButton, !this.state.fileattached ? { bottom: 100 } : { bottom: 100 * 2 }]} onPress={() => this.toggle()} >
                        <AntDesign name='close' style={{ fontSize: 25, color: '#0086b3' }} />
                    </TouchableOpacity>


                    <View style={styles.footer}>
                        <TouchableOpacity style={styles.bottomButtons} onPress={() => this.file()}>
                            <View style={styles.buttonContainer}>
                                <Entypo style={styles.footerText} name='attachment' />
                                <Text style={styles.footerText2}>Attachment</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.bottomButtons} onPress={() => this.imageNew()} >
                            <View style={styles.buttonContainer}>
                                <Entypo style={styles.footerText} name='image' />
                                <Text style={styles.footerText2}>Image</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.bottomButtons} onPress={() => this.opendoodle()}>
                            <View style={styles.buttonContainer}>
                                <Entypo style={styles.footerText} name='pencil' />
                                <Text style={styles.footerText2}>Doodle</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.bottomButtons} onPress={() => {
                            if (Platform.OS === 'android') {
                                this.openPhoneCamera();
                            } else {
                                this.openPhoneCameraIOS();
                            }
                        }}>
                            <View style={styles.buttonContainer}>
                                <Entypo style={styles.footerText} name='camera' />
                                <Text style={styles.footerText2}>Camera</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }

    }


    renderSaveButton() {
        return (
            <View style={styles.getStartedButton}>
                <TouchableOpacity
                    style={[styles.bottomButton, (this.state.disableSaveButton) ? { backgroundColor: '#EEEEEE' } : { backgroundColor: '#4e12bc' }]}
                    onPress={() => { this.validateFormData('save') }}
                    disabled={this.state.disableSaveButton}
                >
                    <Text
                        style={styles.saveButtonText}
                    >
                        Save
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.bottomButton, (this.state.disableSubmitButton) ? { backgroundColor: '#EEEEEE' } : { backgroundColor: '#F51173' }]}
                    onPress={() => { this.validateFormData('submit') }}
                    disabled={this.state.disableSubmitButton}
                >
                    <Text
                        style={styles.saveButtonText}
                    >
                        Submit
          </Text>
                </TouchableOpacity>
            </View>
        );
    }

    validateFormData(clicked) {
        if (this.state.formDataModified || !this.state.formDataModified && clicked == 'submit') {
            let responses = this.getAllResponses();
            //default values
            const fieldIndex = this.propertiesArray.findIndex(item => item.FormFieldName === 'applicable_for_patent_application');
            if (fieldIndex != -1 && this.propertiesArray[fieldIndex].Value == null) {
                this.propertiesArray[fieldIndex].Value = 0;
            }
            //get form responses
            for (var key in responses) {
                if (responses.hasOwnProperty(key)) {
                    const foundIndex = this.propertiesArray.findIndex(item => item.FormFieldName === responses[key].actualName)
                    if (this.propertiesArray[foundIndex].FormPropertyTypeId == 5 || this.propertiesArray[foundIndex].FormPropertyTypeId == 6 || this.propertiesArray[foundIndex].FormPropertyTypeId == 15) {
                        //dropdown, sourcedropdown
                        var answer = responses[key].userAnswer[0];
                        this.propertiesArray[foundIndex].Value = answer;
                    } else if (this.propertiesArray[foundIndex].FormPropertyTypeId == 18 || this.propertiesArray[foundIndex].FormPropertyTypeId == 16) {
                        //switch , checkbox
                        this.propertiesArray[foundIndex].Value = responses[key].userAnswer.regular.length == 0 ? 0 : 1; // 1 - on, 0 - off , 
                    }
                    else if (this.propertiesArray[foundIndex].FormPropertyTypeId == 12 || this.propertiesArray[foundIndex].FormPropertyTypeId == 11) {
                        //tagsfield , userfield
                        let propertyID = this.propertiesArray[foundIndex].FormPropertyTypeId;
                        if (responses[key].userAnswer.length > 0) {
                            let tagValues = responses[key].userAnswer.map((item) => {
                                if (propertyID == 12) {
                                    return item.name;
                                } else if (propertyID == 11) {
                                    return item.value;
                                }
                            }).join(",");
                            console.log("FINAL Tag Values", tagValues);
                            this.propertiesArray[foundIndex].Value = tagValues; // 1 - on, 0 - off , 
                        }
                    }
                    else {
                        this.propertiesArray[foundIndex].Value = responses[key].userAnswer;
                    }
                }
            }
            this.saveIdeaDetails.Properties = this.propertiesArray;
            if (clicked == 'save') {
                this.saveIdeaDetails.DoSubmit = false; //save as draft
                this.saveIdeaDetails.ContentId = this.state.contentId;
            } else {
                this.saveIdeaDetails.DoSubmit = true; //submit
                this.saveIdeaDetails.ContentId = this.state.contentId; // it could be saved and then submitted immediately
            }
            //console.log("SAVE IDEA DETAILS WITH VALUES ********", this.saveIdeaDetails, clicked);
            if (this.attachmentFileList.length > 0) {
                this.toggle();
            }
            this.props.saveIdeaInfo({
                data: this.saveIdeaDetails
            })
                .then(() => {
                    if (this.props.success) {
                        console.log("in saveIdeaInfo props success", this.props.contentId);
                        this.setState({ contentId: this.props.contentId });
                        this.addAllAttachments(clicked);
                        console.log("attachment status", this.state.attachmentStatus);
                        this.props.updateAPIStatusBody({
                            data: { loading: false, success: false }
                        });
                        if (clicked == 'save') {
                            this.refs.toast.show('The Challenge has been saved successfully.', 500, () => {
                                this.initiateHomeAction();
                                this.setState({
                                    formDataModified: false,
                                })
                            });
                        }
                        else {
                            this.refs.toast.show('The Challenge has been submitted successfully.', 500, () => {
                                this.initiateHomeAction();
                                if (this.state.attachmentStatus == true) {
                                    this.props.navigation.navigate("HomeScreen");
                                }
                            });
                        }
                    }
                    if (this.props.error) {
                        console.log(
                            "in saveIdeaInfo props error",
                            this.props.errorMsg
                        );
                        if (this.props.errorMsg.response.status == 412) {
                            alert(this.props.errorMsg.response.data.message);
                        } else {
                            //alert("Some error has occured. Please try again");
                            handleError(error, false);
                        }
                        this.props.updateAPIStatusBody({
                            data: { loading: false, error: false, errorMsg: "" }
                        });
                    }
                });
        }
    }

    addAllAttachments(clicked) {
        //attachments
        if (this.attachmentFileList.length > 0) {
            let fileObject = [];
            let fileObjectPromise = [];
            this.attachmentFileList.map((item, idx) => {
                if (item.uploaded == false) {
                    let index = fileObject.push(
                        {
                            contentFileName: item.fileName,
                            contentFileBytes: item.fileByteString,
                            fileType: item.fileType,
                            key: item.key,
                            contentId: this.state.contentId
                        }) - 1;
                    fileObjectPromise.push(this.fileAttachAPI(fileObject[index]));
                }
            });

            if (fileObjectPromise.length > 0) {
                Promise.all(fileObjectPromise).then((responsesArray) => {
                    console.log("Promise returned values", responsesArray);

                    const checkResult = responsesArray.includes(false);
                    if (checkResult == true) {
                        this.setState({ attachmentStatus: false });
                    }
                    else {
                        this.setState({ attachmentStatus: true });
                    }
                });
            }
        }
    }

    fileAttachAPI(fileObject) {
        this.attachmentFileList[fileObject.key].progressBarShow = true;
        this.setState({ refresh: true });

        let _this = this;
        let key = fileObject.key;

        const url = CLIENT_UAT1_URL + `/Challenge/SaveAttachment`;
        let data = {
            ContentFileName: fileObject.contentFileName,
            ContentId: fileObject.contentId,
            ContentFileId: 0,
            ContentFileBytes: fileObject.contentFileBytes,
            FileType: fileObject.fileType,
            FormFieldId: 25
        }
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': this.props.token,
            'api_key': 'LgOiuSu64U',
        }

        return axios
            .post(url, data, {
                headers: headers, onUploadProgress(progressEvent) {
                    var percentCompleted = (Math.round((progressEvent.loaded * 100) / progressEvent.total)) / 100;
                    _this.setState({ progressIncrement: percentCompleted });
                    _this.attachmentFileList[key].progressIncrement = percentCompleted;
                }
            })
            .then(function (response) {
                console.log("In fileAttachAPI", response, key);
                _this.attachmentFileList[key].fileId = response.data.ContentFileId;
                _this.attachmentFileList[key].progressBarShow = false;
                _this.attachmentFileList[key].success = true;
                _this.attachmentFileList[key].uploaded = true;
                _this.setState({ refresh: true });
                return Promise.resolve(true);
            })
            .catch(function (error) {
                console.log("In fileAttachAPI", error, key);
                _this.attachmentFileList[key].progressBarShow = false;
                _this.attachmentFileList[key].success = false;
                _this.attachmentFileList[key].uploaded = false;
                _this.setState({ refresh: true });
                return Promise.resolve(false);
            });
    }//

    checkOwnKeys(keys, object) {
        let perPageRequiredStatus = {};
        for (var key in keys) {
            let keyNew = key;
            let keyWithoutSpace = keyNew.replace(/\s/g, "");
            perPageRequiredStatus[keyWithoutSpace] = keys[key].every((item) => {
                if (object.hasOwnProperty(item)) {
                    if (object[item].userAnswer == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            });
        }
        return perPageRequiredStatus;
    }

    checkSaveEnabeKeys(key, object) {
        if (object.hasOwnProperty(key)) {
            if (object[key].userAnswer == "") {
                return false;
            }
            else {
                return true;
            }
        }
    }

    getAllResponses() {
        let responseArray = [];
        let responseArray1 = []

        this.formArray.map((form, i) => {
            const response = form._getFormResponses();
            responseArray.push(response);
        });
        responseArray1 = responseArray.reduce(function (result, currentObject) {
            for (var key in currentObject) {
                if (currentObject.hasOwnProperty(key)) {
                    result[key] = currentObject[key];
                }
            }
            return result;
        }, {});
        return responseArray1;
    }

    toggleSwitch(data, idx) {
        if (data.applicable_for_patent_application0.userAnswer.regular.length == 0) {
            this.formArray[idx].props.form.map((element, index) => {
                this.setState({ elementDisableForText: true });
            });
        }
        else if (data.applicable_for_patent_application0.userAnswer.regular.length > 0) {
            this.formArray[idx].props.form.map((element, index) => {
                this.setState({ elementDisableForText: false });
            })
        }
    }

    getSubCatergoryAPI(data, idx) {
        let info = {
            selectedValue: data.challenge_category0.userAnswer[0],
        }
        this.props
            .getSubCategoryValues({
                data: info,
            })
            .then(() => {
                if (this.props.success) {
                    console.log("in getSubCategory props success");
                    this.props.updateAPIStatusBody({
                        data: { loading: false, success: false }
                    });
                }
                if (this.props.error) {
                    console.log(
                        "in getSubCategory props error",
                        this.props.errorMsg
                    );
                    this.props.updateAPIStatusBody({
                        data: { loading: false, error: false, errorMsg: "" }
                    });
                }
            });
    }

    toggleCheckBox(data, idx) {
        if (data.CostSavingsApplicable0) {
            if (data.CostSavingsApplicable0.userAnswer.regular.length == 0) {
                this.setState({ elementDisableForCostDetails: true }); //true

            } else if (data.CostSavingsApplicable0.userAnswer.regular.length > 0) {
                this.setState({ elementDisableForCostDetails: false }); //false
            }
        }
        if (data.ImplementationCostApplicable0) {
            if (data.ImplementationCostApplicable0.userAnswer.regular.length == 0) {
                this.setState({ elementDisableForImplement: true });
            } else if (data.ImplementationCostApplicable0.userAnswer.regular.length > 0) {
                this.setState({ elementDisableForImplement: false });
            }
        }
    }

    formDataChanges(data, idx) {
        const { dispatch } = this.props;
        //console.log(" formDataChanges", data);
        this.setState({ formDataModified: true });
        if (data.applicable_for_patent_application0) {
            this.toggleSwitch(data, idx);
        }
        if (data.challenge_type0) {
            this.setState({ challengeType: data.challenge_type0.userAnswer[0] });

            if (this.requiredFieldsArray['Challenge Type'].indexOf('challenge_team1') != -1 && data.challenge_type0.userAnswer[0] != 2) {
                _.pull(this.requiredFieldsArray['Challenge Type'], 'challenge_team1');
            } else if (this.requiredFieldsArray['Challenge Type'].indexOf('challenge_team1') == -1 && data.challenge_type0.userAnswer[0] == 2) {
                this.requiredFieldsArray['Challenge Type'].push('challenge_team1');
            }

            if (this.requiredFieldsArray['Challenge Type'].indexOf('challenge_department1') != -1 && data.challenge_type0.userAnswer[0] != 3) {
                _.pull(this.requiredFieldsArray['Challenge Type'], 'challenge_department1');
            } else if (this.requiredFieldsArray['Challenge Type'].indexOf('challenge_department1') == -1 && data.challenge_type0.userAnswer[0] == 3) {
                this.requiredFieldsArray['Challenge Type'].push('challenge_department1');
            }
        }

        //make API call to get sub-category challenge_category0
        if (data.challenge_category0) {
            if (data.challenge_category0.userAnswer[0] != this.state.selectedCategory) {
                this.setState({ selectedCategory: data.challenge_category0.userAnswer[0] });
                this.getSubCatergoryAPI(data, idx);
            }
        }

        if (data.CostSavingsApplicable0 || data.ImplementationCostApplicable0) {
            this.toggleCheckBox(data, idx);
        }
        let responses = this.getAllResponses();

        //check if Save can be enabled
        let checkSaveResult = this.checkSaveEnabeKeys('challenge_title0', responses);
        if (checkSaveResult == true) {
            //update state to enable the save button 
            this.setState({ disableSaveButton: false });
        }
        else {
            //update state to enable the save button 
            this.setState({ disableSaveButton: true });
        }

        //check if submit can be enabled
        let checkResultMap = this.checkOwnKeys(this.requiredFieldsArray, responses);
        this.setState({ perPageRequiredStatus: checkResultMap });
        const checkResult = Object.values(checkResultMap).includes(false);

        if (checkResult == true) {
            //update state to enable the save button 
            this.setState({ disableSubmitButton: true });
        }
        else {
            //update state to enable the save button 
            this.setState({ disableSubmitButton: false });
        }
    }

    showChallengeTitle(context, challengeTitle, idx) {
        if (context == 'challenge' && idx == 0) {
            return (
                <View>
                    <Text style={styles.label}>
                        Submit Idea for Challenge:
            </Text>
                    <Text style={styles.challengeTitleStyle}>
                        {challengeTitle}
                    </Text>
                </View>
            );
        }
    }

    render() {
        const challengeCategory = this.props.navigation.getParam('challengeCategory', {});
        const challengeSubCategory = this.props.navigation.getParam('challengeSubCategory', {});
        const challengeLocation = this.props.navigation.getParam('challengeLocation', {});
        const context = this.props.navigation.getParam('context', 'NO-CONTEXT');
        const challengeTitle = this.props.navigation.getParam('challengeTitle', 'NO-CONTEXT');

        if (this.props.createFormJson) {
            const addideaComponent = this.props.createFormJson.SubForms.map((form1, idx) => {
                const fieldsArray = [];
                let dropDownList = [];

                let switchOptions = [
                    {
                        label: '',
                        value: ''
                    }
                ];
                const formTheme = {
                    label: {
                        fontWeight: '900'
                    },
                }
                const mytheme = buildTheme({}, {}, formTheme);
                //create the fields array
                if (form1.Properties != null) {

                    let tempStoreProperties = [...form1.Properties];
                    //filter out 
                    if (form1.Name == 'Challenge Type') {

                        let fiteredProperties = tempStoreProperties.filter((item) => {
                            if (item.Name == 'Invited Members' || item.Name == 'Department Multiselect') {
                                if (this.state.challengeType == 0 || this.state.challengeType == 1) {
                                    return;
                                } else if (this.state.challengeType == 2 && item.Name == 'Invited Members') {
                                    return item;
                                } else if (this.state.challengeType == 3 && item.Name == 'Department Multiselect') {
                                    return item;
                                }
                            } else {
                                return item;
                            }
                        })
                        tempStoreProperties = fiteredProperties;
                    }

                    tempStoreProperties.map((element, index) => {
                        if (element.FormPropertyType != null) {
                            if (element.FormPropertyType.Type != 'FileUpload') { //!FileUpload
                                // if element is a dropdown 
                                dropDownList = [];
                                if (ELEMENT[element.FormPropertyType.Type] == 'select') {
                                    dropDownList = [{
                                        label: "Please select",
                                        value: "Please select",
                                        disabled: true
                                    }];
                                    element.FormPropertyListOptions.map((item, i) => {
                                        dropDownList.push(
                                            {
                                                label: item.Value,
                                                value: item.Id
                                                //value: item.Value
                                            });
                                    });
                                }
                                let subCategoryValue = [...this.props.subCategoryData];

                                fieldsArray.push({
                                    key: element.FormFieldName + index,
                                    actualName: element.FormFieldName,
                                    type: ELEMENT[element.FormPropertyType.Type],
                                    required: element.Required,
                                    label: element.Label,
                                    searchInputPlaceholder: 'Select' + ' ' + element.Label,
                                    values: element.FormPropertyType.Type == 'Switch' ? switchOptions : dropDownList.length == 1 && ELEMENT[element.FormPropertyType.Type] != 'tags' ? subCategoryValue : dropDownList,//element.FormPropertyListOptions,
                                    toggle: element.FormPropertyType.Type == 'Switch' ? true : false, // renders a toggle instead of checkbox
                                    style: { fontWeight: 'bold' },
                                    data: element.FormFieldName == 'challenge_tags' ? this.state.tagsSuggestions : this.state.peopleTagsList,
                                    filterForTags: element.FormFieldName == 'challenge_tags' ? this.customFilterDataTags : this.customFilterDataTeamMembers,
                                    disabled: element.DisableBeforeSubmit ? this.state.elementDisableForCostDetails : element.IsDisabled ? this.state.elementDisableForText : false,
                                    textInputProps: { editable: false },
                                    multiple: element.FormPropertyType.Type == 'SourceBoundMultiselectDropdown' ? true : false,
                                    directTextEdit: element.FormPropertyType.Type == 'Number' ? true : true
                                });
                                if (element.Required == true) {
                                    this.requiredFieldsArray[form1.Name] = this.requiredFieldsArray[form1.Name] ? this.requiredFieldsArray[form1.Name] : [];
                                    if (this.requiredFieldsArray[form1.Name].indexOf(element.FormFieldName + index) === -1) {
                                        this.requiredFieldsArray[form1.Name].push(element.FormFieldName + index);
                                    } else {
                                        if (form1.Name == 'Challenge Type') {
                                            if (this.requiredFieldsArray[form1.Name].indexOf('challenge_team1') != -1 && this.state.challengeType != 2) {
                                                _.pull(this.requiredFieldsArray[form1.Name], 'challenge_team1');
                                            } else if (this.requiredFieldsArray[form1.Name].indexOf('challenge_department1') != -1 && this.state.challengeType != 3) {
                                                _.pull(this.requiredFieldsArray[form1.Name], 'challenge_department1');
                                            }
                                        }
                                    }
                                }
                                if (this.propertiesArray.findIndex(item => item.FormFieldName === element.FormFieldName) === -1) {
                                    let propertyValues = { ...element };
                                    propertyValues.Value = null;
                                    this.propertiesArray.push(propertyValues);
                                }
                            }//!FileUpload
                        }//null check for formpropertytype
                    });
                }
                else {
                    //subforms
                    form1.SubForms.map((subform, i) => {
                        let subFormKey = "";
                        fieldsArray.push({
                            key: i.toString(),
                            type: 'paragraph',
                            subtype: 'h1',
                            label: subform.Name,
                            style: { fontSize: 20, fontWeight: '800' }
                        });
                        subform.Properties.map((element, index) => {
                            // if element is a dropdown 
                            if (element.FormPropertyType.Type == 'SourceDropdown' || element.FormPropertyType.Type == 'Dropdown' || element.FormPropertyType.Type == 'RadioButton') {
                                dropDownList = [];
                                element.FormPropertyListOptions.map((item, i) => {
                                    dropDownList.push(
                                        {
                                            label: item.Value,
                                            value: item.Value
                                        });
                                });
                            }
                            fieldsArray.push({
                                key: element.FormFieldName + index,
                                actualName: element.FormFieldName,
                                type: ELEMENT[element.FormPropertyType.Type],
                                required: element.Required,
                                label: element.Label,
                                values: element.FormPropertyType.Type == 'Switch' || element.FormPropertyType.Type == 'CheckBox' ? switchOptions : dropDownList,//element.FormPropertyListOptions,
                                searchInputPlaceholder: 'Select' + ' ' + element.Label,
                                toggle: element.FormPropertyType.Type == 'Switch' ? true : false, // renders a toggle instead of checkbox
                                style: { fontWeight: 'bold' },
                                directTextEdit: element.FormPropertyType.Type == 'Number' && element.DisableBeforeSubmit && i == 0 ? !this.state.elementDisableForCostDetails : !this.state.elementDisableForImplement,
                                disabled: element.DisableBeforeSubmit && i == 0 ? this.state.elementDisableForCostDetails : element.FormFieldName == 'CostSavingsApplicable' ? false : element.FormFieldName == 'ImplementationCostApplicable' ? false : this.state.elementDisableForImplement
                            });
                            if (element.Required == true) {
                                this.requiredFieldsArray[form1.Name] = this.requiredFieldsArray[form1.Name] ? this.requiredFieldsArray[form1.Name] : [];

                                if (this.requiredFieldsArray[form1.Name].indexOf(element.FormFieldName + index) === -1) {
                                    this.requiredFieldsArray[form1.Name].push(element.FormFieldName + index);
                                }
                            }
                            if (this.propertiesArray.findIndex(item => item.FormFieldName === element.FormFieldName) === -1) {
                                let propertyValues = { ...element };
                                propertyValues.Value = null;
                                this.propertiesArray.push(propertyValues);
                            }

                            //push subform properties
                            if (index == 0) {
                                subFormKey = element.FormFieldName;
                                this.subFormsPropertiesArray[subFormKey] = [];
                            } else {
                                this.subFormsPropertiesArray[subFormKey].push(element.FormFieldName);
                            }
                        });
                    });
                }
                //create the form
                if (fieldsArray.length > 0) {
                    return (
                        <View
                            key={idx}
                            style={{
                                padding: 10
                            }}
                        >
                            <Header
                                containerStyle={styles.headerstyle}
                                leftComponent={<Icon name='left' type='antdesign' color='blue' onPress={() => {
                                    if (this.state.vpPagePosition == 0) {
                                        if (this.state.formDataModified) {
                                            let buttonsArray = [
                                                {
                                                    text: 'OK',
                                                    onPress: () => this.props.navigation.navigate('HomeScreen'),
                                                    style: 'cancel',
                                                },
                                            ];
                                            if (!this.state.disableSaveButton) {
                                                buttonsArray.push({ text: 'Cancel', onPress: () => this.validateFormData('save') });
                                            }
                                            Alert.alert(
                                                '',
                                                'Do you really want to close the window?. You will be losing filled data.',
                                                buttonsArray,
                                                { cancelable: false },
                                            );
                                        } else {
                                            this.props.navigation.navigate('HomeScreen')
                                        }
                                    } else {
                                        this.viewPager.setPage(this.state.vpPagePosition - 1);
                                    }
                                }} />}
                                centerComponent={{ text: form1.Name, style: { color: 'blue', fontSize: 22 } }}
                                rightComponent={this.state.vpPagePosition != (this.pageCountAdjust ? this.state.createFormData.SubForms.length - 2 : this.state.createFormData.SubForms.length - 1) && <TouchableOpacity onPress={() => {
                                    console.log("next button", this.state.createFormData.SubForms.length);
                                    this.viewPager.setPage(this.state.vpPagePosition + 1)
                                }} ><Text style={{ fontSize: 20, color: 'blue', marginRight: Platform.OS === 'android' ? 5 : 5 }}>Next</Text></TouchableOpacity>}
                            />
                            <OfflineNotice />
                            {this.showChallengeTitle(context, challengeTitle, idx)}
                            <DynamicForm
                                ref={(ref) => {
                                    if (this.formArray.indexOf(ref) === -1 && ref != null) {
                                        this.formArray.push(ref);
                                    }
                                }}
                                form={fieldsArray}
                                theme={mytheme}
                                style={styles.backgroundStyle}
                                onFormDataChange={(data) => this.formDataChanges(data, idx)}
                            />
                        </View>
                    );
                } else { return null; }
            });


            return (
                <View pointerEvents={this.props.loading ? "none" : "auto"} style={{ flex: 1 }}>
                    <IndicatorViewPager
                        style={{ height: this.state.footerVisible ? deviceHeight : deviceHeight - 80, backgroundColor: 'white' }}
                        indicator={this._renderDotIndicator()}
                        onPageSelected={(e) => {
                            console.log("onpageselected", e, e.position);
                            this.setState({ vpPagePosition: e.position });
                        }}
                        ref={viewPager => { this.viewPager = viewPager }}
                    >
                        {addideaComponent}

                    </IndicatorViewPager>

                    {this.renderSpinner()}
                    <Toast ref="toast" position='top' />
                    {this.renderAttachmentButton()}
                    {!this.state.footerVisible && this.renderSaveButton()}
                </View>

            );
        } else {
            return (<View>Internal Error(GetSubmitForm API)</View>);
        }
    }

    _renderDotIndicator() {
        let result = this.state.createFormData.SubForms.filter((item) => { return item.Name === 'Attachments' });
        if (result.length > 0) {
            this.pageCountAdjust = true;
        }
        console.log("renderDotIndicator", this.state.createFormData.SubForms.length, result);
        let dotstyleListObjects = [
            { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.Overview ? 'red' : 'lightblue', bottom: -5 },
            { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.Details ? 'red' : 'lightblue', bottom: -5 },
            { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.Duration ? 'red' : 'lightblue', bottom: -5 },
            { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.ChallengeType ? 'red' : 'lightblue', bottom: -5 },
            { borderRadius: 1, height: 25, backgroundColor: !this.state.perPageRequiredStatus.SeekingGain ? 'red' : 'lightblue', bottom: -5 },
            { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 },
            { borderRadius: 1, height: 25, backgroundColor: 'lightblue', bottom: -5 }
        ];

        return (
            <PagerDotIndicator
                style={{
                    top: '-65%',
                    width: 30,
                    transform: [{ rotate: '90deg' }]
                }}
                pageCount={this.pageCountAdjust ? this.state.createFormData.SubForms.length - 1 : this.state.createFormData.SubForms.length} //6
                dotStyle={dotstyleListObjects}
                selectedDotStyle={{ backgroundColor: 'darkblue', borderRadius: 1, height: 45, }} />

        );
    }
}

function mapStateToProps(state) {
    console.log("in mapStateToProps");
    return {
        savedAddIdeaData: state.DataReducer.savedAddIdeaDataFlag,
        loading: state.DataReducer.generalAPIStatusBody.loading,
        success: state.DataReducer.generalAPIStatusBody.success,
        error: state.DataReducer.generalAPIStatusBody.error,
        errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
        subCategoryData: state.DataReducer.subCategoryData,
        createFormJson: state.DataReducer.createFormDataForChallenges,
        token: state.verifyReducer.token,
        contentId: state.DataReducer.saveIdeaData.contentID,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ReduxActions, getHomePageContent, homeSearch, getIdeas, getChallenges }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewPagerAddChallenge);

var styles = StyleSheet.create({
    headerstyle: {
        backgroundColor: 'white',
        marginTop: Platform.OS === 'ios' ? 0 : - 30
    },
    saveButtonText: {
        fontSize: fonts.md,
        fontWeight: "600",
        color: "white"
    },
    removeAttachmentStyle: {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 22,
        marginRight: 10,
        marginLeft: 10
    },
    refreshAttachmentStyle: {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 18,
        marginRight: 10,
        marginLeft: 10
    },
    fileUploadFailedTextStyle: {
        alignItems: 'center',
        fontSize: 14,
        color: 'red'
    },
    progressBarView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 30
    },
    fLFileName: {
        flexGrow: 1,
        width: 0,
        flexDirection: "column",
        justifyContent: "center",
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 16
    },
    fLContainerView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 30,
        borderRadius: 20,
        borderColor: 'black',
        borderWidth: 0.3,
        marginTop: 5
    },
    flatListViewStyle: {
        bottom: 90,
        height: 90
    },
    attachmentHeaderStyle: {
        color: 'white',
        fontSize: 24,
        marginTop: 5,
        marginLeft: 25
    },
    plusIconStyle: {
        fontSize: 25,
        color: '#0086b3'
    },
    fileIconStyle: {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 18,
        margin: 5
    },
    containerforform: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    formContainer: {
        marginTop: 100,
    },
    backgroundStyle: {
        backgroundColor: 'white',
        marginLeft: 15
    },
    footer: {
        position: 'absolute',
        paddingTop: 15,
        flex: 0.1,
        left: 0,
        right: 0,
        bottom: deviceHeight / 1000,
        flexDirection: 'row',
        height: 90,
        backgroundColor: 'white',
        borderWidth: 0.2,
    },
    getStartedButton: {
        position: "absolute",
        bottom: 0,//15,
        left: 0,
        borderRadius: 3,
        height: 80,//55,
        width: deviceWidth,
        justifyContent: "center",
        flex: 1,
        flexDirection: 'row',
        zIndex: 100,
    },
    bottomButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'green'
    },
    activityindicator: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0.3 * deviceHeight,
        bottom: 0,
        alignItems: "center",
        justifyContent: 'center',
    },
    floatingButton: {
        position: 'absolute',
        flex: 1,
        left: '86%',
        right: 10,
        bottom: Platform.OS === 'ios' ? deviceHeight / 8 : deviceHeight / 8 + 24,
        flexDirection: 'row',
        borderWidth: 3,
        borderColor: '#0086b3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        backgroundColor: 'white',
        borderRadius: 100,
        elevation: 4,
        shadowOffset: { width: 5, height: 5 },
        shadowColor: "black",
        shadowOpacity: 0.5,
        shadowRadius: 10,
    },
    attachmentRibbon: {
        position: 'absolute',
        flex: 0.1,
        left: 0,
        right: 0,
        bottom: 90,
        backgroundColor: 'black',
        flexDirection: 'row',
        height: 40,
    },
    bottomButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    footerText: {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 24,
    },
    footerText2: {
        color: '#0086b3',
        alignItems: 'center',
        fontSize: 13,
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 10
    },
    card: {
        padding: 10,
        width: 40,
        borderRadius: 4,
    },
    label: {
        marginTop: 20,
        marginLeft: 30,
        fontSize: 14,
        fontWeight: '800',
        color: '#2A3C53',
    },
    challengeTitleStyle: {
        marginTop: 5,
        marginLeft: 30,
        fontSize: 14,
        color: '#2A3C53',
    },
})
