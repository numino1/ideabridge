import React, { Component } from 'react';
import { Platform, StyleSheet, Text, Button, View, TouchableOpacity, Alert } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { ScrollView } from 'react-native-gesture-handler';
import { Avatar, Divider } from 'react-native-elements';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { logout } from '../actions/verify';
import Box from '../components/Boxes';
import Tags from "react-native-tags";
import AsyncStorage from '@react-native-community/async-storage';
import { fonts, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';


class Profile extends Component {
  static navigationOptions = {
    headerRight: (
      <TouchableOpacity
        style={{}}
        onPress={() => { }}
        disabled
      >
      </TouchableOpacity>
    ),
  };

  render() {
    console.log('userdetails profile===>', this.props.userDetails)
    return (
      <ScrollView style={styles.container}>
        <View style={[styles.container, styles.viewPadding]}>
          <OfflineNotice />
          <View style={[styles.elementsContainer]}>
            <View style={styles.headerViewStyle} >
              <Text style={[systemWeights.bold, { ...header }]}>My Profile</Text>
            </View>
          </View>
          <View style={styles.info}>
            <View style={{ flexDirection: 'column' }}>
              <Text style={styles.fullName}>{this.props.userDetails.FullName}</Text>
              <Text style={styles.designationName}>{this.props.userDetails.DesignationName}</Text>
            </View>
            <Avatar rounded
              source={require('../../images/user1.png')}
              size='large'
            />
          </View>

          <View style={styles.delimeter} />
          <Text style={styles.title}>Roles</Text>

          <View style={styles.tagsViewStyle}>
            <Tags
              inputContainerStyle={{ height: 0 }}
              initialTags={this.props.userDetails.UserRoleName}
              deleteTagOnPress={false}
              containerStyle={{ backgroundcolor: 'black' }}
              tagContainerStyle={{ backgroundColor: '#9ACD32', borderWidth: 1 }}
              tagTextStyle={{ color: 'red' }}
              renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                <TouchableOpacity disabled style={styles.tagButtonStyle}>
                  <View style={styles.parentTag}>
                    <Text style={[systemWeights.semibold, styles.childTag]}>{tag}</Text>
                  </View>
                </TouchableOpacity>
              )}

            />
          </View>

          <View style={styles.delimeter} />

          <Text style={styles.title}>My Score</Text>
          <View style={styles.pointsViewStyle}>
            <TouchableOpacity style={[styles.points]} onPress={() => { }} disabled >
              <View style={styles.pointsButtonViewstyle} >
                <Text style={[systemWeights.bold, styles.value]}>{this.props.userDetails.UserScore}</Text>
                <Text style={[systemWeights.semibold, styles.key]}>Points</Text>
              </View>
            </TouchableOpacity>
            <View style={styles.viewwidth}>
              <View style={styles.viewOneStyle} />
              <View style={styles.viewtwoStyle} />
            </View>

            <TouchableOpacity style={[styles.inr]} onPress={() => { }} disabled>
              <View style={styles.pointsButtonViewstyle} >
                <Text style={[systemWeights.bold, styles.value]}>{this.props.userDetails.RemainingCash}</Text>
                <Text style={[systemWeights.semibold, styles.key]}>INR</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.delimeter} />
          <View style={[styles.parent]}>
          </View>

          <TouchableOpacity style={[styles.logoutButton]} onPress={() => {

            Alert.alert(
              'Logout',
              'Are you sure you want to logout ?',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'OK', onPress: () => {
                    console.log('ok Pressed')
                    this.props.logout();
                    AsyncStorage.removeItem('@VerifiedCode');
                    AsyncStorage.removeItem("@EmailId");
                    this.props.navigation.navigate('LoginScreen')
                  }
                },
              ],
              { cancelable: false },
            );

          }} >
            <View >
              <Text style={[systemWeights.bold, styles.logoutButtonStyles]}>Logout</Text>
            </View>
          </TouchableOpacity>
        </View>

      </ScrollView>
    );
  }
}

function mapStateToProps({ verifyReducer }) {
  return {
    code: verifyReducer.code,
    userDetails: verifyReducer.userDetails
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ logout }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    paddingBottom: 40
  },
  viewwidth: {
    width: 1
  },
  pointsButtonViewstyle: {
    alignItems: 'center'
  },
  viewPadding: {
    padding: 10
  },
  logoutButtonStyles: {
    fontSize: fonts.md,
    color: '#ffffff'
  },
  viewOneStyle: {
    height: 30,
    backgroundColor: '#87CEFA',
  },
  viewtwoStyle: {
    height: 30,
    backgroundColor: '#87CEFA',
    top: 90
  },
  pointsViewStyle: {
    flexDirection: 'row',
    marginTop: 20,
    marginRight: '3%',
    marginLeft: '3%'
  },
  tagButtonStyle: {
    marginLeft: 4,
    marginRight: 4,
    marginBottom: 8
  },
  tagsViewStyle: {
    backgroundColor: 'white',
    paddingTop: 20
  },
  headerViewStyle: {
    flex: 6
  },
  elementsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20
  },
  info: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  fullName: {
    fontSize: fonts.md + 2,
    fontWeight: 'bold',
    color: 'black'
  },
  designationName: {
    fontSize: fonts.md - 3,
    color: 'black'
  },
  childRoles: {
    flex: 1,
    height: 44,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center'
  },
  myscore: {
    height: 124,
    margin: '3%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  parent: {
    top: 10,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  logoutButton: {
    backgroundColor: '#FA8072',
    top: 15,
    height: 70,
    width: '94%',
    marginLeft: 10,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  parentTag: {
    flexDirection: 'row',
    borderRadius: 23,
    backgroundColor: '#9ACD32',
    height: 34,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 16,
    paddingRight: 16
  },
  childTag: {
    fontSize: fonts.sm + 1,
    color: '#ffffff',
    alignItems: 'center'
  },
  key: {
    fontSize: fonts.md - 4,
    color: '#ffffff'
  },
  value: {
    fontSize: fonts.xLg,
    color: '#ffffff'
  },
  points: {
    backgroundColor: '#87CEFA',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    flex: 1,
    height: 150,
    justifyContent: 'center'
  },
  inr: {
    backgroundColor: '#87CEFA',
    borderTopRightRadius: 15,
    flex: 1,
    height: 150,
    borderBottomRightRadius: 15,
    justifyContent: 'center',
    marginBottom: 20
  },
  delimeter: {
    backgroundColor: '#DCDCDC',
    marginTop: 20,
    flex: 0.3,
    height: 1
  },
  title: {
    fontSize: fonts.md - 3,
    color: 'grey',
    marginTop: 20
  }
});