
import React, { Component } from 'react';
import { Platform, DeviceInfo, StyleSheet, Text, View, TouchableOpacity, Button, Dimensions, TextInput, Alert, Keyboard } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { withNavigation } from 'react-navigation';
import ColorPalette from 'react-native-color-palette';
import { Header, Icon } from 'react-native-elements';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';
import Toast, { DURATION } from 'react-native-easy-toast';
import { stat } from 'react-native-fs'

export const getOrientation = () => {
  const { width, height } = Dimensions.get('window');
  return width > height ? 'landscape' : 'portrait';
};
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
let deviceHeaderHeight = Platform.OS === "ios" ? 64 : 56;
const orientation = getOrientation();
deviceHeaderHeight += DeviceInfo.isIPhoneX_deprecated && orientation === 'portrait' ? 24 : 0;

let commonColorList = ['#C0392B', '#E74C3C', '#9B59B6', '#8E44AD', '#2980B9', '#AED6F1', '#52BE80', '#117A65', '#F7DC6F', '#D7DBDD', '#B2BABB', '#212F3D', '#85929E', '#E59866'];
let pencilColorList = ['#D7DBDD', '#B2BABB', '#85929E', '#212F3D'];


class Doodle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      strokeColor: 'red',
      strokeWidth: 7,
      renderView: false,
      pencilSelected: false,
      eraserSelected: false,
      showTextInput: false,
      hideTextBox: false,
      pagePosition: this.props.navigation.getParam('position', 0),
      inputText: '',
      xPosition: 0,
      yPosition: 0,
      displayTextArray: [],
      canvas: '',
      size: 0,
      isDisable: false
    };
  }

  static navigationOptions = ({ navigation }) => ({
    header: null
  });


  showColorPalette() {
    console.log("in show color palette");
    if (this.state.renderView && !this.state.eraserSelected && !this.state.showTextInput) {
      return (
        <View style={{ flexDirection: 'row', border: 'black', borderWidth: 1, backgroundColor: 'white', height: 150, justifyContent: 'center', alignItems: 'flex-start' }}>
          <ColorPalette
            paletteStyles={{ flexDirection: 'row', backgroundColor: 'white' }}
            onChange={color => { this.setState({ strokeColor: color, renderView: false }) }}
            value={this.state.strokeColor}
            colors={this.state.pencilSelected ? pencilColorList : commonColorList}
            title={"Select color:"}
            icon={
              <AntDesign name='checkcircleo' style={{ fontSize: 28, color: 'black' }} />
            }
          />
        </View>
      );
    }
  }

  createdisplayTextArray() {
    var text = this.state.inputText;
    if (text != '') {
      var stateTextArray = this.state.displayTextArray;
      var xPosition = this.state.xPosition;
      var yPosition = this.state.yPosition;
      stateTextArray.push({
        text: text,
        overlay: 'TextOnSketch',
        position: { x: xPosition, y: yPosition },
        fontSize: Platform.select({ ios: 24, android: 48 })
      });
      this.setState({ displayTextArray: stateTextArray });
    }
  }

  showTextInput() {
    console.log("in show TEXT INPUT");
    if (this.state.showTextInput) {
      return (
        <View
          onTouchStart={(e) => {
            this.setState({ xPosition: e.nativeEvent.locationX, yPosition: e.nativeEvent.locationY });
          }}
          style={{ backgroundColor: 'transparent', height: deviceHeight - deviceHeaderHeight - 100, width: deviceWidth }}>
          <View style={{ position: 'absolute', top: this.state.yPosition, left: this.state.xPosition, borderColor: 'black', borderWidth: 1, backgroundColor: 'transparent', width: 200, height: 50, fontSize: 24, color: 'black' }}>
            <TextInput
              autoFocus={true}
              multiline={true}
              onChangeText={(text) => this.setState({ inputText: text })}
              value={this.state.inputText}
              placeholder={'Enter text here...'}
              returnKeyType="done"
              onKeyPress={(e) => {
                if (e.nativeEvent.key == "Enter") {
                  this.createdisplayTextArray();
                  Keyboard.dismiss();
                  this.setState({ hideTextBox: true, inputText: '', xPosition: 0, yPosition: 0, showTextInput: false });
                }
              }}
              style={{ flexDirection: 'row', borderColor: 'black', borderWidth: 1, borderRadius: 1, borderStyle: 'dashed', backgroundColor: 'transparent', width: 200, height: 50, fontSize: 24, color: 'black' }}
            />
          </View>
        </View>
      );
    }
  }


  saveDoodle() {
    this.canvas.save('jpg', true, 'RNSketchCanvas', `${Math.ceil(Math.random() * 100000000)}_doodle`, false, true, false);
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          containerStyle={styles.headerstyle}
          leftComponent={<Icon name='left' type='antdesign' color='blue' onPress={() => {
            Alert.alert(
              '',
              'Do you want to save changes?',
              [
                { text: 'Close', onPress: () => console.log('Ask me later pressed') },
                {
                  text: 'Cancel',
                  onPress: () => this.props.navigation.goBack(),
                  style: 'cancel',
                },
                { text: 'Save', onPress: () => this.saveDoodle() },
              ],
              { cancelable: false },
            );
          }} />}
          rightComponent={<Button title='Done' color='blue' onPress={() => {
            this.saveDoodle();
          }} />}
        />
        <Toast ref="toast" position='top' />
        <View style={styles.sketchCanvasView}>
          <SketchCanvas
            text={
              this.state.displayTextArray
            }
            ref={canvas => { this.state.canvas = canvas; this.canvas = canvas }}
            style={{ flex: 1, height: deviceHeight - 100, backgroundColor: 'white' }}
            strokeColor={this.state.strokeColor}
            strokeWidth={this.state.strokeWidth}
            onSketchSaved={async (success, path) => {
              this.refs.toast.show('The doodle has been saved successfully', 500);
              //console.log("onsketchsaved", success, path, this.props.navigation);
              let statResult = await stat(path);
              let resobj = {
                path: path,
                size: statResult.size,
                mime: 'image/jpeg'
              }
              //console.log('size of the resobject',resobj)
              const { navigation } = this.props;
              this.props.navigation.goBack();
              navigation.state.params.onAttachment({ attachDetails: resobj });
              if (this.state.pagePosition == 0) {
                this.props.navigation.navigate('ViewPager', { uri1: 'file://' + path, uri2: 'NO-URI', uri3: 'NO-URI' });
              }
              else if (this.state.pagePosition == 1) {
                this.props.navigation.navigate('ViewPager', { uri1: 'NO-URI', uri2: 'file://' + path, uri3: 'NO-URI' });
              }
              else if (this.state.pagePosition == 2) {
                this.props.navigation.navigate('ViewPager', { uri1: 'NO-URI', uri2: 'NO-URI', uri3: 'file://' + path });
              }
            }}
            onStrokeStart={(x, y) => {
              console.log("onStrokeStart");
              if (this.state.showTextInput) {
                this.setState({ hideTextBox: false });
              }
            }}
          />

        </View>
        <View>
          {this.showColorPalette()}
        </View>
        <View>
          {!this.state.hideTextBox && this.showTextInput()}
        </View>
        <View >
          <View style={styles.buttonPanelView}>

            <TouchableOpacity
              style={[styles.strokeWidthButton, { backgroundColor: this.state.strokeColor }]}
              onPress={() => { this.setState({ renderView: true }) }} >
            </TouchableOpacity>

            <TouchableOpacity style={[styles.functionButton, { backgroundColor: 'white' }]}
              disabled={this.state.showTextInput}
              onPress={() => {
                this.setState({ strokeWidth: 2, strokeColor: 'grey', pencilSelected: true, eraserSelected: false, showTextInput: false })
              }}>
              <MaterialCommunityIcons name='pencil' style={{ fontSize: 30, color: this.state.showTextInput ? 'grey' : 'blue' }} />
              <Text style={styles.labelTextStyle}>Pencil</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.functionButton, { backgroundColor: 'white' }]} disabled={this.state.showTextInput} onPress={() => {
              this.setState({ strokeWidth: 3, strokeColor: 'red', pencilSelected: false, eraserSelected: false, showTextInput: false })
            }}>
              <MaterialCommunityIcons name='pen' style={{ fontSize: 30, color: this.state.showTextInput ? 'grey' : 'blue' }} />
              <Text style={styles.labelTextStyle}>Pen</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.functionButton, { backgroundColor: 'white' }]} disabled={this.state.showTextInput} onPress={() => {
              this.setState({ strokeWidth: 15, strokeColor: 'red', pencilSelected: false, eraserSelected: false, showTextInput: false })
            }}>
              <MaterialCommunityIcons name='marker' style={{ fontSize: 30, color: this.state.showTextInput ? 'grey' : 'blue' }} />
              <Text style={styles.labelTextStyle}>Marker</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.functionButton, { backgroundColor: 'white' }]} onPress={() => {
              this.setState({ strokeColor: 'white', strokeWidth: 10, pencilSelected: false, eraserSelected: false, showTextInput: true, isDisable: true })
            }}>
              <MaterialCommunityIcons name='format-text' style={{ fontSize: 30, color: 'blue' }} />
              <Text style={styles.labelTextStyle}>Text</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.functionButton, { backgroundColor: 'white' }]} disabled={this.state.showTextInput} onPress={() => {
              this.setState({ strokeColor: 'white', strokeWidth: 10, pencilSelected: false, eraserSelected: true, showTextInput: false })
            }}>
              <MaterialCommunityIcons name='pen' style={{ fontSize: 30, color: this.state.showTextInput ? 'grey' : 'blue' }} />
              <Text style={styles.labelTextStyle}>Eraser</Text>
            </TouchableOpacity>

          </View>

        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  console.log("in mapStateToProps");
  return {
    contentId: state.DataReducer.saveIdeaData.contentID,
  };
}

export default connect(mapStateToProps, null)(Doodle);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  labelTextStyle: {
    color: '#6f9fed',
    fontSize: 17
  },
  sketchCanvasView: {
    flex: 1,
    flexDirection: 'row'
  },
  strokeWidthButton: {
    marginHorizontal: 2.5, marginVertical: 8, width: 30, height: 30, borderRadius: 15, borderColor: 'black',
    justifyContent: 'center', alignItems: 'center', backgroundColor: '#39579A', borderWidth: 2,
  },
  functionButton: {
    marginHorizontal: 2.5, marginVertical: 8,
    flex: 1, height: 30, width: 60,
    backgroundColor: '#39579A', justifyContent: 'center', alignItems: 'center', borderRadius: 5,
    aspectRatio: 1
  },
  headerstyle: {
    backgroundColor: 'white',
    marginTop: Platform.OS === 'ios' ? 0 : - 30
  },
  buttonPanelView: {
    flexDirection: 'row',
    borderColor: 'black',
    borderWidth: 1,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  }
});
