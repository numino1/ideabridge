import React, { Component } from 'react';
import { Platform, Switch, ScrollView, StyleSheet, Text, View, Alert, Button, FlatList, TouchableHighlight, TouchableOpacity, Dimensions, ActivityIndicator, TextInput, KeyboardAvoidingView } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar, Slider } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
let _ = require('lodash');
import { fonts, dimensions, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast';
import { Dropdown } from 'react-native-material-dropdown';
import { CLIENT_UAT1_URL } from '../config/configuration';
import axios from "axios";
import DynamicForm, { buildTheme } from 'react-native-dynamic-form';
import { getPendingTasksForLoggedInUser } from '../actions/home';
import DatePicker from 'react-native-datepicker';
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height

class EvaluateApprovals extends Component {
  static navigationOptions = ({ navigate, navigation, props }) => ({
    headerLeft: null,
    header: null
  })

  constructor(props) {
    super(props);
    this.state = {
      simpleEvalMatrixArray: [],
      detailedEvalMatrixArray: [],
      projectName: 'Just Do It',
      sliderValue: 5,
      subCriteria: false,
    };
    const showSimpleEvalMatrix = this.props.navigation.getParam('showSimpleEvalMatrix', 'NO-data');
    if (showSimpleEvalMatrix == false) {
      this.state.projectName = 'Project';
    }
    let simpleEvalData = [];
    let detailEvalData = [];
    this.props.evaluationMatrix.EvaluationMatrices[0].Criterias.map((item, idx) => {
      if (simpleEvalData.findIndex(data => data.name === item.Name) === -1) {
        simpleEvalData.push(
          {
            id: item.Id,
            name: item.Name,
            toggle: true
          }
        );
      }
    });
    this.state.simpleEvalMatrixArray = simpleEvalData;
    this.props.evaluationMatrix.EvaluationMatrices[1].Criterias[0].SubCriterias.map((item, idx) => {
      if (detailEvalData.findIndex(data => data.name === item.Name) === -1) {
        detailEvalData.push(
          {
            id: item.Id,
            name: item.Name,
            ranges: item.Ranges,
            sliderValue: 5
          }
        );
      }
    });
    this.state.detailedEvalMatrixArray = detailEvalData;
  }

  componentDidMount() {
    let data = {
      ContentID: this.props.ideaData.ContentID
    }
    this.props.getContentDataForEvaluate({ data: data })
      .then(() => {
      })
  }

  componentWillUnmount() {
    this.props
      .clearTaskIdValues();
  }

  setProjectName() {
    const showSimpleEvalMatrix = this.props.navigation.getParam('showSimpleEvalMatrix', 'NO-data');
    if (showSimpleEvalMatrix == true) {
      let result = this.state.simpleEvalMatrixArray.some((item) => {
        return item.toggle == false;
      })
      if (result == true) {
        this.setState({ projectName: 'Kaizen/Project' })
      }
      else {
        this.setState({ projectName: 'Just Do It' })
      }
    }
  }

  closeEvaluate() {
    this.props.navigation.navigate("Approvals");
  }

  submitEvaluate() {
    if (this.state.projectName == 'Just Do It') {
      Alert.alert(
        'JDI Confirmation',
        '\nThanks for evaluating the idea.\n\nThis Idea qualiﬁes for JDI.\n\nPlease click on "Submit" to assign a leader or click on "Edit" to continue evaluation.\n',
        [
          {
            text: 'Submit',
            onPress: () => { this.jdiSubmitApi(1) },
            style: 'cancel',
          },
          { text: 'Edit', onPress: () => { return } },
        ],
        { cancelable: false },
      );
    } else if (this.state.projectName == 'Kaizen/Project') {
      Alert.alert(
        'Kaizen/Project Confirmation',
        '\nThanks for evaluating the idea.\n\nThis Idea qualiﬁes for Kaizen/Project.\n\nPlease click on "Submit" to evalute for Kaizen/Project\n\nor click on "Edit" to continue evaluation.\n',
        [
          {
            text: 'Submit',
            onPress: () => { this.jdiSubmitApi(1) },
            style: 'cancel',
          },
          { text: 'Edit', onPress: () => { return } },
        ],
        { cancelable: false },
      );
    } else if (this.state.projectName == 'Kaizen') {
      Alert.alert(
        'Kaizen Confirmation',
        '\nThanks for evaluating the idea. This Idea is selected for Kaizen.\n\nPlease click on "Submit" to assign a leader\n\nor click on "Edit" to continue evaluation.\n',
        [
          {
            text: 'Submit',
            onPress: () => { this.jdiSubmitApi(2) },
            style: 'cancel',
          },
          { text: 'Edit', onPress: () => { return } },
        ],
        { cancelable: false },
      );
    } else if (this.state.projectName == 'Project') {
      Alert.alert(
        'Project Confirmation',
        '\nThanks for evaluating the idea.\n\nThis Idea is selected for Project.\n\nPlease click on "Submit" for evaluation to go to L2 for assigning lead\n\nor click on "Edit" to continue evaluation.\n',
        [
          {
            text: 'Submit',
            onPress: () => { this.jdiSubmitApi(2) },
            style: 'cancel',
          },
          { text: 'Edit', onPress: () => { return } },
        ],
        { cancelable: false },
      );
    }
  }

  jdiSubmitApi(status) {
    if (status == 1) {
      const evalTaskID = this.props.navigation.getParam('taskId', 'NO-data');
      let evaluationData = [];
      this.state.simpleEvalMatrixArray.map((data, i) => {
        evaluationData.push(
          {
            ContentID: this.props.ideaData.ContentID.toString(),
            EvaluationCriteriaID: (i + 1).toString(),
            Response: data.toggle.toString()
          }
        )
      })

      let data = {
        contentID: this.props.ideaData.ContentID.toString(),
        taskID: evalTaskID.toString(),
        evaluationResponse: evaluationData
      }

      this.props.submitContentEvaluateData({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in submitContentEvaluateData props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (this.state.projectName == 'Just Do It') {
              this.refs.toast.show('Evaluated: Idea has been marked as Just do It successfully.', 500, () => {
                this.props.navigation.navigate('InitiateProject', { title: "Initiate JDI from the Idea", label: "Remarks", btnLabel: "Assign JDI Leader", ideaName: this.props.ideaData.Title, projectType: 'JDI' });
              });
            }
            if (this.state.projectName == 'Kaizen/Project') {
              this.setState({ subCriteria: true });
              this.setState({ projectName: 'Project' });
            }
          }

          if (this.props.error) {
            console.log(
              "in submitContentEvaluateData props error",
              this.props.errorMsg
            );
            //alert("Some error has occured. Please try again.");
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }

    if (status == 2) {
      let newTaskId = this.props.navigation.getParam('taskId', 'NO-data');
      let evalTaskID = newTaskId;
      if (this.props.submitEvalTaskID === undefined || this.props.submitEvalTaskID === "") {
        evalTaskID = newTaskId;
      } else {
        evalTaskID = this.props.submitEvalTaskID;
      }
      let evaluationScoresData = [];
      let totalScore = this.getScore();
      let id = 0;
      this.state.detailedEvalMatrixArray.map((data, i) => {
        if (this.props.ideaData.ContentStatus == 'Awaiting Level 2 Approver') {
          id = this.props.ratingData[i].ID;
        }
        evaluationScoresData.push(
          {
            Id: id,
            ContentID: this.props.ideaData.ContentID.toString(),
            EvaluationSubCriteriaID: (i + 1).toString(),
            Points: data.sliderValue.toString()
          }
        )
      })

      let data = {
        contentID: this.props.ideaData.ContentID.toString(),
        taskID: evalTaskID.toString(),
        totalScore: totalScore.toString(),
        scores: evaluationScoresData,
      }

      this.props.evaluateData({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in submitContentEvaluateData props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (this.state.projectName == 'Kaizen') {
              this.props.navigation.navigate('InitiateProject', { title: "Initiate Kaizen from the Idea", label: "Remarks", btnLabel: "Assign Kaizen Leader", ideaName: this.props.ideaData.Title, projectType: 'Kaizen' });
            }
            if (this.state.projectName == 'Project') {
              this.refs.toast.show('Idea Evaluated: Thankyou for evaluating the idea.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
          }

          if (this.props.error) {
            console.log(
              "in EvaluateData props error",
              this.props.errorMsg
            );
            //alert("Some error has occured. Please try again.");
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }
  }

  initiateHomeAction() {
    this.props.getPendingTasksForLoggedInUser();
  }

  renderSpinner() {
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  getcolor(index) {
    let color = '';
    if (this.state.detailedEvalMatrixArray[index].sliderValue == 5) {
      color = '#00A551';
    } else if (this.state.detailedEvalMatrixArray[index].sliderValue == 3) {
      color = '#EEF20A';
    } else if (this.state.detailedEvalMatrixArray[index].sliderValue == 1) {
      color = '#FF0000';
    }
    return color
  }

  getScore() {
    let totalSubCriterias = this.state.detailedEvalMatrixArray.length;
    let totalScore = 0;
    let finalScore = 0;
    this.state.detailedEvalMatrixArray.forEach(data => totalScore = totalScore + data.sliderValue);
    if (totalScore > 0) {
      finalScore = totalScore / totalSubCriterias;
    }
    return finalScore.toFixed(2);
  }

  getSliderLabel(index) {
    let sliderValue = this.state.detailedEvalMatrixArray[index].sliderValue;
    let obj = this.state.detailedEvalMatrixArray[index].ranges.find(o => o.Minimum == sliderValue);
    return obj.Name

  }

  doBackSubcriteriaEvaluate() {
    let newTaskId = this.props.navigation.getParam('taskId', 'NO-data');
    let evalTaskID = newTaskId;
    if (this.props.submitEvalTaskID === undefined || this.props.submitEvalTaskID === "") {
      evalTaskID = newTaskId;
    } else {
      evalTaskID = this.props.submitEvalTaskID;
    }
    let data = {
      contentID: this.props.ideaData.ContentID.toString(),
      taskID: evalTaskID.toString(),
      formfields: '',
      ContentType: 'Idea'
    }

    this.props.undoSubmitContentOnBack({ data: data })
      .then(() => {
        if (this.props.success) {
          console.log("in submitContentEvaluateData props success");
          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
          this.props.navigation.navigate({
            routeName: "EvaluateApprovals",
            params: {
              showSimpleEvalMatrix: true,
              showDetailedEvalMatrix: false,
              contentId: this.props.ideaData.ContentID,
              taskId: this.props.submitEvalTaskID
            },
            key: 'test' + Math.random() * 10000
          });
        }

        if (this.props.error) {
          console.log(
            "in submitContentEvaluateData props error",
            this.props.errorMsg
          );
          //alert("Some error has occured. Please try again.");
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }


  render() {
    let showSimpleEvalMatrix = this.props.navigation.getParam('showSimpleEvalMatrix', 'NO-data');
    if (this.state.subCriteria == true) {
      showSimpleEvalMatrix = false;
    }
    const showDetailedEvalMatrix = this.props.navigation.getParam('showDetailedEvalMatrix', 'NO-data');

    return (
      <View style={styles.container}>
        <View style={styles.labelView}>
          <Text style={styles.textLabel} >Evaluate Idea</Text>
        </View>
        <View style={styles.viewTwoStyle}>
          {showSimpleEvalMatrix == true && this.state.subCriteria == false && <Text style={{ fontSize: 20, marginTop: 10 }} >This Idea Qualifies for: {this.state.projectName} </Text>}

          {(showSimpleEvalMatrix == false || this.state.subCriteria == true) && <Text style={styles.projectNameTextStyle} >This Idea Qualifies for: {this.state.projectName} </Text>}
          {(showSimpleEvalMatrix == false || this.state.subCriteria == true) && <Text style={styles.projectNameTextStyle} >Score: {this.getScore()}</Text>}
        </View>
        {this.renderSpinner()}
        <View style={styles.scrollViewStyle} >
          <ScrollView style={{ flex: 1 }}>
            {
              showSimpleEvalMatrix == true && this.state.subCriteria == false &&
              this.state.simpleEvalMatrixArray.map((data, i, matrixArray) => {
                return (
                  <View key={i} style={styles.dataNameViewOne}>
                    <View style={styles.dataNameViewTwo}>
                      <Text style={styles.dataNameText}>{data.name}</Text>
                    </View>
                    <View style={styles.switchView}>
                      <Switch
                        thumbColor={'#0077cb'}
                        trackColor={'#00a5ff'}
                        onValueChange={(toggleValue) => {
                          matrixArray[i].toggle = toggleValue;
                          this.setState({ simpleEvalMatrixArray: matrixArray })
                          this.setProjectName();
                        }}
                        value={this.state.simpleEvalMatrixArray[i].toggle}
                      />
                    </View>
                  </View>
                )
              })
            }
            {
              (this.state.subCriteria == true || showSimpleEvalMatrix == false) &&
              this.state.detailedEvalMatrixArray.map((data, i, matrixArray) => {
                return (
                  <View key={i} style={styles.detailedViewOne}>
                    <View style={styles.detailedViewTwo}>
                      <Text >{data.name}</Text>
                    </View>
                    <View style={{ flex: 0.5 }}>
                      <Text style={{ backgroundColor: this.getcolor(i), height: 25, fontSize: 15, fontWeight: 'bold', overflow: "hidden", borderRadius: 3, color: 'white', }}>{" "}{this.state.detailedEvalMatrixArray[i].sliderValue}.0{" "}</Text>

                    </View>
                    <View style={styles.sliderViewStyle}>
                      <Slider
                        value={this.state.detailedEvalMatrixArray[i].sliderValue}
                        onValueChange={(value) => {
                          matrixArray[i].sliderValue = value;
                          this.setState({ detailedEvalMatrixArray: matrixArray });
                          if (this.getScore() <= 3) {
                            this.setState({ projectName: 'Kaizen' });
                          } else {
                            this.setState({ projectName: 'Project' });
                          }
                        }}
                        style={{ height: 40, width: 120, marginLeft: 3 }}
                        minimumValue={1}
                        maximumValue={5}
                        thumbTintColor={'#007bff'}
                        step={2}
                        minimumTrackTintColor={this.getcolor(i)}
                      />
                    </View>
                    <View style={{ alignItems: 'flex-start', flex: 1.5 }}>
                      <Text>{this.getSliderLabel(i)}</Text>
                    </View>
                  </View>
                )
              })
            }
            <View style={styles.xtraViewHeight} />
          </ScrollView>

          <View style={styles.outerContainer}>
            {
              (showSimpleEvalMatrix == false || this.state.subCriteria == true) && this.props.ideaData.ContentStatus != "Awaiting Level 2 Approver" &&
              <View style={styles.buttonViewStyle}>
                <TouchableOpacity onPress={() => { this.doBackSubcriteriaEvaluate() }}>
                  <Text style={styles.buttonTextStyle}>Back</Text>
                </TouchableOpacity>
              </View>
            }
            <View style={styles.buttonViewStyle}>
              <TouchableOpacity onPress={() => { this.closeEvaluate() }}>
                <Text style={styles.buttonTextStyle}>Close</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonViewStyle}>
              <TouchableOpacity onPress={() => { this.submitEvaluate() }}>
                <Text style={styles.buttonTextStyle}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <Toast ref="toast" position='top' />
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
    ideaData: state.DataReducer.IdeaInfoById,
    evaluationMatrix: state.DataReducer.evaluationMatrix,
    ratingData: state.DataReducer.contentDataForEvaluate,
    submitEvalTaskID: !state.DataReducer.submittedEvaluateDataResponse ? '' : state.DataReducer.submittedEvaluateDataResponse.taskID,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ReduxActions, getPendingTasksForLoggedInUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EvaluateApprovals);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    maxWidth: deviceWidth
  },
  sliderViewStyle: {
    flex: 2,
    marginRight: 20
  },
  detailedViewOne: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 25,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1
  },
  detailedViewTwo: {
    alignItems: 'flex-start',
    marginRight: 5,
    flex: 2
  },
  scrollViewStyle: {
    margin: 10,
    flex: 1,
    marginTop: 20
  },
  switchView: {
    width: '20%',
    alignItems: 'flex-end',
    marginRight: 20,
    justifyContent: 'center'
  },
  dataNameText: {
    fontSize: 16,
    color: 'black'
  },
  dataNameViewOne: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  dataNameViewTwo: {
    alignItems: 'flex-start',
    margin: 20,
    justifyContent: 'center',
    flex: 1
  },
  scoreTextStyle: {
    alignItems: 'flex-end',
    width: '30%',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#2A5AC4',
    marginTop: 12
  },
  projectNameTextStyle: {
    flex: 1,
    width: '70%',
    alignItems: 'flex-start',
    fontSize: 17,
    marginTop: 12
  },
  viewTwoStyle: {
    flexDirection: 'row',
    color: 'black',
    margin: 10
  },
  labelView: {
    flexDirection: 'row',
    color: 'black'
  },
  textLabel: {
    fontWeight: 'bold',
    fontSize: 30,
    margin: 10,
    marginBottom: 0,
    marginTop: 30
  },
  xtraViewHeight: {
    height: 100
  },
  buttonViewStyle: {
    borderRadius: 3,
    height: 70,
    alignItems: 'center',
    margin: 5,
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#F51173'
  },
  buttonTextStyle: {
    fontSize: 24,
    color: 'white'
  },
  elementsContainer: {
    height: 500,
  },
  outerContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    bottom: deviceHeight / 1000,
    left: 0, right: 0
  },
})