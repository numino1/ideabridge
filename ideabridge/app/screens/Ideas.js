
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { systemWeights } from 'react-native-typography';
import CardMyIdea from '../components/CardMyIdea';
import { getIdeaInfoById, updateAPIStatusBody } from "../actions/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fonts, header } from '../css/GlobalCss';
let _ = require('lodash');

class Ideas extends Component {

  componentDidMount() {
    var data = [];
    this.setState({ data: data });
    const { navigation } = this.props;
    data = navigation.getParam('data', ' ');
    this.setState({ data: data });
  }

  renderSpinner() {
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  getIdeaByIdAPI(contentId) {

    this.props.getIdeaInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getIdeaByIdAPI props success");
          let ideaData = {
            data: this.props.IdeaInfoById.ContentData,
            costDetailsData: this.props.IdeaInfoById.CostDetails,
            challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
            canRespond: this.props.IdeaInfoById.CanRespond,
            canEdit: this.props.IdeaInfoById.CanEdit,
          };
          if (this.props.userDetails.UserID == this.props.IdeaInfoById.CreatorUserId) {
            ideaData.contentId = this.props.IdeaInfoById.ContentID;
            ideaData.contentStatus = this.props.IdeaInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewIdea", ideaData);

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getIdeaByIdAPI props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  listItemClicked(itemData) {
    this.getIdeaByIdAPI(itemData.ContentID);
  }

  render() {
    const { navigation, ideas } = this.props;
    if (ideas.length == 0) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      )
    }

    let approvedIdeas = _.filter(ideas, (item) => {
      return (item.ContentStatus == "Approved");
    });

    const data = navigation.getParam('data', ' ');
    const ideaCount = approvedIdeas.length;
    return (
      <View style={styles.mainviewStyle} pointerEvents={this.props.loading ? "none" : "auto"}>
        {this.renderSpinner()}
        <ScrollView style={styles.container}>
          <View style={[styles.elementsContainer]}>
            <Text style={[systemWeights.bold, { ...header }]}>Ideas</Text>
            <Text style={styles.count}>( {ideaCount} )</Text>
          </View>

          <View style={styles.padding}>
            <FlatList
              data={approvedIdeas}
              renderItem={({ item: rowData }) => {
                return (
                  <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                    <CardMyIdea rowDatab={rowData} />
                  </TouchableOpacity>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  let count = Object.keys(state.DataReducer.addIdeaInfo).length;
  return {
    myIdeaCount: count > 0 ? count : 0,
    userDetails: state.verifyReducer.userDetails,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    content: (state.homeReducer.content),
    ideas: state.homeReducer.ideas,
    notificationCount: state.homeReducer.notification.NotificationCount,
    approvalCount: state.homeReducer.notification.ApprovalCount,
    searchData: state.homeReducer.serachData,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getIdeaInfoById, updateAPIStatusBody }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Ideas);

const styles = StyleSheet.create({
  mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },
  count: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1,
    marginLeft: 2

  },
  activityIndicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5FCFF88"
  },
  padding: { paddingBottom: 90 }
})