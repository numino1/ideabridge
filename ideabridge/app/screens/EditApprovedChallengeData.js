import React, { Component } from 'react';
import { ScrollView, Platform, StyleSheet, Text, View, FlatList, Button, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Icon, Header } from 'react-native-elements';
import { getIdeaInfoById, postComment, getChallengeInfoById, updateAPIStatusBody } from '../actions/actions'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { fonts, shadow, header } from '../css/GlobalCss'
import DatePicker from 'react-native-datepicker';
import Moment from 'moment';
import * as ReduxActions from "../actions/actions";
import Toast, { DURATION } from 'react-native-easy-toast';
import handleError from '../config/errorHandler';

class EditApprovedChallengeData extends Component {
    static navigationOptions = {
        header: null
    }

    updateDuration() {
        let contentId = this.props.ChallengeInfoById.ContentID
        let startDate = this.state.startDate
        let endDate = this.state.endDate
        var dateStart = Moment(startDate, "DD/MM/YYYY");
        var dateEnd = Moment(endDate, "DD/MM/YYYY");
        let diff = dateEnd.diff(dateStart, "days")
        if (diff <= 0) {
            alert("Start date cannot be greater than end date");
        }
        else {
            let obj =
            {
                formFields: [
                    {
                        Name: "challenge_startdate",
                        Value: this.state.startDate,
                    },
                    {
                        Name: "challenge_enddate",
                        Value: this.state.endDate,
                    },
                ],
                contentId: contentId
            }

            this.props.updateDate({
                data: obj
            })
                .then(() => {
                    this.refs.toast.show('The Date has been updated successfully', 500);
                    this.props.getChallengeInfoById({
                        data: contentId,
                    })
                    if (this.props.success) {
                        this.props.updateAPIStatusBody({
                            data: { loading: false, success: false }
                        });
                    }
                    if (this.props.error) {
                        console.log(
                            "in update duration props error",
                            this.props.errorMsg
                        );
                        if (this.props.errorMsg.response.status == 412) {
                            alert(this.props.errorMsg.response.data.message);
                        } else {
                            //alert("Some error has occured. Please try again");
                            handleError(error, false);
                        }
                        this.props.updateAPIStatusBody({
                            data: { loading: false, error: false, errorMsg: "" }
                        });
                    }
                    this.props.navigation.goBack()
                });
        }
    }


    constructor(props) {
        super(props);
        this.state = {
            startDate: `${(new Date(this.props.ChallengeInfoById.StartDate)).getDate()}/${(new Date(this.props.ChallengeInfoById.StartDate)).getMonth() + 1}/${(new Date(this.props.ChallengeInfoById.StartDate)).getFullYear()}`,
            endDate: `${(new Date(this.props.ChallengeInfoById.EndDate)).getDate()}/${(new Date(this.props.ChallengeInfoById.EndDate)).getMonth() + 1}/${(new Date(this.props.ChallengeInfoById.EndDate)).getFullYear()}`,
        };
    }
    render() {
        let minDate = `${(new Date(this.props.ChallengeInfoById.ContentDate)).getDate()}/${(new Date(this.props.ChallengeInfoById.ContentDate)).getMonth() + 1}/${(new Date(this.props.ChallengeInfoById.ContentDate)).getFullYear()}`
        return (
            <View style={{ flex: 1, }}>
                <Header
                    containerStyle={styles.headerstyle}
                    leftComponent={<Icon name='left' type='antdesign' color='blue' onPress={() => {

                        this.props.navigation.goBack();

                    }} />}
                    centerComponent={{ text: 'Edit challenge', style: { fontSize: 22, color: 'blue' } }}
                />
                <View style={styles.datepickerViewOne}>
                    <View style={styles.datepickerViewTwo}>
                        <View style={styles.datepickerViewThree}>
                            <View style={styles.datepickerViewFour}>
                                <Text style={styles.dateText}>Start Date:</Text>
                            </View>
                            <DatePicker
                                style={{ width: 200 }}
                                date={this.state.startDate}
                                mode="date"
                                placeholder="select date"
                                format="DD/MM/YYYY"
                                minDate={minDate}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                onDateChange={(startDate) => { this.setState({ startDate: startDate }) }}
                            />
                        </View>
                        <View style={styles.datepickerTwoViewOne}>
                            <View style={styles.datapickerViewFour}>
                                <Text style={styles.dateText}>End Date:</Text>
                            </View>
                            <DatePicker
                                style={{ width: 200 }}
                                date={this.state.endDate}
                                mode="date"
                                placeholder="select date"
                                format="DD/MM/YYYY"
                                minDate={minDate}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                onDateChange={(endDate) => { this.setState({ endDate: endDate }) }}
                            />
                        </View>
                        <Toast ref="toast" position='top' />
                    </View>
                    <View style={styles.closeViewStyle}>
                        <TouchableOpacity style={styles.closeButtonStyle}
                            onPress={() => { this.props.navigation.goBack(); }}
                        >
                            <Text style={styles.buttonTextStyle}>Close</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.closeButtonStyle}
                            onPress={() => { this.updateDuration() }}>
                            <Text style={styles.buttonTextStyle}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        IdeaInfoById: state.DataReducer.IdeaInfoById,
        ChallengeInfoById: state.DataReducer.ChallengeInfoById,
        loading: state.DataReducer.generalAPIStatusBody.loading,
        success: state.DataReducer.generalAPIStatusBody.success,
        error: state.DataReducer.generalAPIStatusBody.error,
        errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ReduxActions, getIdeaInfoById, getChallengeInfoById, postComment, updateAPIStatusBody }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(EditApprovedChallengeData);

const styles = StyleSheet.create({
    dateContainer: {
        flex: 1,
        marginTop: 10,
    },
    closeViewStyle: {
        flexDirection: 'row',
        height: 75,
        backgroundColor: '#4e12bc'
    },
    closeButtonStyle: {
        borderWidth: 1,
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'white'
    },
    buttonTextStyle: {
        fontSize: 24,
        color: 'white'
    },
    datepickerTwoViewOne: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    datepickerViewOne: {
        flex: 1,
        justifyContent: 'space-between'
    },
    datepickerViewTwo: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        height: 200
    },
    datepickerViewThree: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '97%'
    },
    datapickerViewFour: {
        justifyContent: 'center'
    },
    dateText: {
        fontSize: 18,
        marginLeft: -10
    },
    headerstyle: {
        backgroundColor: 'white',
        marginTop: Platform.OS === 'ios' ? 0 : - 30
    },
    textStyle: {
        marginLeft: 10,
        marginTop: 50,
        fontSize: 24,
        fontWeight: 'bold'
    },
    headertextStyle: {
        marginLeft: 10,
        marginTop: 50,
        fontSize: 24,
        fontWeight: 'bold'
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    elementsContainer: {
        flex: 1,
        left: 20,
        flexDirection: 'row',
        marginTop: 50
    },
})

