import React, { Component } from 'react';
import { ScrollView, Platform, StyleSheet, Text, View, FlatList, ActivityIndicator, Button, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Avatar, Card, SearchBar, Divider } from 'react-native-elements';
import { getIdeaInfoById, postComment, getChallengeInfoById, updateAPIStatusBody } from '../actions/actions'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { fonts, shadow, header } from '../css/GlobalCss'

class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  onChangeText = (text) => {
    this.setState({ text: text })
  }

  renderSpinner() {
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  postCommentMessage() {
    const contentype = this.props.navigation.getParam('type', 'NO-data');
    if (this.state.text) {
      if (contentype == 'Idea') {
        this.props.postComment(this.state.text, this.props.IdeaInfoById.ContentID)
          .then(() => {
            console.log("in postComment props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.setState({ text: '' });
            this.props.getIdeaInfoById({
              data: this.props.IdeaInfoById.ContentID
            })
              .then(() => {
                console.log("in postComment getIdeaInfoById props success");
                this.props.updateAPIStatusBody({
                  data: { loading: false, success: false }
                });
              });
          });
      } else {
        this.props.postComment(this.state.text, this.props.ChallengeInfoById.ContentID)
          .then(() => {
            console.log("in postComment props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.setState({ text: '' });
            this.props.getChallengeInfoById({
              data: this.props.ChallengeInfoById.ContentID
            })
              .then(() => {
                console.log("in postComment getIdeaInfoById props success");
                this.props.updateAPIStatusBody({
                  data: { loading: false, success: false }
                });
              });
          });
      }
    }
  }

  render() {
    const contentype = this.props.navigation.getParam('type', 'NO-data');
    let commentCount = 0
    let commentList = []

    if (contentype == 'Idea') {
      const { IdeaInfoById } = this.props;
      if (IdeaInfoById) {
        commentCount = IdeaInfoById.Comment.length;
        commentList = IdeaInfoById.Comment;
      }
    } else {
      const { ChallengeInfoById } = this.props;
      if (ChallengeInfoById) {
        commentCount = ChallengeInfoById.Comment.length;
        commentList = ChallengeInfoById.Comment;
      }
    }

    return (

      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }}>
          <View style={[styles.elementsContainer]}>
            <Text style={[systemWeights.bold, { ...header }]}>Comments</Text>
            <Text style={styles.countStyle}>({commentCount})</Text>
          </View>

          <View style={styles.flatListViewStyle}>
            {this.renderSpinner()}
            <FlatList
              data={commentList}
              renderItem={({ item: rowData }) => {
                return (
                  <View>
                    <Card title={null}
                      containerStyle={[styles.card]}
                    >
                      <View style={{ flexDirection: 'column' }}>
                        <Text style={styles.fullNameStyle}>
                          {rowData.UserDetails.FullName}
                        </Text>
                        <Text style={{ fontSize: fonts.sm + 3 }}>
                          {`${(new Date(rowData.ActivityDate)).getDate()}/${(new Date(rowData.ActivityDate)).getMonth() + 1}/${(new Date(rowData.ActivityDate)).getFullYear()}`}
                        </Text>
                      </View>

                      <Text style={[systemWeights.regular, styles.commentStyle]}>
                        {rowData.Comment}
                      </Text>

                    </Card>

                  </View>

                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={styles.xtraView} />
        </ScrollView>

        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={Platform.OS == 'ios' ? '100' : '-150'} style={styles.outerContainer}>
          <View style={styles.textInputContainer}>
            <TextInput
              style={{ fontSize: fonts.sm + 5 }}
              placeholder="Type a message"
              placeholderTextColor='black'
              onChangeText={this.onChangeText}
              value={this.state.text}
              autoFocus={true}
            />
          </View>
          <TouchableOpacity style={[styles.fieldName]} onPress={() => { this.postCommentMessage() }}>
            <Ionicons name='md-send' style={styles.filterButton} />
          </TouchableOpacity>
        </KeyboardAvoidingView>

      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getIdeaInfoById, getChallengeInfoById, postComment, updateAPIStatusBody }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Comments);
const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  commentStyle: {
    marginTop: 10,
    color: 'black',
    fontSize: fonts.sm + 6
  },
  xtraView: {
    height: 100
  },
  fullNameStyle: {
    color: 'black',
    fontSize: fonts.sm + 4
  },
  flatListViewStyle: {
    paddingBottom: 10
  },
  countStyle: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },
  filterButton: {
    fontSize: fonts.lg + 2,
    color: 'black',
    alignSelf: 'center',
  },
  card: {
    ...shadow,
    width: window.width,
    height: window.height,
    borderRadius: 6,
    marginBottom: 10,
    backgroundColor: 'skyblue',
  },
  outerContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 20,
    left: 0, right: 0
  },
  fieldName: {
    flex: 1,
    backgroundColor: 'white',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'lightgrey',
  },
  textInputContainer: {
    flex: 5,
    backgroundColor: 'white',
    height: 60,
    justifyContent: 'center',
    margin: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'lightgrey',
  },
})