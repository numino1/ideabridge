import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, Button, FlatList, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import CardApprovals from '../components/CardApprovals';
let _ = require('lodash');
import { fonts, dimensions, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';
import { getPendingTasksForLoggedInUser } from '../actions/home';
import { HeaderBackButton } from 'react-navigation';

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height

class Approvals extends Component {
  static navigationOptions = ({ navigate, navigation, props }) => {
    return {
      headerLeft: (<HeaderBackButton onPress={() => { navigation.navigate('HomeScreen') }} />)
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      text: '',

    };
  }

  initiateHomeAction() {
    this.props.getPendingTasksForLoggedInUser();
  }

  componentDidMount() {
    const { navigation } = this.props;

  }

  onChangeText = (text) => {

    this.setState({
      text,
    })
  }

  getIdeaByIdAPI(contentId, taskId, contentType) {
    if (contentType == 1) {

      this.props
        .getIdeaInfoById({
          data: contentId,
          taskID: taskId
        })
        .then(() => {
          if (this.props.success) {
            //console.log("in getIdeaByIdAPI props success", this.props.IdeaInfoById);
            console.log("in getIdeaByIdAPI props success", this.props.IdeaInfoById);
            let ideaData = {
              data: this.props.IdeaInfoById.ContentData,
              costDetailsData: this.props.IdeaInfoById.CostDetails,
              challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
              formControls: this.props.IdeaInfoById.FormControls,
              //required for approvals
              contentId: this.props.IdeaInfoById.ContentID,
              showReviewerInvite: this.props.IdeaInfoById.ShowReviewerInvite,
              reviewersList: this.props.IdeaInfoById.IdeaReviewers,
              taskId: taskId,
              showPatent: this.props.IdeaInfoById.PatentRequired,
              canEdit: this.props.IdeaInfoById.CanEdit,
            };
            if (this.props.userDetails.UserID == this.props.IdeaInfoById.CreatorUserId) {
              ideaData.contentId = this.props.IdeaInfoById.ContentID;
              ideaData.contentStatus = this.props.IdeaInfoById.ContentStatus;
            }
            this.props.navigation.navigate("ViewPagerViewIdea", ideaData);
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
          }
          if (this.props.error) {
            console.log(
              "in getIdeaByIdAPI props error",
              this.props.errorMsg
            );
            //alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (contentType == 2) {

      this.props
        .getChallengeInfoById({
          data: contentId,
          taskID: taskId
        })
        .then(() => {
          if (this.props.success) {
            let challengeData = {
              data: { ...this.props.ChallengeInfoById.ContentData },
              title: this.props.ChallengeInfoById.Title,
              type: this.props.ChallengeInfoById.ChallengeType,
              attachments: this.props.ChallengeInfoById.Attachment,
              formControls: this.props.ChallengeInfoById.FormControls,
              //required for approvals
              contentId: this.props.ChallengeInfoById.ContentID,
              showReviewerInvite: this.props.ChallengeInfoById.ShowReviewerInvite,
              reviewersList: this.props.ChallengeInfoById.ChallengeReviewers,
              taskId: taskId,
            };
            if (this.props.userDetails.UserID == this.props.ChallengeInfoById.ChallengerDetails.UserID) {
              challengeData.contentId = this.props.ChallengeInfoById.ContentID;
              challengeData.contentStatus = this.props.ChallengeInfoById.ContentStatus;
            }
            this.props.navigation.navigate("ViewPagerViewChallenge", challengeData);

            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
          }
          if (this.props.error) {
            console.log(
              "in getIdeaByIdAPI props error",
              this.props.errorMsg
            );
            //alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });

    } else if (contentType == 3) {
      alert("Project: coming soon..");
    }
  }

  onClearSearchText = () => {

  }

  renderSpinner() {
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  listItemClicked(itemData) {
    this.getIdeaByIdAPI(itemData.ContentId, itemData.TaskId, itemData.ContentTypeId);
  }

  addIdeaButton() {
    console.log("in add idea button");
    let savedStatus = {
      status: false
    };

    this.props.updateSavedAddIdeaFlag({ status: savedStatus });
    let ideaId = this.generateID();
    this.props.navigation.navigate('ViewPager', { ideaId: ideaId });
  }

  handleKeyDown = () => {
  }

  render() {
    let userID;
    let { navigation, approvals, userDetails } = this.props;

    if (approvals.length == 0) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      )
    }
    if (this.state.text) {
      approvals = _.filter(approvals, (item) => {
        return (item.Title.toLowerCase().indexOf((this.state.text).toLowerCase()) != -1);
      });
    }
    let count = approvals.length;

    return (
      <View pointerEvents={this.props.loading ? "none" : "auto"} style={styles.container}>
        {this.renderSpinner()}
        <OfflineNotice />
        <ScrollView style={styles.container}>
          <View style={styles.elementsContainer}>
            <Text style={[systemWeights.bold, { ...header }]}>Approvals</Text>
            <Text style={styles.countStyle}>( {count} )</Text>
          </View>

          <View style={styles.parentSearchBar}>
            <SearchBar
              lightTheme
              inputContainerStyle={[styles.color]}
              leftIconContainerStyle={[styles.color]}
              rightIconContainerStyle={[styles.color]}
              containerStyle={styles.childSearchBar}
              inputStyle={styles.searchBarInputStyle}
              value={this.state.text}
              onChangeText={this.onChangeText}
              placeholder='Search'
              placeholderTextColor={'black'}
              clearIcon={this.state.text !== '' ? { name: 'close', color: 'black', type: 'evilicon', size: 32 } : false}
              searchIcon={{ name: 'search', type: 'evilicons', size: 32, color: 'black' }}
              onSubmitEditing={this.handleKeyDown}
              onClear={this.onClearSearchText}
              showLoadingIcon={this.state.loading} />
          </View>
          {approvals.length === 0 ?
            <View style={styles.resSearchData}>
              <Text style={styles.resultNotFoundStyle}>Results not found</Text>
            </View> :
            <View style={styles.flatListView}>
              <FlatList
                key={approvals.length}
                data={approvals}
                renderItem={({ item: rowData }) => {
                  return (
                    <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                      <CardApprovals rowDatab={rowData} />
                    </TouchableOpacity>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.verifyReducer.userDetails,
    addIdeaInfo: state.DataReducer.addIdeaInfo,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
    approvals: state.homeReducer.pendingTasks,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,

  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ReduxActions, getPendingTasksForLoggedInUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Approvals);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  elementsContainer: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 50
  },
  countStyle: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1,
    marginLeft: 2
  },
  searchBarInputStyle: {
    backgroundColor: '#FFFFFF',
    fontSize: fonts.md,
    fontWeight: '500',
    color: 'black',
    left: -9
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    fontSize: fonts.md,
  },
  searchIcon: {
    padding: 10,
  },
  resultNotFoundStyle: {
    fontSize: fonts.sm + 5,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: 'white',
    color: '#424242',
  },
  parentSearchBar: {
    width: '100%',
    flexDirection: 'row',
    maxWidth: deviceWidth,
    marginLeft: 5
  },
  childSearchBar: {
    width: '64%',
    margin: '3%',
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    width: '91%'
  },
  parentIconBar: {
    width: '100%',
    flexDirection: 'row'
  },
  childIconBar: {
    width: '34%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  childViewBadge: {
    width: '34%',
    margin: '3%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  flatListView: {
    paddingBottom: 90
  },
  filterIcon: {
    fontSize: fonts.lg + 2,
    color: 'grey',
    left: 20,
    bottom: 8
  },
  mainviewStyle: {
    flex: 1,
  },
  color: {
    backgroundColor: '#FFFFFF',
  },
  floatingButtonContainer: {
    flexDirection: 'row',
    width: '44%',
    justifyContent: 'flex-end'
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: dimensions.fullHeight / 1000,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    height: 90,
    width: '100%',
    borderColor: 'black',
  },
  floatingButton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 4,
    borderColor: 'white',
    width: 60,
    height: 60,
    margin: 12,
    backgroundColor: 'white',
    borderRadius: 100,
    elevation: 4,
    shadowOffset: { width: 5, height: 5 },
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowRadius: 10,
    backgroundColor: 'orange'
  },
  footerText: {
    color: '#0086b3',
    fontWeight: 'bold',
    alignItems: 'center',
    fontSize: fonts.lg,
  },
  footerText2: {
    alignItems: 'center',
    fontWeight: 'bold',
    color: 'white',
    fontSize: fonts.md + 1,
  },
  iconContainer: {
    width: '56%',
    left: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignSelf: 'center'
  },
  resSearchData: {
    paddingBottom: Platform.OS === 'android' ? 90 : 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
})