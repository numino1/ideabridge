import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, Alert, FlatList, TouchableHighlight, TouchableOpacity, Dimensions, ActivityIndicator, TextInput, KeyboardAvoidingView } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import CardApprovals from '../components/CardApprovals';
let _ = require('lodash');
import { fonts, dimensions, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast';
import { Dropdown } from 'react-native-material-dropdown';
import { CLIENT_UAT1_URL } from '../config/configuration';
import axios from "axios";
import DynamicForm, { buildTheme } from 'react-native-dynamic-form';
import { getPendingTasksForLoggedInUser } from '../actions/home';
import DatePicker from 'react-native-datepicker';
import handleError from '../config/errorHandler';
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


class ApprovalOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remarks: '',
      parkTillDate: '',
      rejectReason: '',
      suggestions: [],
      reviewerIds: '',
      category: '',
      subCategory: '',
      subCategoryList: [],
      reviewersCounter: this.props.navigation.getParam('reviewerCount', 0),
      userList: [],
      reasonSelected: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    this.props.getRejectionReasons(contentId);
    this.props.getAllCategoriesAndSubCategories();
    const showTags = this.props.navigation.getParam('showTags', 'NO-data');
    const reviewersList = this.props.navigation.getParam('reviewersList', '');
    if (showTags == true) {
      if (reviewersList.length > 0) {
        reviewersList.map((item, idx) => {
          if (this.state.userList.findIndex(data => data.name === item.Reviewers.FullName) === -1) {
            this.state.userList.push(
              {
                name: item.Reviewers.FullName,
                disabled: true
              }
            );
          }
        });
      }
    }
  }

  customFilterData = query => {
    if (this.state.reviewersCounter > 1) {
      return;
    }
    query = query.toLowerCase();
    this.getAllPeopleEmail(query);
  };

  initiateHomeAction() {
    this.props.getPendingTasksForLoggedInUser();
  }

  validateForInputs(index) {
    const showDropdown = this.props.navigation.getParam('showDropdown', 'NO-data');
    const showDropdownTwice = this.props.navigation.getParam('showDropdownTwice', 'NO-data');
    const showTags = this.props.navigation.getParam('showTags', 'NO-data');
    const showDate = this.props.navigation.getParam('showDate', 'NO-data');

    if (this.state.remarks == '') {
      Alert.alert("Please enter comments/remarks");
    } else if (showDropdown == true) {
      if (this.state.rejectReason == '') {
        Alert.alert("Please select rejection reason");
      } else {
        this.executeAPI(index);
      }
    } else if (showDropdownTwice == true) {
      if (this.state.category == '') {
        Alert.alert("Please select Category");
      } else if (this.state.subCategory == '') {
        Alert.alert("Please select Subcategory");
      }
      else {
        this.executeAPI(index);
      }
    } else if (showTags == true) {
      if (this.state.reviewerIds == '') {
        Alert.alert("Please select reviewer");
      } else {
        this.executeAPI(index);
      }
    } else if (showDate == true) {
      if (this.state.parkTillDate == '') {
        Alert.alert("Please select Date");
      }
      else {
        this.executeAPI(index);
      }
    }
    else {
      this.executeAPI(index);
    }
  }

  renderSpinner() {
    //check state value of loading variable
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  executeAPI(index) {
    const contentId = this.props.navigation.getParam('contentId', 'NO-data');
    const taskId = this.props.navigation.getParam('taskId', 'NO-data');
    const title = this.props.navigation.getParam('title', 'NO-data');
    const type = this.props.navigation.getParam('type', 'NO-data');
    if (index == 0) {
      let publishData = {
        contentId: contentId,
        taskId: taskId,
        remarks: this.state.remarks
      }
      if (type == 1) {
        this.props.publishIdeaContent({ data: publishData })
          .then(() => {
            if (this.props.success) {
              console.log("in publishIdeaContent props success");
              this.props.updateAPIStatusBody({
                data: { loading: false, success: false }
              });
              this.refs.toast.show('Idea has been published successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
            if (this.props.error) {
              console.log(
                "in publishIdeaContent props error",
                this.props.errorMsg
              );
              alert(this.props.errorMsg);
              this.props.updateAPIStatusBody({
                data: { loading: false, error: false, errorMsg: "" }
              });
            }
          });
      } else {
        this.props.publishChallengeContent({ data: publishData })
          .then(() => {
            if (this.props.success) {
              console.log("in publishChallengeContent props success");
              this.props.updateAPIStatusBody({
                data: { loading: false, success: false }
              });
              this.refs.toast.show('Challenge has been published successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
            if (this.props.error) {
              console.log(
                "in publishChallengeContent props error",
                this.props.errorMsg
              );
              alert(this.props.errorMsg);
              this.props.updateAPIStatusBody({
                data: { loading: false, error: false, errorMsg: "" }
              });
            }
          });
      }
    } else if (index == 1) {
      let redirectData = {
        contentId: contentId,
        taskId: taskId,
        remarks: this.state.remarks
      };
      this.props.redirectContent({ data: redirectData })
        .then(() => {
          if (this.props.success) {
            console.log("in redirectData props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (type == 1) {
              this.refs.toast.show('Idea has been redirected successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            } else {
              this.refs.toast.show('Challenge has been redirected successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
          }
          if (this.props.error) {
            console.log(
              "in redirectData props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });

    } else if (index == 2) {
      let queryData = {
        contentId: contentId,
        taskId: taskId,
        remarks: this.state.remarks
      }
      this.props.askQuery({ data: queryData })
        .then(() => {
          if (this.props.success) {
            console.log("in askQuery props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (type == 1) {
              this.refs.toast.show('Idea query has been posted successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            } else {
              this.refs.toast.show('Challenge query has been posted successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
          }
          if (this.props.error) {
            console.log(
              "in askQuery props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });

    } else if (index == 3) {
      let rejectData = {
        contentId: contentId,
        taskId: taskId,
        remarks: this.state.remarks
      }
      this.props.rejectContent({ data: rejectData })
        .then(() => {
          if (this.props.success) {
            console.log("in rejectContent props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (type == 1) {
              this.refs.toast.show('Idea has been rejected successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            } else {
              this.refs.toast.show('Challenge has been rejected successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
          }
          if (this.props.error) {
            console.log(
              "in rejectContent props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });

    } else if (index == 4) {
      let reworkData = {
        contentId: contentId,
        taskId: taskId,
        remarks: this.state.remarks,
      }
      if (title === 'Send Back to Ideator') {
        this.props.reworkIdeaContent({ data: reworkData })
          .then(() => {
            if (this.props.success) {
              console.log("in reworkIdeaContent props success");
              this.props.updateAPIStatusBody({
                data: { loading: false, success: false }
              });
              this.refs.toast.show('Idea has been sent for rework successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
            if (this.props.error) {
              console.log(
                "in reworkIdeaContent props error",
                this.props.errorMsg
              );
              alert(this.props.errorMsg);
              this.props.updateAPIStatusBody({
                data: { loading: false, error: false, errorMsg: "" }
              });
            }
          });
      } else {
        this.props.reworkChallengeContent({ data: reworkData })
          .then(() => {
            if (this.props.success) {
              console.log("in reworkChallengeContent props success");
              this.props.updateAPIStatusBody({
                data: { loading: false, success: false }
              });
              this.refs.toast.show('Challenge has been send for rework successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
            if (this.props.error) {
              console.log(
                "in reworkChallengeContent props error",
                this.props.errorMsg
              );
              alert(this.props.errorMsg);
              this.props.updateAPIStatusBody({
                data: { loading: false, error: false, errorMsg: "" }
              });
            }
          });
      }
    } else if (index == 5) {
      let reviewerData = {
        contentID: contentId,
        assignReviewerUserIds: this.state.reviewerIds,
        assignReviewerComments: this.state.remarks
      }
      this.props.assignReviewer({ data: reviewerData })
        .then(() => {
          if (this.props.success) {
            console.log("in assignReviewer props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('Reviewer/s has/have been assigned successfully.', 500, () => {
              this.initiateHomeAction();
              this.props.navigation.navigate("Approvals");
            });
          }
          if (this.props.error) {
            console.log(
              "in assignReviewer props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (index == 6) {
      let data = {
        contentID: contentId,
        taskID: taskId,
        remarks: this.state.remarks
      }
      this.props.rejectRedirectRequest({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in rejectRedirectRequest props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('Redirect request has been successfully rejected.', 500, () => {
              this.initiateHomeAction();
              this.props.navigation.navigate("Approvals");
            });
          }
          if (this.props.error) {
            console.log(
              "in rejectRedirectRequest props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (index == 7) {
      let data = {
        contentID: contentId,
        categoryID: this.state.category,
        subCategoryID: this.state.subCategory
      }
      this.props.changeCategory({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in changeCategory props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('Category has been changed successfully.', 500, () => {
              this.initiateHomeAction();
              this.props.navigation.navigate("Approvals");
            });
          }
          if (this.props.error) {
            console.log(
              "in changeCategory props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (index == 9) {
      let data = {
        contentID: contentId,
        remarks: this.state.remarks,
      }
      this.props.applyForPatent({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in applyForPatent props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('Patent has been applied successfully.', 500, () => {
              this.initiateHomeAction();
              this.props.navigation.navigate("Approvals");
            });
          }
          if (this.props.error) {
            console.log(
              "in applyForPatent props error",
              this.props.errorMsg
            );
            //alert("Some error has occured. Please try again.");
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }
    else if (index == 10 || index == 11) {
      let data = {
        contentID: contentId,
        reviewerComments: this.state.remarks,
        reviewerStatus: index == 10 ? "Approved" : "Rejected",
        reviewerUserName: "",
        scores: ""
      }
      this.props.submitReview({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in submitReview props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            if (type == 1 && index == 10) {
              this.refs.toast.show('Idea has been approved successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            } else if (type == 2 && index == 10) {
              this.refs.toast.show('Challenge has been approved successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            } else if (type == 1 && index == 11) {
              this.refs.toast.show('Idea has been rejected successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            } else if (type == 2 && index == 11) {
              this.refs.toast.show('Challenge has been rejected successfully.', 500, () => {
                this.initiateHomeAction();
                this.props.navigation.navigate("Approvals");
              });
            }
          }
          if (this.props.error) {
            console.log(
              "in submitReview props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (index == 12) {
      let data = {
        contentID: contentId,
        Response: this.state.remarks,
      }
      this.props.respondToQuery({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in respondToQuery props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('Responded to query successfully.', 500, () => {
              this.initiateHomeAction();
              this.props.navigation.navigate("Approvals");
            });
          }
          if (this.props.error) {
            console.log(
              "in arespondToQuery props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (index == 13) {
      let data = {
        ContentID: contentId,
        Remarks: this.state.remarks,
        ParkEndDate: this.state.parkTillDate,
        TaskID: taskId,//taskId
      }
      this.props.parkIdea({ data: data })
        .then(() => {
          if (this.props.success) {
            console.log("in parkIdea props success");
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
            this.refs.toast.show('Idea has been parked successfully.', 500, () => {
              this.initiateHomeAction();
              this.props.navigation.navigate("Approvals");
            });
          }
          if (this.props.error) {
            console.log(
              "in parkIdea props error",
              this.props.errorMsg
            );
            alert(this.props.errorMsg);
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }
  }

  getAllPeopleEmail(text) {
    let self = this;
    let tagList = [];
    const url = CLIENT_UAT1_URL + `/User/UserSearch`;
    let config = {
      headers: {
        api_key: 'LgOiuSu64U',
        Authorization: this.props.token
      },
      params: {
        prefix: text,
        excludeIDs: '0',
        includeCurrentUser: false,
        onlyIdeaEntryOperators: false
      },
    };

    return axios
      .get(url, config)
      .then(function (response) {
        console.log("In getAllTeamMembers approvals", response);
        if (response.data !== null && response.data.length > 0) {
          response.data.map((item, i) => {
            if (self.state.userList.findIndex(data => data.name === item.name) === -1) {
              tagList.push(
                {
                  name: item.name,//item.info,
                  value: item.id
                });
            }
          });
        }
        console.log("Teammembers list approvals", tagList);
        self.setState({ suggestions: tagList });
      })
      .catch(function (error) {
        console.log("In getAllTeamMembers approvals", error);
        self.setState({ suggestions: [] });
        handleError(error, false);
      });
  }

  formDataChanges(data) {
    console.log("form approvals data", data);
    let newDataString = "";
    let self = this;
    if (data.tags_reviewers.userAnswer.length == 0) {
      this.state.userList = [];
    }
    if (data.tags_reviewers.userAnswer.length > 1) {
      this.setState({ suggestions: [] });
    }
    if (data.tags_reviewers.userAnswer.length > 0) {
      newDataString = Object.keys(data.tags_reviewers.userAnswer).map(function (k) {
        return data.tags_reviewers.userAnswer[k].value
      }).join(",");
      console.log("form approvals data", newDataString);
    }
    this.setState({ reviewerIds: newDataString });
    this.setState({ reviewersCounter: data.tags_reviewers.userAnswer.length })
    console.log("counter", this.state.reviewersCounter);
  }

  getSubCategory(category) {
    this.setState({ category });
    let subCategoryList = [];
    this.props.allCategorySubCategoryData.map((item) => {
      if (item.groupValue == category) {
        subCategoryList.push(
          {
            label: item.subGroupLabel,
            value: item.subGroupValue,
          });
      }
    })
    this.setState({ subCategoryList });
  }

  render() {
    const title = this.props.navigation.getParam('title', 'NO-data');
    const label = this.props.navigation.getParam('label', 'NO-data');
    const btnLabel = this.props.navigation.getParam('btnLabel', 'NO-data');
    const index = this.props.navigation.getParam('index', 'NO-data');
    const showDropdown = this.props.navigation.getParam('showDropdown', 'NO-data');
    const showDropdownTwice = this.props.navigation.getParam('showDropdownTwice', 'NO-data');
    const showTags = this.props.navigation.getParam('showTags', 'NO-data');
    const showDate = this.props.navigation.getParam('showDate', 'NO-data');
    const minDate = new Date();
    let dropDownList = [];
    let categoryList = [];
    const formTheme = {
      label: {
        fontWeight: 'bold',
        fontSize: 25
      },
    }
    const mytheme = buildTheme({}, {}, formTheme);
    const fieldsArray = [
      {
        key: 'tags_reviewers',
        type: 'tags',
        label: 'Select Reviewer',
        data: this.state.suggestions,
        filterForTags: this.customFilterData,
        values: this.state.userList,
        searchInputPlaceholder: 'Type name or email id'
      }
    ];
    if (showDropdown == true) {
      this.props.rejectionReasons.map((item) => {
        dropDownList.push(
          {
            label: item.Name,
            value: item.Id,
          });
      })
    }
    if (showDropdownTwice == true) {
      this.props.allCategorySubCategoryData.map((item) => {
        if (categoryList.findIndex(data => data.label === item.groupLabel) === -1) {
          categoryList.push(
            {
              label: item.groupLabel,
              value: item.groupValue,
            });
        }
      })
    }
    return (
      <View style={styles.container}>
        <View style={styles.titleViewStyle}>
          <Text style={styles.titleTextStyle} >{title}</Text>
        </View>

        <View style={styles.scrollViewStyle} >
          <ScrollView style={styles.scrollStyle}>
            {showDropdownTwice == true &&
              <View>
                <Text style={styles.dropDownTitleStyle}>Select Category</Text>
                <Dropdown
                  onChangeText={category => { this.getSubCategory(category) }
                  }
                  value={this.state.category}
                  data={categoryList}
                  lineWidth={2}
                  inputContainerStyle={styles.dropDownInputContainerStyle}
                />
                <Text style={styles.secondDropDownTitleStyle}>Select Sub Category</Text>
                <Dropdown
                  onChangeText={subCategory =>
                    this.setState({ subCategory })
                  }
                  value={this.state.subCategory}
                  data={this.state.subCategoryList}
                  lineWidth={2}
                  inputContainerStyle={styles.dropDownInputContainerStyle}
                />
              </View>
            }

            {showDropdown == true && <Dropdown
              label={this.state.reasonSelected ? "" : "Select Reason"}
              onChangeText={rejectReason =>
                {
                  this.setState({ rejectReason });
                  this.setState({reasonSelected: true});
              }
              }
              value={this.state.rejectReason}
              data={dropDownList}
              lineWidth={2}
              inputContainerStyle={styles.reasonDropDownStyle}
            />}

            {showDate == true && <DatePicker
              style={styles.datePickerStyles}
              date={this.state.parkTillDate}
              mode="date"
              minDate={minDate}
              placeholder="Select Date"
              format="DD/MM/YYYY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              onDateChange={(parkTillDate) => { this.setState({ parkTillDate: parkTillDate }) }}
            />}

            <Text style={styles.textInputLabel}>{label}</Text>

            <TextInput
              style={styles.textInputStyle}
              onChangeText={(text) => this.setState({ remarks: text })}
              value={this.state.remarks}
              multiline={true}
              placeholder='Maximum allowed characters is 250'
              maxLength={250}
            />

            {
              showTags == true && <View style={[styles.elementsContainer]}>
                <DynamicForm
                  form={fieldsArray}
                  theme={mytheme}
                  style={styles.backgroundStyle}
                  onFormDataChange={(data) => this.formDataChanges(data)}
                />
              </View>
            }
            {this.renderSpinner()}
            <View style={styles.xtraViewHeight}></View> 
          </ScrollView>
          <View style={styles.outerContainer}>
            <View style={styles.buttonViewStyle}>
              <TouchableOpacity onPress={() => { this.validateForInputs(index) }}>
                <Text style={styles.buttonTextStyle}>{btnLabel}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Toast ref="toast" position='top' />
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    approvals: state.homeReducer.pendingTasks,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
    rejectionReasons: state.DataReducer.rejectionReasons,
    allCategorySubCategoryData: state.DataReducer.allCategorySubCategoryData,
    token: state.verifyReducer.token,

  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ReduxActions, getPendingTasksForLoggedInUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ApprovalOptions);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  elementsContainer: {
    height: 500,
  },
  xtraViewHeight: {
    height: 200
  },
  buttonViewStyle: {
    borderRadius: 3,
    height: 80,
    alignItems: 'center',
    margin: 20,
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#F51173'
  },
  buttonTextStyle: {
    fontSize: 24,
    color: 'white'
  },
  titleViewStyle: {
    flexDirection: 'row',
    color: 'black',
    margin: 20,
    flexWrap:'wrap'
  },
  titleTextStyle: {
    fontWeight: 'bold',
    fontSize: 30,
    marginTop: 20
  },
  scrollViewStyle: {
    margin: 10,
    flex: 1
  },
  scrollStyle:
  {
    flex: 1
  },
  dropDownTitleStyle: {
    fontWeight: 'bold',
    marginLeft: 20,
    marginBottom: 0,
    marginTop: 20,
    fontSize: 25,
    color: 'black'
  },
  secondDropDownTitleStyle: {
    fontWeight: 'bold',
    marginLeft: 20,
    marginTop: 20,
    fontSize: 25,
    color: 'black'
  },
  dropDownInputContainerStyle: {
    marginTop: 0,
    margin: 20,
    borderBottomWidth: 2
  },
  reasonDropDownStyle: {
    margin: 20,
    borderBottomWidth: 2
  },
  datePickerStyles: {
    height: 75,
    width: 200,
    margin: 20
  },
  textInputLabel: {
    flexDirection: 'row',
    color: 'black',
    left: 20,
    fontWeight: 'bold',
    fontSize: 25
  },
  textInputStyle: {
    fontSize: 18,
    margin: 20,
    top: 0,
    flexDirection: 'row',
    borderBottomColor: 'black',
    borderBottomWidth: 1
  },
  customTagsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    backgroundColor: "#efeaea",
    width: '100%'
  },
  customTag: {
    backgroundColor: "#9d30a5",
    justifyContent: "center",
    alignItems: "center",
    height: 30,
    marginLeft: 5,
    marginTop: 5,
    borderRadius: 30,
    padding: 8
  },
  autocompleteContainer: {
    flex: 1,
    left: 20,
    position: "absolute",
    right: 10,
    top: 40,
    backgroundColor: '#FFFFFF',
    overflow: 'visible',
    height: 10,
    zIndex: 1,

  },
  label: {
    color: "#614b63",
    fontWeight: "bold",
    marginBottom: 10
  },
  header: {
    flex: 1,
    backgroundColor: "#9d30a5",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    paddingTop: 15,
    marginBottom: 10,
    left: 20,
  },
  backgroundStyle: {
  },
  outerContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    bottom: deviceHeight / 1000,
    left: 0, right: 0
  },
})