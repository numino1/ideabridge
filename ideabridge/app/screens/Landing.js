import React, { Component } from 'react';
import { StyleSheet, View, Image, Dimensions, Animated, Easing } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { verify, setAppToken } from '../actions/verify';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import base64 from 'react-native-base64';
import { dimensions } from '../css/GlobalCss';

class Landing extends Component {

    static navigationOptions = {

        header: null,
    }

    constructor(props) {

        super(props);

        this.fadeAway = new Animated.Value(0)
    }

    componentDidMount() {

        Animated.timing(this.fadeAway, {
            toValue: 1,
            duration: 2000,
            easing: Easing.ease
        }).start(() => {
            console.log("Animation Done");
            this.isAuthenticated();
        })
    }

    isAuthenticated() {

        let emailPromies = AsyncStorage.getItem("@EmailId");
        let verificationCodePromiese = AsyncStorage.getItem("@VerifiedCode");
        Promise.all([emailPromies, verificationCodePromiese])
            .then((data) => {
                if (data[1] && data[1]) {
                    let info = {
                        'code': data[1],
                        'apiKey': 'LgOiuSu64U',
                        'emailId': data[0]
                    }
                    console.log('info==>', info)
                    this.props.verify(info, () => {
                        let token = `Basic ${base64.encode(data[0] + ':' + data[1])}`;
                        this.props.setAppToken(token);
                        this.props.navigation.navigate('HomeScreen');
                    }, (error) => {
                        console.log('error async', error);
                        this.props.navigation.navigate('LoginScreen')
                    })
                } else {
                    this.props.navigation.navigate('LoginScreen')
                }
            })
    }

    render() {

        const opacity = this.fadeAway.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        });

        return (
            <View style={styles.container}>
                <Animated.View style={{ top: -10, opacity: opacity }}>
                    <Image

                        style={{ width: dimensions.fullWidth, resizeMode: 'contain' }}
                        source={require('../../images/IdeaBridge_Logo.png')}

                    />
                </Animated.View>
            </View>
        )
    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ verify, setAppToken }, dispatch);
}

export default connect(null, mapDispatchToProps)(Landing);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5F5F5',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%',
        height: dimensions.fullHeight - 40

    }
})