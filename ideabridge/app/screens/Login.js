import React, { Component } from 'react';
import {
  ScrollView, FlatList, StyleSheet, TouchableOpacity,
  View, TextInput, Dimensions, Button, Image, Text, Animated, Easing, Platform, BackHandler
} from 'react-native';
import * as ReduxActions from "../actions/actions"; //Import your actions
import { login, updateLoggedInEmail } from '../actions/auth';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { verify } from '../actions/verify';
import OfflineNotice from '../components/OfflineNotice';
import { fonts, fontStyle, dimensions } from '../css/GlobalCss'
import { NavigationEvents } from 'react-navigation';


class Login extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }

  constructor(props) {
    super(props);
    this.state = {
      emailId: '',
      isAnimationComplete: false,
      apiStatus: true,
      validated: false,
      isDisabled: true,
      emaildID: '',
      code: ''
    }

    this.scaleImage = new Animated.Value(0)

    this.slideLeft = new Animated.Value(0)

    this.slideImageUp = new Animated.Value(0)

    this.fadeAway = new Animated.Value(0)

    this.fadeIn = new Animated.Value(0)
  }

  _onBlurr = () => {
    BackHandler.removeEventListener('hardwareBackPress',
      this._handleBackButtonClick);
  }

  _onFocus = () => {
    BackHandler.addEventListener('hardwareBackPress',
      this._handleBackButtonClick);
  }

  _handleBackButtonClick = () => true

  submitLogin(e) {

    let user = {
      'emailId': this.state.emailId,
      'apiKey': 'LgOiuSu64U'
    };

    this.props.updateLoggedInEmail(this.state.emailId);

    this.props.login(user, (data) => {
      console.log("DATDASDHSD ============>", data.data.StatusCode);
      if (data.data.StatusCode == 200) {
        this.props.navigation.navigate('Verification');
      } else {
        this.setState({ apiStatus: false })
      }
    }, (error) => {
      this.setState({ apiStatus: false })
    });

  }

  componentDidMount() {
    Animated.parallel([
      Animated.timing(this.slideLeft, {
        toValue: 1,
        duration: 5000,
        easing: Easing.ease
      }),
      Animated.sequence([
        Animated.timing(this.scaleImage, {
          toValue: 1,
          duration: 2000,
          easing: Easing.ease
        }),
        Animated.parallel([
          Animated.timing(this.slideImageUp, {
            toValue: 1,
            duration: 3000,
            easing: Easing.ease
          }),
          Animated.timing(this.fadeAway, {
            toValue: 1,
            duration: 3000,
            easing: Easing.ease
          })
        ])
      ])
    ]).start(() => {
      console.log("Animation DONE")
      this.setState({ isAnimationComplete: true })
      Animated.timing(this.fadeIn, {
        toValue: 1,
        duration: 2000,
        easing: Easing.ease
      })
    })
  }


  validate = (text) => {

    this.setState({ apiStatus: true })

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (reg.test(text)) {
      this.setState({ emailId: text })
      this.setState({ isDisabled: false })

    } else {
      this.setState({ emailId: text })
      this.setState({ isDisabled: true })

    }
  }

  render() {
    const { isDisabled, apiStatus } = this.state
    const isAnimationComplete = this.state.isAnimationComplete;

    const scaleBImage = this.scaleImage.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 2.4]
    })

    const slideToLeft = this.slideLeft.interpolate({
      inputRange: [0, 0.3, 0.4, 0.5, 1],
      outputRange: [-340, -150, -150, -150, 50]
    })

    const slideImageToUp = this.slideImageUp.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [100, 0, 60]
    })

    const opacity = this.fadeAway.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 0]
    });

    const submitOpacity = this.fadeIn.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    })

    return (

      <View style={styles.container}>
        <NavigationEvents
          onWillFocus={this._onFocus}
          onWillBlur={this._onBlurr}
        />
        <OfflineNotice />

        <View style={[{ alignItems: 'center' }, styles.border]}>
          <Text style={styles.text}>
            Login
          </Text>
        </View>

        <View style={[{ width: dimensions.fullWidth, alignItems: 'flex-end' }, styles.border]}>
          <Animated.View
            style={{ left: slideToLeft }}>
            <Image source={require('../../images/Group1125.png')}
              style={{}}>{console.log('image')}</Image>
          </Animated.View>
        </View>

        <View style={[{ alignItems: 'center', zIndex: 999 }, styles.border]}>
          <View style={{ left: 3, width: '76%' }}>
            <Text style={styles.emailIdText}>Email Id</Text>
          </View>
          <View style={[styles.placeView, apiStatus ? styles.validTextInput : styles.invalidTextinput]}>
            <TextInput style={styles.placeInput}
              placeholder={'Enter Email Id'}
              editable={true}
              keyboardType={'email-address'}
              onChangeText={(text) => this.validate(text)}
            ></TextInput>
          </View>
        </View>

        {apiStatus ? null :
          <View style={styles.errorText}>
            <Text style={{ color: '#FF0000' }}>Entered Email Id is either invalid or locked</Text>
            <Text style={{ color: '#FF0000' }}>Try after 15 minutes</Text>
          </View>
        }

        <View style={[{ alignItems: 'center', paddingBottom: 20 }, styles.border]}>
          <Animated.View
            style={{ top: slideImageToUp, transform: [{ scale: scaleBImage }] }}>
            <Image
              style={{ width: dimensions.fullWidth, resizeMode: 'contain' }}
              source={require('../../images/Group1126.png')}
            />
          </Animated.View>

          {isAnimationComplete ? (
            <TouchableOpacity style={[isDisabled ? styles.disabled : styles.enabled, styles.button]}
              disabled={isDisabled}
              onPress={e => this.submitLogin(e)} >
              <Text style={{ fontSize: fonts.md, color: '#0D90F3' }}>Submit</Text>
            </TouchableOpacity>
          ) : (
              <Animated.View
                style={{ top: -10, opacity: opacity }}>
                <Image
                  style={{ width: dimensions.fullWidth, resizeMode: 'contain' }}
                  source={require('../../images/Group567.png')}
                />
              </Animated.View>
            )}

        </View>

      </View>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators({ login, updateLoggedInEmail, verify }, dispatch);
}

function mapStateToProps({ verifyReducer }) {
  return {
    code: verifyReducer.code
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  invalidTextinput: {
    borderColor: 'red'
  },
  validTextInput: {
    borderColor: '#0253CC'
  },
  border: {
    padding: 10
  },
  text: {
    fontSize: fonts.md,
    fontWeight: 'bold',
    fontFamily: Platform.OS === 'android' ? fontStyle.dis : fontStyle.sys,
    color: '#4A4A4A'
  },
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    justifyContent: 'space-around',
    width: '100%',
    height: dimensions.fullHeight - 40

  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row'
  },
  placeView: {
    backgroundColor: 'white',
    borderRadius: 6,
    width: '75%',
    borderWidth: 1
  },
  placeInput: {
    fontSize: fonts.sm + 1,
    fontFamily: Platform.OS === 'android' ? fontStyle.txt : fontStyle.sys,
    color: 'grey',
    left: 10,
    height: 40,
    color: '#000000'
  },
  enabled: {
    opacity: 1,
  },
  disabled: {
    opacity: 0.3,
  },
  button: {
    top: 20,
    backgroundColor: 'white',
    borderWidth: 1.5,
    borderRadius: 7,
    width: '84%',
    height: 73,
    borderColor: '#0D90F3',
    justifyContent: 'center',
    alignItems: 'center'
  },
  emailIdText: {
    color: '#4A4A4A',
    fontFamily: Platform.OS === 'android' ? fontStyle.txt : fontStyle.sys,
    fontSize: fonts.sm + 1
  },
  errorText: {
    fontSize: fonts.sm,
    color: '#FF0000',
    alignItems: 'center'
  }
});