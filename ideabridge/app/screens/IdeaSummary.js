import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { ListItem, Icon } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import CardMyIdea from '../components/CardMyIdea';
let _ = require('lodash');
import { fonts, dimensions, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';
import Moment from 'moment';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Toast, { DURATION } from 'react-native-easy-toast';

class IdeaSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refresh: false,

        };
    }

    componentDidMount() {
        const { navigation } = this.props;

    }

    displayListFunc() {
        let displayList = [
            {
                name: <Text style={styles.displayHeaderStyle}>Idea Posted on:</Text>,
                value: <Text style={styles.displayValueStyle}>{`${(new Date(this.props.ideaData.ContentDate)).getDate()}/${(new Date(this.props.ideaData.ContentDate)).getMonth() + 1}/${(new Date(this.props.ideaData.ContentDate)).getFullYear()}`}</Text>
            },
            {
                name: <Text style={styles.displayHeaderStyle}>Registration Number:</Text>,
                value: <Text style={styles.displayValueStyle}>{this.props.ideaData.RegistrationNumber}</Text>
            },
            {
                name: <Text style={styles.displayHeaderStyle}>Category:</Text>,
                value: <Text style={styles.displayValueStyle}>{this.props.ideaData.CategoryName}</Text>
            },
            {
                name: <Text style={styles.displayHeaderStyle}>Subcategory:</Text>,
                value: <Text style={styles.displayValueStyle}>{this.props.ideaData.SubcategoryName}</Text>
            },
            {
                name: <Text style={styles.displayHeaderStyle}>Status:</Text>,
                value: <Text style={styles.displayValueStyle}>{this.props.ideaData.Status.Name}</Text>
            },
            {
                name: <Text style={styles.displayHeaderStyle}>Ideator:</Text>,
                value: <Text style={styles.displayValueStyle}>{this.props.ideaData.IdeatorName}</Text>
            },
        ];
        if (this.props.ideaData.PendingWithDetails.length > 0) {
            let data = this.props.ideaData.PendingWithDetails.map(item => {
                return item.FullName
            }).join(", ");
            displayList.push(
                {
                    name: <Text style={styles.displayHeaderStyle}>Pending with:</Text>,
                    value: <Text style={styles.displayValueStyle}>{data}</Text>
                }
            );
        }

        if (this.props.ideaData.PatentStatus != null) {
            displayList.push(
                {
                    name: <Text style={styles.displayHeaderStyle}>Patent Status:</Text>,
                    value: <Text style={styles.displayValueStyle}>{this.props.ideaData.PatentStatus.Name}</Text>
                }
            );
        }
        if (this.props.ideaData.IdeaType != '') {
            displayList.push(
                {
                    name: <Text style={styles.displayHeaderStyle}>Idea Evaluated As:</Text>,
                    value: <Text style={styles.displayValueStyle}>{this.props.ideaData.IdeaType}</Text>
                }
            );
        }

        if (this.props.ideaData.Score > 0) {
            displayList.push(
                {
                    name: <Text style={styles.displayHeaderStyle}>Score:</Text>,
                    value: <Text style={styles.displayValueStyle}>{this.props.ideaData.Score}</Text>
                }
            );
        }

        if (this.props.ideaData.ChallengeTitle != null) {
            displayList.push(
                {
                    name: <Text style={styles.displayHeaderStyle}>Challenge:</Text>,
                    value: <Text style={styles.displayValueStyle}>{this.props.ideaData.ChallengeTitle}</Text>
                }
            );
        }
        if (this.props.ideaData.IdeaTeamMembers.length > 0) {
            let teamMembers = this.props.ideaData.IdeaTeamMembers.map(item => {
                return item.FullName
            }).join(",");
            displayList.push(
                {
                    name: <Text style={styles.displayHeaderStyle}>Team Members:</Text>,
                    value: <Text style={styles.displayValueStyle}>{teamMembers}</Text>
                }
            );
        }
        return displayList;
    }

    renderItem({ item }) {
        let value = item.value;
        return (
            <ListItem
                title={item.value}
                leftIcon={item.name}
                bottomDivider
            />
        )
    }

    removeReviewer(data) {
        const taskId = this.props.navigation.getParam('taskId', 'NO-data');
        let dataToBeSent = {
            ContentId: this.props.ideaData.ContentID,
            ReviewerId: data.Reviewers.UserID,
            InviterId: data.Inviter.UserID,
            TaskId: taskId
        }
        this.props.removeInvitedReviewer({ data: dataToBeSent })
            .then(() => {
                if (this.props.success) {
                    console.log("in removeInvitedReviewer props success");
                    this.refs.toast.show('Reviewer has been removed successfully.', 500, () => {
                        this.props.getIdeaInfoById({
                            data: this.props.ideaData.ContentID
                        });
                        this.setState({ refresh: true })
                    });
                }
                if (this.props.error) {
                    console.log(
                        "in removeInvitedReviewer props error",
                        this.props.errorMsg
                    );
                    //alert(this.props.errorMsg);
                    this.props.updateAPIStatusBody({
                        data: { loading: false, error: false, errorMsg: "" }
                    });
                }
            });

    }

    remindReviewer(data) {
        const taskId = this.props.navigation.getParam('taskId', 'NO-data');
        let dataToBeSent = {
            ContentId: this.props.ideaData.ContentID,
            ReviewerId: data.Reviewers.UserID,
            ContentType: 'Idea'
        }
        this.props.remindInvitedReviewer({ data: dataToBeSent })
            .then(() => {
                if (this.props.success) {
                    console.log("in remindInvitedReviewer props success");
                    this.refs.toast.show('Reviewer has been reminded successfully.', 500, () => {
                        this.initiateHomeAction();
                    });
                }
                if (this.props.error) {
                    console.log(
                        "in remindInvitedReviewer props error",
                        this.props.errorMsg
                    );
                    //alert(this.props.errorMsg);
                    this.props.updateAPIStatusBody({
                        data: { loading: false, error: false, errorMsg: "" }
                    });
                }
            });
    }

    showIdeaReviewers() {
        if (this.props.ideaData.IdeaReviewers.length > 0) {
            return (
                <View style={styles.inviteReviewerViewStyle}>
                    <Text style={styles.inviteReviewerTextStyle}>Invited Reviewers</Text>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 20 }}
                        style={{ margin: 10, paddingTop: 5 }}
                        key={this.props.ideaData.IdeaReviewers.length}
                        data={this.props.ideaData.IdeaReviewers}
                        extraData={this.state.refresh}
                        renderItem={({ item: rowData }) => {
                            return (

                                <View style={styles.flatListViewStyle}>
                                    <Text style={styles.reviewerNameStyle}>{rowData.Reviewers.FullName} </Text>
                                    {rowData.Status == 'Pending' && <Entypo style={styles.removeReviewerStyle} name='cross' onPress={() => { this.removeReviewer(rowData) }} />}
                                    {rowData.Status == 'Pending' && <FontAwesome style={styles.remindReviewerStyle} name='exclamation' onPress={() => { this.remindReviewer(rowData) }} />}
                                </View>

                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={styles.mainviewStyle}>
                <OfflineNotice />
                <ScrollView style={styles.container}>
                    <View style={styles.elementsContainer}>
                        <Text style={[systemWeights.bold, styles.ideaSummaryTextStyle]}>Idea Summary</Text>
                    </View>


                    <View style={{ margin: 5 }}>
                        <FlatList
                            data={this.displayListFunc()}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        {this.props.ideaData.PatentRequired == true && <View style={styles.patentViewStyle}>
                            <Icon
                                name='badge'
                                type='simple-line-icon'
                                iconStyle={StyleSheet.patentIconStyle}
                            >
                            </Icon>
                            <Text style={styles.patentTextStyle}>Applied For Patent</Text>
                        </View>}
                        {this.props.ideaData.ShowInvitedUserListPanel == true && this.showIdeaReviewers()}
                    </View>
                    <View style={styles.xtraViewStyle} />
                </ScrollView>
                <Toast ref="toast" position='top' />
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        userDetails: state.verifyReducer.userDetails,
        ideaData: state.DataReducer.IdeaInfoById,
        ideas: state.homeReducer.ideas,
        loading: state.DataReducer.generalAPIStatusBody.loading,
        success: state.DataReducer.generalAPIStatusBody.success,
        error: state.DataReducer.generalAPIStatusBody.error,
        errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(ReduxActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(IdeaSummary);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#EEEEEE'
    },
    xtraViewStyle: {
        height: 100
    },
    patentViewStyle: {
        marginTop: 10,
        borderRadius: 6,
        backgroundColor: 'orange',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    patentIconStyle: {
        color: 'white',
        backgroundColor: 'orange',
        height: 40,
        width: 30,
        fontSize: 25,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10
    },
    patentTextStyle: {
        backgroundColor: 'orange',
        color: 'white',
        height: 40,
        fontSize: 25,
        marginTop: 10
    },
    ideaSummaryTextStyle: {
        fontSize: fonts.lg,
        color: 'black',
        bottom: 20,
        marginTop: 20
    },
    removeReviewerStyle:
    {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 22,
        marginRight: 10,
        marginLeft: 5
    },
    remindReviewerStyle: {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 22,
        marginRight: 10,
        marginLeft: 5
    },
    reviewerNameStyle: {
        color: 'blue',
        flexGrow: 1,
        width: 0,
        flexDirection: "column",
        justifyContent: "center",
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 16,
        marginLeft: 10
    },
    flatListViewStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 30,
        borderRadius: 20,
        borderColor: 'black',
        borderWidth: 0.3,
        marginTop: 5
    },
    inviteReviewerViewStyle: {
        backgroundColor: 'white'
    },
    inviteReviewerTextStyle: {
        color: 'blue',
        fontSize: 24,
        marginTop: 5,
        marginLeft: 10
    },
    displayHeaderStyle: {
        fontWeight: 'bold',
        fontSize: 20
    },
    displayValueStyle: {
        fontSize: 18,
        alignSelf: 'flex-start'
    },
    elementsContainer: {
        flex: 1,
        left: 20,
        flexDirection: 'row',
        marginTop: 20
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        fontSize: fonts.md,
    },
    searchIcon: {
        padding: 10,
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: 'white',
        color: '#424242',
    },
    parentSearchBar: {
        width: '100%',
        flexDirection: 'row',
    },
    childSearchBar: {
        width: '64%',
        margin: '3%',
        borderRadius: 5,
    },
    parentIconBar: {
        width: '100%',
        flexDirection: 'row'
    },
    childIconBar: {
        width: '34%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    childViewBadge: {
        width: '34%',
        margin: '3%',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    filterIcon: {
        fontSize: fonts.lg + 2,
        color: 'grey',
        left: 20,
        bottom: 8
    },
    mainviewStyle: {
        flex: 1,
        flexDirection: 'column',
    },
    color: {
        backgroundColor: '#FFFFFF',
    },
    floatingButtonContainer: {
        flexDirection: 'row',
        width: '44%',
        justifyContent: 'flex-end'
    },
    footer: {
        position: 'absolute',
        flex: 1,//0.1,
        left: 0,
        right: 0,
        bottom: dimensions.fullHeight / 1000,
        backgroundColor: 'transparent',
        flexDirection: 'row',
        height: 90,
        width: '100%',
        borderColor: 'black',
    },
    floatingButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 4,
        borderColor: 'white',
        width: 60,
        height: 60,
        margin: 12,
        backgroundColor: 'white',
        borderRadius: 100,
        elevation: 4,
        shadowOffset: { width: 5, height: 5 },
        shadowColor: "black",
        shadowOpacity: 0.5,
        shadowRadius: 10,
        backgroundColor: 'orange'
    },
    footerText: {
        color: '#0086b3',
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: fonts.lg,
    },
    footerText2: {
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white',
        fontSize: fonts.md + 1,
    },
    iconContainer: {
        width: '56%',
        left: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'center'
    },
    resSearchData: {
        paddingBottom: Platform.OS === 'android' ? 90 : 0,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50
    },
})