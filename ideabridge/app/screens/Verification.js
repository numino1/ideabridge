import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button, TouchableOpacity, TextInput, ImageBackground,
    KeyboardAvoidingView, Keyboard, Image, Animated, Easing, ActivityIndicator, Dimensions, BackHandler
} from 'react-native';
import { verify, verifiedCode, setAppToken } from '../actions/verify';
import { connect } from "react-redux";
import CodeInput from 'react-native-confirmation-code-input';
import { bindActionCreators } from 'redux';
import { resend } from '../actions/resend';
import AsyncStorage from '@react-native-community/async-storage';
import base64 from 'react-native-base64';
import { fonts, fontStyle, dimensions } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';
import { NavigationEvents } from 'react-navigation';

class Verification extends Component {
    static navigationOptions = {
        header: null,
        visible: false,
        gesturesEnabled: false
    }
    constructor(props) {
        super(props);

        this.state = {
            code: '',
            isInactive: true,
            isLoading: true,
            isCodeEntered: false,
            errorMessage: ' ',
            isResend: false,
            isDisable: true
        };

        this.scaleImage = new Animated.Value(0.3)
        this.scaleErrorImage = new Animated.Value(0.3)
    }

    _onBlurr = () => {
        BackHandler.removeEventListener('hardwareBackPress',
            this._handleBackButtonClick);
    }

    _onFocus = () => {
        BackHandler.addEventListener('hardwareBackPress',
            this._handleBackButtonClick);
    }

    _handleBackButtonClick = () => true


    _onFulfill(code) {
        let reg = /\b[a-zA-Z0-9]{6}\b/
        if (!reg.test(code)) {
            console.log("Verification Code is Not Correct");
            this.setState({ isInactive: false })
        } else {
            this.setState({ isInactive: true })
            this.setState({ isCodeEntered: true });
            this.setState({ isLoading: true });

            let info = {
                'emailId': this.props.emailId,
                'apiKey': 'LgOiuSu64U',
                'code': code
            }

            this.props.verify(info, () => {
                console.log(' VERIFICATION API CALL SUCCESS')
                this.setState({ isLoading: false });
                this.setState({ isInactive: true });
                this.setState({ isCodeEntered: true });
                let token = `Basic ${base64.encode(this.props.emailId + ':' + code)}`;
                this.props.setAppToken(token);

                this.props.verifiedCode(info.code);
                AsyncStorage.setItem("@VerifiedCode", info.code);
                AsyncStorage.setItem('@EmailId', info.emailId);
                this.animate();
            }, (error) => {
                this.setState({ errorMessage: error })
                this.setState({ isLoading: false });
                this.setState({ isInactive: false });
                this.setState({ isCodeEntered: false });
                this.animateForWrongVerification();
            });
        }
    }

    animate() {

        Animated.parallel([
            Animated.delay(4000),
            Animated.spring(this.scaleImage, {
                toValue: 1,
                friction: 1
            }),
            Animated.delay(4000)
        ]).start(() => {
            this.props.navigation.navigate('HomeScreen');
        })
    }

    animateForWrongVerification() {

        Animated.parallel([
            Animated.delay(4000),
            Animated.spring(this.scaleErrorImage, {
                toValue: 1,
                friction: 1
            }),
            Animated.delay(4000)
        ]).start(() => {
            console.log(" Error code Executed !!!!!");
        })
    }



    resend() {
        this.setState({ isInactive: true });
        this.setState({ isCodeEntered: false });
        this.setState({ isLoading: true });
        this.refs.codeInputRef1.clear();
        let user = {
            'emailId': this.props.emailId
        };
        console.log('emailId ', this.props.emailId);

        this.props.resend(user, () => {

        })
    }

    renderElement() {
        if (!this.state.isInactive) {

            return (
                <View style={styles.textError}>
                    <Text style={styles.errorTextColor}>Entered code is Invalid,</Text>
                    <Text style={styles.errorTextColor}>Please enter valid code or click Resend</Text>
                </View>
            )
        } else if (this.state.isResend) {
            this.setState({ isInactive: true });
            return (
                <Text>A new code has been generated and sent to {"\n"}your registered email id.{"\n"} Your code is valid for 15 minutes</Text>
            )
        }
        else {
            return (<Text style={styles.codeSentMessageStyle}>Enter the code sent to your registered email id</Text>)
        }
    }

    render() {

        const scaleBImage = this.scaleImage.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })

        const scaleErrorImage = this.scaleErrorImage.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })

        return (
            <View style={styles.container}>
                <NavigationEvents
                    onWillFocus={this._onFocus}
                    onWillBlur={this._onBlurr}
                />
                <OfflineNotice />
                <View style={styles.enterCodeView}>
                    <Text style={styles.text}>
                        Enter Code
                  </Text>
                </View>
                <View style={styles.renderElementView}>
                    {this.renderElement()}
                </View>

                <View style={styles.codeInputViewStyle}>
                    <CodeInput
                        ref="codeInputRef1"
                        activeColor='rgba(2, 83, 204, 1)'
                        inactiveColor={this.state.isInactive ? 'rgba(2, 83, 204, 1)' : 'rgba(255,0,0,1)'}
                        autoFocus={true}
                        keyboardType={Platform.OS === 'ios' ? "numbers-and-punctuation" : "numeric"}
                        ignoreCase={true}
                        inputPosition='center'
                        codeLength={6}
                        space={5}
                        size={40}
                        onFulfill={(code) => this._onFulfill(code)}
                        containerStyle={{}}
                        codeInputStyle={styles.code}
                    />
                </View>
                <View style={styles.animatedViewStyle}>
                    {
                        this.state.isCodeEntered ? (this.state.isLoading ? (<ActivityIndicator size="large" />) :
                            (
                                <Animated.View style={{ left: 10, transform: [{ scale: scaleBImage }] }}>
                                    <Image source={require('../../images/Group568correct.png')}
                                        style={{}}>{console.log('image')}</Image>
                                </Animated.View>

                            )) : (this.state.isLoading ? (null) :
                                (
                                    <Animated.View style={{ left: 10, transform: [{ scale: scaleErrorImage }] }}>
                                        <Image source={require('../../images/Group568wrong.png')}
                                            style={{}}>{console.log('image')}</Image>
                                    </Animated.View>

                                ))

                    }
                </View>

                <View>
                    <View style={styles.viewImageStyle}>
                        <Image source={require('../../images/Group1126.png')}
                            style={[styles.img]}></Image>
                    </View>
                    <View style={styles.resend}>
                        <TouchableOpacity style={[this.state.isInactive ? styles.disabled : styles.enabled, styles.button]} onPress={() => { this.resend() }} disabled={this.state.isInactive}>
                            <Text style={styles.resendCodeText}>Resend Code</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps({ authReducer }) {
    return {
        emailId: authReducer.emailId
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ verify, resend, verifiedCode, setAppToken }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Verification);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around',
        width: '100%',
        backgroundColor: '#F5F5F5'
    },
    renderElementView: {
        alignItems: 'center'
    },
    enterCodeView: {
        alignItems: 'center',
        marginTop: 20
    },
    resendCodeText: {
        fontSize: fonts.md,
        color: '#0D90F3'
    },
    viewImageStyle: {
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    animatedViewStyle: {
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    codeInputViewStyle: {
        height: 70,
        justifyContent: 'center'
    },
    errorTextColor: {
        color: '#FF0000'
    },
    codeSentMessageStyle: {
        fontSize: fonts.sm + 1,
        top: 10
    },
    textError: {
        fontSize: fonts.sm,
        top: 10,
        color: '#FF0000',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: fonts.md,
        fontFamily: Platform.OS === 'android' ? fontStyle.rob : fontStyle.sys,
        fontWeight: 'bold',
        color: '#4A4A4A'
    },
    code: {
        borderWidth: 1,
        borderRadius: 5,
        height: '100%',
        fontSize: fonts.md + 2,
        backgroundColor: '#fff',
        color: '#5D5D5D'
    },
    img: {
        width: dimensions.fullWidth,
        height: 0.35 * dimensions.fullHeight
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    enabled: {
        opacity: 1,
    },
    disabled: {
        opacity: 0.3,
    },
    button: {
        backgroundColor: 'white',
        borderWidth: 1.5,
        borderRadius: 7,
        width: '84%',
        height: '65%',
        borderColor: '#0D90F3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    resend: {
        height: 110,
        alignItems: 'center',
        justifyContent: 'center'
    }
});