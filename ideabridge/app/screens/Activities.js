import React, { Component } from 'react';
import { ScrollView, Platform, StyleSheet, Text, View, FlatList, Button, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Avatar, Card, SearchBar, Divider } from 'react-native-elements';
import { getIdeaInfoById, postComment, getChallengeInfoById } from '../actions/actions'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { fonts, shadow, header } from '../css/GlobalCss'

class Activities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  render() {
    const contentype = this.props.navigation.getParam('type', 'NO-data');
    let activityCount = 0
    let activityList = []

    if (contentype == 'Idea') {
      const { IdeaInfoById } = this.props;
      if (IdeaInfoById) {
        activityCount = IdeaInfoById.Activity.length;
        activityList = IdeaInfoById.Activity;
      }
    } else {
      const { ChallengeInfoById } = this.props;
      if (ChallengeInfoById) {
        activityCount = ChallengeInfoById.Activity.length;
        activityList = ChallengeInfoById.Activity;
      }
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollViewStyle}>
          <View style={[styles.elementsContainer]}>
            <Text style={[systemWeights.bold, { ...header }]}>Activities</Text>
            <Text style={styles.count}>({activityCount})</Text>
          </View>

          <View style={styles.flatlistView}>
            <FlatList
              data={activityList}
              renderItem={({ item: rowData }) => {
                return (
                  <View>
                    <Card title={null}
                      containerStyle={[styles.card]}
                    >
                      <Text style={[systemWeights.regular, styles.remarks]}>
                        {rowData.Remarks}
                      </Text>

                      <View style={styles.fullNameView}>
                        <Text style={styles.fullName}>
                          {rowData.UserDetails.FullName}
                        </Text>
                        <Text style={styles.displayDate}>
                          {`${(new Date(rowData.ActivityDate)).getDate()}/${(new Date(rowData.ActivityDate)).getMonth() + 1}/${(new Date(rowData.ActivityDate)).getFullYear()}`}
                        </Text>
                      </View>
                    </Card>
                  </View>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={styles.viewHeight} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getIdeaInfoById, getChallengeInfoById, postComment }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Activities);
const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  fullNameView: {
    flexDirection: 'row',
  },
  flatlistView: {
    paddingBottom: 10,
  },
  scrollViewStyle: {
    flex: 1
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },
  filterButton: {
    fontSize: fonts.lg + 2,
    color: 'black',
    alignSelf: 'center',
    top: Platform.OS === 'android' ? 7 : 0,
  },
  card: {
    ...shadow,
    width: window.width,
    height: window.height,
    borderRadius: 6,
    marginBottom: 10,
    backgroundColor: 'skyblue',
  },
  count: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1
  },
  remarks: {
    marginTop: 10,
    color: 'black',
    fontSize: fonts.sm + 6
  },
  fullName: {
    flex: 1,
    color: 'black',
    fontSize: fonts.sm + 4
  },
  displayDate: {
    fontSize: fonts.sm + 3
  },
  viewHeight: {
    height: 100
  }

})