import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getNotificationList } from '../actions/notification';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Card1 from '../components/Cards';
import { updateAPIStatusBody } from '../actions/actions';
import { getIdeaInfoById } from "../actions/actions";
import { fonts, header } from '../css/GlobalCss'
let _ = require('lodash');

import OfflineNotice from '../components/OfflineNotice';

class Notifications extends Component {

  constructor(props) {
    super(props);
    this.state = {
      text: '',
      search: '',
      notificationData: []
    };
  }

  componentDidMount() {

    this.props.getNotificationList().then(() => {

      this.setState({
        notificationData: this.props.notificationData
      })
    });

  }

  onChangeText = (text) => {
    console.log("in onChangeText", text)
    this.setState({
      text
    })
  }

  search = () => {
    this.setState({
      loading: true,
    })
    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 1000);
  }


  updateSearch = search => {
    this.setState({ search });
  };

  renderSpinner() {
    //check state value of loading variable
    if (this.props.loading) {
      return (
        <View style={[styles.activityIndicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    let notificationCount = 0;
    let filteredNotificationList = [];

    let { notificationData } = this.props;

    if (notificationData) {

      notificationData = JSON.parse(JSON.stringify(notificationData));
      if (this.state.text) {
        notificationData = _.filter(notificationData, (item) => {
          return (item.NotificationDetails.toLowerCase().indexOf((this.state.text).toLowerCase()) != -1);
        });
      }

      notificationCount = notificationData.length;
    }

    return (
      <ScrollView style={styles.container}>
        <OfflineNotice />
        <View style={[styles.elementsContainer]}>
          <Text style={[systemWeights.bold, { ...header }]}>Notifications</Text>
          <Text style={styles.count}>( {notificationCount} )</Text>
        </View>
        <View style={styles.parentSearchBar}>
          <SearchBar
            lightTheme
            inputContainerStyle={[styles.color]}
            leftIconContainerStyle={[styles.color]}
            rightIconContainerStyle={[styles.color]}
            containerStyle={[styles.childSearchBar]}
            inputStyle={[styles.color, styles.inputStyle]}
            value={this.state.text}
            onChangeText={this.onChangeText}
            placeholder='Search'
            placeholderTextColor={'black'}
            clearIcon={this.state.text !== '' ? { name: 'close', color: 'black', type: 'evilicon', size: 32, fontWeight: "50%" } : false}
            searchIcon={{ name: 'search', type: 'evilicons', size: 32, color: 'black' }}
            onClear={this.onClearSearchText}
            showLoadingIcon={this.state.loading} />
        </View>
        {this.renderSpinner()}
        {notificationData == '' ?
          <View style={styles.noResponse}>
            {this.props.loading ? null : <Text style={{ fontSize: fonts.sm + 5, }}>No Notifications</Text>}
          </View>
          :
          <Card1 text={this.state.text} />}
      </ScrollView>

    );
  }
}

function mapStateToProps(state) {
  return {
    notificationData: state.notification.notifications,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getNotificationList, getIdeaInfoById, updateAPIStatusBody }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#ffffff',

  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
    marginTop: 50
  },

  count: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1,
    marginLeft: 2
  },
  parentSearchBar: {
    width: '99%',
    flexDirection: 'row',
    marginLeft: 5
  },
  childSearchBar: {
    width: '95%',
    margin: '3%',
    borderRadius: 5,
    backgroundColor: '#EEEEEE'
  },
  inputStyle: {
    fontSize: fonts.md,
    fontWeight: '500',
    color: 'black',
    left: -9
  },
  parentIconBar: {
    width: '100%',
    flexDirection: 'row',

  },
  childViewBadge: {
    width: '34%',
    margin: '3%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F0F8FF',
    left: 10,

  },
  filterButton: {
    fontSize: fonts.lg + 2,
    color: 'grey',
    left: 20,
    bottom: 8
  },
  color: {
    backgroundColor: '#EEEEEE'
  },
  activityIndicator: {
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  noResponse: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150
  },
  resSearchData: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
})