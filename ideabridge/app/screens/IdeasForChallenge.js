import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { systemWeights } from 'react-native-typography';
import CardIdeaOnChallenge from '../components/CardIdeaOnChallenge';
import { getIdeaInfoById, updateAPIStatusBody } from "../actions/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fonts, header } from '../css/GlobalCss'
import { SearchBar } from 'react-native-elements';
let _ = require('lodash');

class IdeasForChallenge extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      text: '',
      challengeTitle: ''

    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    let challengeTitle = navigation.getParam('challengeTitle', ' ');
    this.setState({ challengeTitle: challengeTitle });
  }

  getIdeaByIdAPI(contentId) {

    this.props.getIdeaInfoById({
      data: contentId,
    })
      .then(() => {
        if (this.props.success) {
          console.log("in getIdeaByIdAPI props success");
          let ideaData = {
            data: { ...this.props.IdeaInfoById.ContentData },
            costDetailsData: this.props.IdeaInfoById.CostDetails,
            challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
            canRespond: this.props.IdeaInfoById.CanRespond,
            canEdit: this.props.IdeaInfoById.CanEdit,
          };
          if (this.props.userDetails.UserID == this.props.IdeaInfoById.CreatorUserId) {
            ideaData.contentId = this.props.IdeaInfoById.ContentID;
            ideaData.contentStatus = this.props.IdeaInfoById.ContentStatus;
          }
          this.props.navigation.navigate("ViewPagerViewIdea", ideaData);

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getIdeaByIdAPI props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  onClearSearchText = () => {

  }

  handleKeyDown = () => {

  }

  onChangeText = (text) => {

    this.setState({
      text,
    })
  }

  listItemClicked(itemData) {
    this.getIdeaByIdAPI(itemData.ContentID);
  }

  render() {
    let { navigation, ideas } = this.props;
    if (ideas.length == 0) {
      return (
        <View></View>
      );
    }
    if (this.state.text) {
      ideas = _.filter(ideas, (item) => {
        return (item.Title.toLowerCase().indexOf((this.state.text).toLowerCase()) != -1);
      });
    }

    const data = navigation.getParam('data', ' ');
    const ideaCount = ideas.length;
    return (
      <View style={styles.mainviewStyle}>
        <ScrollView style={styles.container}>
          <View style={[styles.elementsContainer, { marginTop: 50 }]}>
            <Text style={{ fontSize: 20 }}> Ideas for Challenge:  </Text>

          </View>
          <View style={[styles.elementsContainer, { marginTop: 30 }]}>
            <Text style={[systemWeights.bold, { ...header }]}>{this.state.challengeTitle}</Text>

          </View>
          <View style={[styles.parentSearchBar]}>
            <SearchBar
              lightTheme
              inputContainerStyle={[styles.color]}
              leftIconContainerStyle={[styles.color]}
              rightIconContainerStyle={[styles.color]}
              containerStyle={[styles.childSearchBar, {
                backgroundColor: '#FFFFFF',
                width: '92%'
              }]}
              inputStyle={{ backgroundColor: '#FFFFFF', fontSize: fonts.md, fontWeight: '500', color: 'black', left: -9 }}
              value={this.state.text}
              onChangeText={this.onChangeText}
              placeholder='Search'
              placeholderTextColor={'black'}
              clearIcon={this.state.text !== '' ? { name: 'close', color: 'black', type: 'evilicon', size: 32 } : false}
              searchIcon={{ name: 'search', type: 'evilicons', size: 32, color: 'black' }}
              onSubmitEditing={this.handleKeyDown}
              onClear={this.onClearSearchText}
              showLoadingIcon={this.state.loading} />
          </View>
          {ideas.length === 0 ?
            <View style={styles.resSearchData}>
              <Text style={{ fontSize: fonts.sm + 5, }}>Results not found</Text>
            </View> :
            <View style={styles.padding}>
              <FlatList
                data={ideas}
                renderItem={({ item: rowData }) => {
                  return (
                    <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                      <CardIdeaOnChallenge rowDatab={rowData} />
                    </TouchableOpacity>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>}

        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  let count = Object.keys(state.DataReducer.addIdeaInfo).length;
  return {
    myIdeaCount: count > 0 ? count : 0,
    userDetails: state.verifyReducer.userDetails,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    content: (state.homeReducer.content),
    notificationCount: state.homeReducer.notification.NotificationCount,
    approvalCount: state.homeReducer.notification.ApprovalCount,
    searchData: state.homeReducer.serachData,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,
    ideas: state.DataReducer.ideasOnChallenge,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getIdeaInfoById, updateAPIStatusBody }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(IdeasForChallenge);

const styles = StyleSheet.create({
  mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row',
  },

  count: {
    fontSize: fonts.sm,
    color: 'black',
    marginTop: 1

  },
  activityIndicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5FCFF88"
  },
  padding: { paddingBottom: 90 },
  parentSearchBar: {
    width: '100%',
    flexDirection: 'row',
  },
  childSearchBar: {
    width: '64%',
    margin: '3%',
    borderRadius: 5,
  },
  color: {
    backgroundColor: '#FFFFFF',
  },
  resSearchData: {
    paddingBottom: Platform.OS === 'android' ? 90 : 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
})