import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import CardMyIdea from '../components/CardMyIdea';
let _ = require('lodash');
import { fonts, dimensions, shadow, header } from '../css/GlobalCss'
import OfflineNotice from '../components/OfflineNotice';


class MyIdeas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      text: '',

    };
  }

  componentDidMount() {
    const { navigation } = this.props;

  }

  generateID() {

  }

  onChangeText = (text) => {

    this.setState({
      text,
    })
  }

  renderSpinner() {
    //check state value of loading variable
    if (this.props.loading) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      );
    } else {
      return null;
    }
  }

  getIdeaByIdAPI(contentId) {
    this.props
      .getIdeaInfoById({
        data: contentId,
      })
      .then(() => {
        if (this.props.success) {
          console.log("in getIdeaByIdAPI props success");
          this.props.navigation.navigate("ViewPagerViewIdea", {
            data: this.props.IdeaInfoById.ContentData,
            costDetailsData: this.props.IdeaInfoById.CostDetails,
            contentId: this.props.IdeaInfoById.ContentID,
            contentStatus: this.props.IdeaInfoById.ContentStatus,
            challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
            canRespond: this.props.IdeaInfoById.CanRespond,
            canEdit: this.props.IdeaInfoById.CanEdit,
          });

          this.props.updateAPIStatusBody({
            data: { loading: false, success: false }
          });
        }
        if (this.props.error) {
          console.log(
            "in getIdeaByIdAPI props error",
            this.props.errorMsg
          );
          this.props.updateAPIStatusBody({
            data: { loading: false, error: false, errorMsg: "" }
          });
        }
      });
  }

  onClearSearchText = () => {

  }

  listItemClicked(itemData) {
    this.getIdeaByIdAPI(itemData.ContentID);
  }

  addIdeaButton() {
    console.log("in add idea button");
    let savedStatus = {
      status: false
    };

    this.props.updateSavedAddIdeaFlag({ status: savedStatus });
    let ideaId = this.generateID();
    this.props.navigation.navigate('ViewPager', { ideaId: ideaId });
  }

  handleKeyDown = () => {

  }

  render() {
    let userID;
    let myIdeas = [];
    const { navigation, ideas, userDetails } = this.props;

    if (ideas.length == 0) {
      return (
        <View pointerEvents="none" style={[styles.activityindicator]}>
          <ActivityIndicator size="large" color='red' />
        </View>
      )
    }

    if (userDetails.UserID) {
      userID = userDetails.UserID
    }

    myIdeas = _.filter(ideas, (item) => {
      return (item.CreatorUserId == userID);
    });

    if (this.state.text) {
      myIdeas = _.filter(myIdeas, (item) => {
        return (item.Title.toLowerCase().indexOf((this.state.text).toLowerCase()) != -1);
      });
    }


    let count = myIdeas.length;

    return (
      <View style={styles.mainviewStyle} pointerEvents={this.props.loading ? "none" : "auto"}>
        <OfflineNotice />
        {this.renderSpinner()}
        <ScrollView style={styles.container}>
          <View style={[styles.elementsContainer, { marginTop: 50 }]}>
            <Text style={[systemWeights.bold, { ...header }]}>My Ideas</Text>
            <Text style={{ fontSize: fonts.sm, color: 'black', marginTop: 1 }}>( {count} )</Text>
          </View>

          <View style={[styles.parentSearchBar]}>
            <SearchBar
              lightTheme
              inputContainerStyle={[styles.color]}
              leftIconContainerStyle={[styles.color]}
              rightIconContainerStyle={[styles.color]}
              containerStyle={styles.childSearchBar}
              inputStyle={{ backgroundColor: '#FFFFFF', fontSize: fonts.md, fontWeight: '500', color: 'black', left: -9 }}
              value={this.state.text}
              onChangeText={this.onChangeText}
              placeholder='Search'
              placeholderTextColor={'black'}
              clearIcon={this.state.text !== '' ? { name: 'close', color: 'black', type: 'evilicon', size: 32 } : false}
              searchIcon={{ name: 'search', type: 'evilicons', size: 32, color: 'black' }}
              onSubmitEditing={this.handleKeyDown}
              onClear={this.onClearSearchText}
              showLoadingIcon={this.state.loading} />
          </View>
          {myIdeas.length === 0 ?
            <View style={styles.resSearchData}>
              <Text style={styles.resultNotFoundStyle}>Results not found</Text>
            </View> :
            <View style={styles.flatListViewStyle}>
              <FlatList
                key={myIdeas.length}
                data={myIdeas}
                renderItem={({ item: rowData }) => {
                  if (rowData.CreatorUserId)
                    return (
                      <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                        <CardMyIdea rowDatab={rowData} />
                      </TouchableOpacity>
                    );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>}

        </ScrollView>
        <View>
          <View style={styles.footer}>
            <View style={styles.iconContainer}>
            </View>
            <View style={styles.floatingButtonContainer}>
              <TouchableOpacity style={styles.floatingButton} onPress={() => { this.addIdeaButton() }}>
                <AntDesign style={styles.footerText2} name='plus' />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}


function mapStateToProps(state) {
  let count = Object.keys(state.DataReducer.addIdeaInfo).length;
  console.log("mapStateToProps in MyIdeas count of myideas", count);
  return {
    userDetails: state.verifyReducer.userDetails,
    addIdeaInfo: state.DataReducer.addIdeaInfo,
    myIdeaCount: count > 0 ? count : 0,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ideas: state.homeReducer.ideas,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,

  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ReduxActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyIdeas);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  flatListViewStyle:{
    paddingBottom: 90 
  },
  resultNotFoundStyle:{
    fontSize: fonts.sm + 5,
  },
  elementsContainer: {
    flex: 1,
    left: 20,
    flexDirection: 'row'
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    fontSize: fonts.md,
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: 'white',
    color: '#424242',
  },
  parentSearchBar: {
    width: '99%',
    flexDirection: 'row',
    marginLeft: 5
  },
  childSearchBar: {
    width: '64%',
    margin: '3%',
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    width: '92%'
  },
  parentIconBar: {
    width: '100%',
    flexDirection: 'row'
  },
  childIconBar: {
    width: '34%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  childViewBadge: {
    width: '34%',
    margin: '3%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  filterIcon: {
    fontSize: fonts.lg + 2,
    color: 'grey',
    left: 20,
    bottom: 8
  },
  mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  color: {
    backgroundColor: '#FFFFFF',
  },
  floatingButtonContainer: {
    flexDirection: 'row',
    width: '44%',
    justifyContent: 'flex-end'
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: dimensions.fullHeight / 1000,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    height: 90,
    width: '100%',
    borderColor: 'black',
  },
  floatingButton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 4,
    borderColor: 'white',
    width: 60,
    height: 60,
    margin: 12,
    backgroundColor: 'white',
    borderRadius: 100,
    elevation: 4,
    shadowOffset: { width: 5, height: 5 },
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowRadius: 10,
    backgroundColor: 'orange'
  },
  footerText: {
    color: '#0086b3',
    fontWeight: 'bold',
    alignItems: 'center',
    fontSize: fonts.lg,
  },
  footerText2: {
    alignItems: 'center',
    fontWeight: 'bold',
    color: 'white',
    fontSize: fonts.md + 1,
  },
  iconContainer: {
    width: '56%',
    left: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignSelf: 'center'
  },
  resSearchData: {
    paddingBottom: Platform.OS === 'android' ? 90 : 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
})