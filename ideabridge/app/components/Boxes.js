import React, { Component } from 'react';
import { Platform, StyleSheet, Text, Button, View, TouchableOpacity } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { fonts } from '../css/GlobalCss'

class Boxes extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    const color = this.props.color
    const route = this.props.route
    const title = this.props.title
    const count = this.props.count
    const listData = this.props.listData
    const isDisable = this.props.disabled
    console.log('disable', isDisable);
    return (
      <TouchableOpacity style={[styles.child, { backgroundColor: color }]} onPress={() => { this.props.navigation.navigate(route, { data: listData, countList: count }) }} disabled={isDisable}  >
        <View style={[styles.childContents]} >
          <Text style={[systemWeights.bold, styles.count]}>{count}</Text>
          <Text style={[systemWeights.semibold, styles.title]}>{title}</Text>
        </View>
      </TouchableOpacity>

    );
  }
}
export default Boxes;

const styles = StyleSheet.create({
  childContents: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  child: {
    width: '44%',
    margin: '3%',
    aspectRatio: 1,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  count: {
    fontSize: fonts.xLg,
    color: '#ffffff'
  },
  title: {
    fontSize: fonts.md - 4,
    color: '#ffffff'
  }
});