import React, { PureComponent } from 'react'
// import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import { Svg, G, Line, Rect, Text } from 'react-native-svg'
import * as d3 from 'd3'

let deviceWidth = Dimensions.get('window').width

const GRAPH_MARGIN = 30
const GRAPH_BAR_WIDTH = 35
let colors = {
  axis: '#E4E4E4',
  bars: '#5855D6',
  zeroBar: '#000000'
}

export default class BarChart1 extends PureComponent {
  render() {
    // Dimensions
    const SVGHeight = 250
    const SVGWidth = deviceWidth;
    const graphHeight = SVGHeight - GRAPH_MARGIN
    const graphWidth = SVGWidth - GRAPH_MARGIN
    const data = this.props.data
    colors.bars = this.props.color;
    colors.zeroBar = this.props.zerocolor;
    // X scale point
    const xDomain = data.map(item => item.label)
    const xRange = [0, graphWidth]
    const x = d3.scalePoint()
      .domain(xDomain)
      .range(xRange)
      .padding(1)

    // Y scale linear
    const maxValue = d3.max(data, d => d.value)
    const topValue = Math.ceil(maxValue / this.props.round) * this.props.round
    const yDomain = [0, maxValue]
    const yRange = [0, graphHeight - 70]
    const y = d3.scaleLinear()
      .domain(yDomain)
      .range(yRange)

    // top axis and middle axis
    const middleValue = topValue / 2

    return (
      <Svg width={SVGWidth} height={SVGHeight}>
        <G x="15" y={graphHeight - GRAPH_MARGIN}>
          {/* Top value label */}
          <Text
            x={graphWidth}
            textAnchor="end"
            y={y(topValue) * -1 - 5}
            fontSize={12}
            fill="black"
            fillOpacity={0.4}>
            {topValue + ' ' + topValue}
          </Text>


          {data.map(item => {
            return item.value ? (
              <Rect
                key={'bar' + item.label}
                x={x(item.label) - GRAPH_BAR_WIDTH / 2}
                y={y(item.value) * -1}
                rx={3.5}
                width={GRAPH_BAR_WIDTH}
                height={y(item.value)}
                fill={colors.bars}
              />
            ) : (
                <Rect
                  key={'bar' + item.label}
                  x={x(item.label) - GRAPH_BAR_WIDTH / 2}
                  y={y(item.value) * -1}
                  rx={3.5}
                  width={GRAPH_BAR_WIDTH}
                  height="1"
                  fill={colors.zeroBar}
                />
              )
          })}

          {/* labels */}
          {data.map(item => (
            <Text
              key={'label' + item.value}
              fontSize="12"
              width="10"
              height="100"
              x={x(item.label)}
              y={y(item.value) * -1 - 10}
              textAnchor="middle">{item.value}</Text>
          ))}

          {/* labels */}
          {data.map(item => (
            <Text
              key={'label' + item.label}
              fontSize="12"
              x={x(item.label)}
              y="14"
              textAnchor="middle">{item.label.split(' ')[0]}</Text>
          ))}

          {/* labels */}
          {data.map(item => (
            <Text
              key={'label' + item.label}
              fontSize="12"
              x={x(item.label)}
              y="28"
              textAnchor="middle">{item.label.split(' ')[1]}</Text>
          ))}

          {/* labels */}
          {data.map(item => (
            <Text
              key={'label' + item.label}
              fontSize="12"
              x={x(item.label)}
              y="40"
              textAnchor="middle">{item.label.split(' ')[2]}</Text>
          ))}
        </G>
      </Svg>
    )
  }
}
