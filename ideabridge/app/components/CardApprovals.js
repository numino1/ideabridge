import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { Card } from 'react-native-elements';
import { fonts, shadow } from '../css/GlobalCss';

class CardApprovals extends Component {
  render() {
    const rowData = this.props.rowDatab
    return (
      <Card title={null}
        containerStyle={styles.main}>

        <View style={[styles.textparent, styles.row]}>
          <Text style={[systemWeights.semibold, styles.text]}>
            {rowData.ContentType}
          </Text>
          <Text style={[systemWeights.semibold, styles.text]}>
            {`${(new Date(rowData.CreatedOn)).getDate()}/${(new Date(rowData.CreatedOn)).getMonth() + 1}/${(new Date(rowData.CreatedOn)).getFullYear()}`}
          </Text>
        </View>

        <View style={styles.row}>
          <View style={styles.column}>
            <Text style={styles.text1}>
              {rowData.Title}
            </Text>
          </View>
        </View>

      </Card>
    );
  }
}

export default CardApprovals;

const styles = StyleSheet.create({
  main: {
    ...shadow,
    flex: 1,
    borderRadius: 5,
  },
  bar: {
    width: 4,
    height: 25,
    backgroundColor: '#EE2C38',
    left: -15
  },
  text1: {
    color: '#6495ED',
    fontSize: fonts.sm + 5
  },
  text2: {
    color: '#6495ED',
    fontSize: fonts.sm + 3,
    marginTop: 16
  },
  textparent: {
    marginBottom: 10,
    justifyContent: 'space-between'
  },
  text: {
    color: 'black',
    fontSize: fonts.sm + 5
  },
  contentStatus: {
    marginBottom: 10,
    color: 'darkgrey',
    fontSize: fonts.sm + 1
  },
  categoryName: {
    fontSize: fonts.sm,
    color: 'darkgrey',
    marginTop: -8
  },
  row: {
    flexDirection: 'row'
  },
  column: {
    flexDirection: 'column'
  }
})