import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { Card, Divider } from 'react-native-elements';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as ReduxActions from "../actions/actions"; //Import your actions
import HTMLView from 'react-native-htmlview';
import { withNavigation } from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';
let _ = require('lodash');

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
  }

  getIdeaByIdAPI(contentId, taskId, contentType) {
    if (contentType == 1) {

      this.props
        .getIdeaInfoById({
          data: contentId,
          taskID: taskId
        })
        .then(() => {
          if (this.props.success) {
            let ideaData = {
              data: this.props.IdeaInfoById.ContentData,
              costDetailsData: this.props.IdeaInfoById.CostDetails,
              challengeTitle: this.props.IdeaInfoById.ChallengeTitle,
              formControls: this.props.IdeaInfoById.FormControls,
              canRespond: this.props.IdeaInfoById.CanRespond,
              canEdit: this.props.IdeaInfoById.CanEdit,
              //required for approvals
              contentId: this.props.IdeaInfoById.ContentID,
              showReviewerInvite: this.props.IdeaInfoById.ShowReviewerInvite,
              reviewersList: this.props.IdeaInfoById.IdeaReviewers,
              taskId: taskId,
              showPatent: this.props.IdeaInfoById.PatentRequired,
            };
            if (this.props.userDetails.UserID == this.props.IdeaInfoById.CreatorUserId) {
              ideaData.contentId = this.props.IdeaInfoById.ContentID;
              ideaData.contentStatus = this.props.IdeaInfoById.ContentStatus;
            }
            this.props.navigation.navigate("ViewPagerViewIdea", ideaData);
            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
          }
          if (this.props.error) {
            console.log(
              "in getIdeaByIdAPI props error",
              this.props.errorMsg
            );
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    } else if (contentType == 2) {

      this.props
        .getChallengeInfoById({
          data: contentId,
          taskID: taskId
        })
        .then(() => {
          if (this.props.success) {
            console.log("in getIdeaByIdAPI props success");
            let challengeData = {
              data: { ...this.props.ChallengeInfoById.ContentData },
              title: this.props.ChallengeInfoById.Title,
              type: this.props.ChallengeInfoById.ChallengeType,
              attachments: this.props.ChallengeInfoById.Attachment,
              formControls: this.props.ChallengeInfoById.FormControls,
              //required for approvals
              contentId: this.props.ChallengeInfoById.ContentID,
              showReviewerInvite: this.props.ChallengeInfoById.ShowReviewerInvite,
              reviewersList: this.props.ChallengeInfoById.ChallengeReviewers,
              taskId: taskId,
              canEdit: this.props.ChallengeInfoById.CanEdit,
              canRespond: this.props.ChallengeInfoById.CanRespond
            };
            if (this.props.userDetails.UserID == this.props.ChallengeInfoById.ChallengerDetails.UserID) {
              challengeData.contentId = this.props.ChallengeInfoById.ContentID;
              challengeData.contentStatus = this.props.ChallengeInfoById.ContentStatus;
            }
            this.props.navigation.navigate("ViewPagerViewChallenge", challengeData);

            this.props.updateAPIStatusBody({
              data: { loading: false, success: false }
            });
          }
          if (this.props.error) {
            console.log(
              "in getIdeaByIdAPI props error",
              this.props.errorMsg
            );
            this.props.updateAPIStatusBody({
              data: { loading: false, error: false, errorMsg: "" }
            });
          }
        });
    }
  }

  listItemClicked(itemData) {
    console.log(itemData);
    this.getIdeaByIdAPI(itemData.ContentId, itemData.ContentDetails.TaskId, itemData.ContentDetails.ContentType.Id);
  }

  render() {
    let filteredNotifications = [];
    const { notificationData } = this.props
    let { navigation, userDetails } = this.props;

    filteredNotifications = JSON.parse(JSON.stringify(notificationData));

    const text = this.props.text;

    if (text) {
      filteredNotifications = _.filter(filteredNotifications, (item) => {
        return (item.NotificationDetails.toLowerCase().indexOf((text).toLowerCase()) != -1);
      });
    }

    return (

      <View style={styles.flatListView} pointerEvents={this.props.loading ? "none" : "auto"}>

        <FlatList
          data={filteredNotifications}
          renderItem={({ item: rowData }) => {
            const htmlContent = `<Text>${rowData.NotificationDetails}</Text>`;
            return (

              <View>
                <TouchableOpacity onPress={() => { this.listItemClicked(rowData) }}>
                  <Card title={null}
                    containerStyle={styles.main}
                  >

                    <View style={styles.topLevelViewStyles}>
                      <View style={styles.bar} />
                      <View style={styles.viewBelowBar}>

                        <Text style={styles.text1}>
                          {rowData.ContentDetails.ContentStatus.DisplayName}
                        </Text>
                        <Text style={styles.text3}>
                          {`${(new Date(rowData.Date)).getDate()}/${(new Date(rowData.Date)).getMonth() + 1}/${(new Date(rowData.Date)).getFullYear()}`}
                        </Text>

                      </View>
                    </View>
                    <View styles={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                      <HTMLView value={htmlContent} stylesheet={styles1} /></View>
                  </Card>
                  <Divider style={styles.divider} />
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.verifyReducer.userDetails,
    addIdeaInfo: state.DataReducer.addIdeaInfo,
    notificationData: state.notification.notifications,
    IdeaInfoById: state.DataReducer.IdeaInfoById,
    ChallengeInfoById: state.DataReducer.ChallengeInfoById,
    loading: state.DataReducer.generalAPIStatusBody.loading,
    success: state.DataReducer.generalAPIStatusBody.success,
    error: state.DataReducer.generalAPIStatusBody.error,
    errorMsg: state.DataReducer.generalAPIStatusBody.errorMsg,

  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ReduxActions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Cards));

const styles = StyleSheet.create({

  main: {
    width: window.width,
    height: 160,
    borderRadius: 6,
    marginBottom: 10
  },
  topLevelViewStyles: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  flatListView: {
    paddingBottom: 45
  },
  viewBelowBar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  bar: {
    width: 4,
    height: 20,
    backgroundColor: '#FF1493',
    left: -15,
    flexWrap: 'wrap',
  },
  text1: {
    color: '#808080',
    fontSize: 17,
  },
  text2: {
    marginTop: 10,
    color: '#6495ED',
    fontSize: 19,
  },
  text3: {
    fontSize: 16,

  },
  activityIndicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5FCFF88",
    marginTop: 300
  },
  divider: {
    backgroundColor: 'grey',
    width: '90%',
    alignSelf: 'center',
    marginTop: 3,
    height: 1
  }
})

const styles1 = StyleSheet.create({
  b: {
    fontWeight: '700',
    color: '#6495ED',
  },
  text: {
    marginTop: 10,
    color: '#6495ED',
    fontSize: 15,
    marginRight: 0,
    marginLeft: 0
  }
});