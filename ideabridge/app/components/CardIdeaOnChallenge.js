import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { systemWeights } from 'react-native-typography';
import { SearchBar, Card } from 'react-native-elements';
import { fonts, shadow } from '../css/GlobalCss'
class CardIdeaOnChallenge extends Component {

  render() {
    const rowData = this.props.rowDatab
    return (
      <Card title={null}
        containerStyle={styles.main}>

        <View style={styles.row}>
          <View style={styles.bar} />
          <View style={styles.column}>
            <Text style={styles.contentStatus}>
              {rowData.ContentStatus}
            </Text>
            <Text style={[systemWeights.semibold, styles.categoryName]} numberOfLines={2}>
              {rowData.CategoryName}
            </Text>
            <Text style={styles.text1}>
              {rowData.Title}
            </Text>
            <Text style={[systemWeights.semibold, styles.text2]}>
              {rowData.IdeatorDetails.FullName}
            </Text>
          </View>
        </View>

        <View style={[styles.textparent, styles.row]}>
          <Text style={[systemWeights.light, styles.text]}>
            {`${(new Date(rowData.ContentDate)).getDate()}/${(new Date(rowData.ContentDate)).getMonth() + 1}/${(new Date(rowData.ContentDate)).getFullYear()}`}
          </Text>

          <Text style={[systemWeights.light, styles.text]}>
            {`${rowData.LikeCount} Likes`}
          </Text>
          <Text style={[systemWeights.light, styles.text]}>
            {`${rowData.CommentsCount} Comments`}
          </Text>
        </View>

      </Card>
    );
  }
}

export default CardIdeaOnChallenge;

const styles = StyleSheet.create({
  main: {
    ...shadow,
    flex: 1,
    borderRadius: 5,
  },
  bar: {
    width: 4,
    height: 25,
    backgroundColor: '#EE2C38',
    left: -15
  },
  text1: {
    color: '#6495ED',
    fontSize: fonts.sm + 5
  },
  text2: {
    color: '#6495ED',
    fontSize: fonts.sm + 3,
    marginTop: 16
  },
  textparent: {
    marginTop: 10,
    justifyContent: 'space-between'
  },
  text: {
    color: '#6495ED',
    fontSize: fonts.sm + 2
  },
  contentStatus: {
    marginBottom: 10,
    color: 'darkgrey',
    fontSize: fonts.sm + 1
  },
  categoryName: {
    fontSize: fonts.sm,
    color: 'darkgrey',
    marginTop: -8
  },
  row: {
    flexDirection: 'row'
  },
  column: {
    flexDirection: 'column'
  }
})