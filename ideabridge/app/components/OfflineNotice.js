import React, { PureComponent } from 'react';
import { View, Text, NetInfo, DeviceInfo, Dimensions, StyleSheet, Alert } from 'react-native';


const { width, height } = Dimensions.get('window');
export const getOrientation = () => {
  const { width, height } = Dimensions.get('window');
  return width > height ? 'landscape' : 'portrait';
};
const orientation = getOrientation();
let marginTop = DeviceInfo.isIPhoneX_deprecated && orientation === 'portrait' ? 29 : 0;

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}

class OfflineNotice extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isConnected: true
    };

  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);

    NetInfo.isConnected.fetch().done((isConnected) => {

      if (isConnected) {
        this.setState({ isConnected })
      } else {
        this.setState({ isConnected })
      }

    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  handleConnectivityChange = isConnected => {

    if (isConnected) {

      this.setState({ isConnected });
    } else {

      this.setState({ isConnected });
    }
  };

  render() {

    if (!this.state.isConnected) {
      return <MiniOfflineSign />;
    }
    return null;
  }
}

const styles = StyleSheet.create({

  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    top: marginTop
  },

  offlineText: {
    color: '#fff'
  }

});

export default OfflineNotice;